<?php

namespace App\Helpers;

if (!function_exists('helperx')) {

    function helperx()
    {
        return "helperx";
    }
}

if (!function_exists('usernameTrimmer')) {

    function usernameTrimmer($username)
    {
        if (strpos($username, '@') !== false) {
            return substr($username, 0, -6);
        }

        return substr($username, 0, -5);
    }
}
