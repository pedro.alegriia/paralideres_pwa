<?php

namespace App\Http\Controllers\Api\V1;

use App\Events\RegisteredUser;
use App\Http\Requests\User\UpdatePassword;
use App\Notifications\Admin\AvatarChangeRequest;
use App\Service\Notifiable;
use App\TempAvatar;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Notification;
use Intervention\Image\Facades\Image;
use Storage;
use App\Models\User;
use App\Models\UserProfile;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\User\UserCreateRequest;
use App\Http\Requests\User\UserUpdateRequest;
use App\Http\Requests\User\UserProfileUpdateRequest;
use App\Http\Requests\User\UserImageProfileImageRequest;
use App\Http\Requests\User\UserDeleteUserRequest;
use App\Service\CommonService;

class UserController extends Controller
{
    use Notifiable,CommonService;

    public function index()
    {
        return response()->json(User::simplePaginate(20));
    }

    public function show($id)
    {
        $user = User::with(['profile', 'badges'])->find($id);

        if ($user) {
            $user->avatar = $user->getFirstMediaUrl('profiles');
            return response()->json($user, 200);
        }

        return response()->json('Not Found', 404);
    }

    public function store(UserCreateRequest $request)
    {
        $activation_token = str_random(20);

        $user = User::create([
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'username' => strtok($request->email, '@'),
            'activation_token' => $activation_token,
            'accept_terms' => 1,
        ]);

        $user_profile = new UserProfile();
        $user->profile()->save($user_profile);

       // event(new RegisteredUser($user));
        Mail::to($user->email)->send(new \App\Mail\UserActivationCode($user));
        return response()->json([
            'status' => 1,
            'title' => 'El usuario ha sido registrado',
            'message' => 'Debes activar tu cuenta desde tu correo',
        ], 200);
    }


    // Update one at a time with the required password validation

    public function update(Request $request, $id)
    {
        //dd($request->all());
        $user = User::find($id);
        //dd($user);

        $user->profile()->update($request->all());

        return response()->json(['status' => 1, 'message' => 'Los cambios han sido guardados'], 200);
    }

    public function delete(UserDeleteUserRequest $request, $id)
    {
        if (User::find($id)->delete()) {
            return response()->json('', 200);
        }
        return response()->json('Not Found', 404);
    }

    public function getProfile()
    {
        $user = User::find(Auth::user()->id);
        if ($user) {
            $profile = $user->profile();
            return response()->json($profile);
        }
        return response()->json('Not Found', 404);
    }

    public function subscribe(Request $request)
    {
        $user = User::find(Auth::user()->id);
        $user->subscribe = $request->subscribe;
        $user->save();
        $message = "Actualizado con exito!";
        return response()->json(['status' => 1, 'message' => $message], 200);

    }
    public function updateProfile(UserProfileUpdateRequest $request)
    {
        $user = User::find(Auth::user()->id);
        $profile = UserProfile::where('user_id', $user->id)->first();

        
        if(!isset($profile->id)){
            $profile = new UserProfile();
        }
        $profile->fullname = $request->fullname;
        $profile->country_id = $request->country_id;
        $profile->city = $request->city;
        $profile->birthdate = $request->birthdate;
        $profile->description = $request->description;
        $profile->social_facebook = $request->social_facebook;
        $profile->social_twitter = $request->social_twitter;
        $profile->social_youtube = $request->social_youtube;
        $profile->social_instagram = $request->social_instagram;
        $profile->social_snapchat = $request->social_snapcha;
        $profile->user_id = $user->id;
        $profile->genre = $request->gender;
        $profile->save();
        $array_explode = explode(' ',$request->fullname);
        if(sizeof($array_explode)>=2){
             $name_str= strtolower($array_explode[0].'_'.$array_explode[1]);
        }else{
            $name_str = strtolower($array_explode[0]);
        }
        
        $user->username = $username = $this->compobeUserName($name_str, 0);
        $user->save();
        return response()->json(['status' => 1, 'message' => $profile], 200);
        
        
        //return response()->json($profile);
    }

    public function compobeUserName($username, $index)
    {
        $finalUserName = '';
        if ($index == 0) {
            $user_exist = User::where([['username', '=', $username]])->first();
            if (isset($user_exist->id)) {
                return $this->compobeUserName($username, $index + 1);
            } else {
                return $username;
            }
        } else {
            $username_new = $username . strval($index);
            $user_exist = User::where([['username', '=', $username_new]])->first();
            if (isset($user_exist->id)) {
                return $this->compobeUserName($username, $index + 1);
            } else {
                return $username_new;
            }
        }
    }

    public function AuthorById($id){
        $profile = UserProfile::where('user_id', $id)->first();
        $user = User::find($id);
        $profile->Country = 'México';
        $profile->top5 = $profile->TopResources($id);
        $profile->last5 = $profile->lastResources($id);
        $profileInfo = $user->profile();
        $profile->registerNumber = $profile->countResources($id);
        $profile->user_info  = $profileInfo;
        return $this->setResponse($profile, 'success', 'OK', '200', '', '');

    }

    /*public function updateImage(UserImageProfileImageRequest $request, $id)
    {

        $profile = User::find($id)->profile;
        $previous_image = $profile->image;
        $image = $request->file('image');
        list($width, $height) = getimagesize($image);
        // Image Manipulation
        $original_img = Image::make($image)->encode('png');
        $profile_img = Image::make($image)->fit(300)->encode('png');
        $thumb_img = Image::make($image)->fit(100)->encode('png');
        $img_name = str_random(5) . '_' . uniqid($id);
        $path = storage_path('app/public/assets/uploads/');

        // Image storage
        if ($width > 1000 || $height > 1000) {
            $original_img->widen(1000);
        }

        $original_img->save($path . $img_name . '_original.png');
        $profile_img->save($path . $img_name . '_profile.png');
        $thumb_img->save($path . $img_name . '_thumb.png');
        $profile->image = $img_name;
        $profile->save();

        // Delete old File
        if ($previous_image) {
            Storage::delete('public/assets/uploads/' . $previous_image . '_original.png');
            Storage::delete('public/assets/uploads/' . $previous_image . '_profile.png');
            Storage::delete('public/assets/uploads/' . $previous_image . '_thumb.png');
        }

        // @todo Send Image to S3
        return response()->json(['image' => $img_name], 200);
    }*/

    public function uploadImageProfile(Request $request)
    {
        try {
            $user = User::find(Auth::user()->id);
            $profile = UserProfile::where('user_id', $user->id)->first();
            if($request->file('profile_image') != null){
                $file_name = uniqid() . '.' . $request->profile_image->getClientOriginalExtension();
                $path = Storage::putFileAs('public/profile', $request->file('profile_image'), $file_name);
                $profile->image = 'profile/'.$file_name;
                $profile->save();
            }else{
                $profile->image = $request->profile_image;
                $profile->save();
            }
            
          
            //Notification::send($this->getAdmins(), (new AvatarChangeRequest($request->user()))->delay(10));

            return response()->json(['status' => 1, 'message' => 'Imagen de perfil actualizada'], 200);
        } catch (\Exception $exception) {
        
            return response()->json(['status' => 0, 'message' => 'Ocurrio un problema','error'=>json_encode($exception)], 500);
        }
    }

    public function updateImage(Request $request)
    {
        try {
            $filename = uniqid() . '.' . $request->profile->getClientOriginalExtension();
            $path = Storage::putFileAs('temp_resource_file', $request->file('profile'), $filename);
            $image = Image::make(storage_path('app/' . $path));
            $storage_path = storage_path('app/temp_resource_file/' . $filename);
            $image->save($storage_path, 50);

            $tempAvatar = TempAvatar::firstOrCreate(['user_id' => $request->user()->id]);
            $tempAvatar->addMedia(storage_path('app/temp_resource_file/' . $filename))
                ->toMediaCollection('temp_avatars', 's3');

            Notification::send($this->getAdmins(), (new AvatarChangeRequest($request->user()))->delay(10));

            return response()->json(['status' => 1, 'message' => 'Imagen de perfil actualizada'], 200);
        } catch (\Exception $exception) {
            return response()->json(['status' => 0, 'message' => 'Ocurrio un problema'], 500);
        }
    }

    public function deleteImage(UserDeleteUserRequest $request, $id)
    {
        $profile = User::find($id)->profile;
        $previous_image = $profile->image;
        $profile->image = null;
        $profile->save();

        if ($previous_image) {
            Storage::delete('public/assets/uploads/' . $previous_image . '_original.png');
            Storage::delete('public/assets/uploads/' . $previous_image . '_profile.png');
            Storage::delete('public/assets/uploads/' . $previous_image . '_thumb.png');
        }

        return response()->json(null, 200);
    }

    public function currentUser()
    {
        return response()->json(Auth::user(), 200);
    }

    public function resetPassword(Request $request)
    {
        $user = User::find(Auth::user()->id);

        if ($request->password !== $request->password_confirmation) {
            return response()->json(['errors' => ['old' => ['La confirmación de contraseña no coincide']]], 422);
        }

        if (!Hash::check($request->old, $user->password)) {
            return response()->json(['errors' => ['old' => ['La contraseña actual es incorrecta']]], 422);
        }

        $user->password = bcrypt($request->password);
        $user->save();
        return response()->json(['status' => 1, 'message' => 'La contraseña ha sido actualizada'], 200);
    }

    public function updatePassword(UpdatePassword $request, $id)
    {
        $user = User::find($id);

        if ($request->password !== $request->password_confirmation) {
            return response()->json(['errors' => ['old' => ['La confirmación de contraseña no coincide']]], 422);
        }

        if (!Hash::check($request->old, $user->password)) {
            return response()->json(['errors' => ['old' => ['La contraseña actual es incorrecta']]], 422);
        }

        User::find($id)->update(['password' => bcrypt($request->password)]);
        return response()->json(['status' => 1, 'message' => 'La contraseña ha sido actualizada'], 200);
    }

    public function setAvatar(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $avatar = $user->getFirstMedia('profiles');
        if ($avatar) {
            $avatar->delete();
        }

        $user->update(['default_avatar' => $request->avatar]);


        return response()->json(['status' => 1, 'message' => 'Tu imagen de perfil ha sido actualizada']);
    }

    public function setSettings(Request $request, $id)
    {
        User::findOrFail($id)->update(['receive_emails' => $request['configurations']['send_emails']]);

        return response()->json(['status' => 1, 'message' => 'Cambios guardados'], 200);
    }
}