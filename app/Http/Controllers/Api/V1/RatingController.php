<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Resource;
use App\Models\User;
use App\Models\ResourceRating;
use App\Models\ResourceLike;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RatingController extends Controller
{
    public function sendVote(Request $request)
    {
        $user = User::find(Auth::user()->id);
        $resource_like = ResourceLike::
        where([['user_id','=',$user->id],['resource_id','=',$request->resource_id]])
        ->first();
        if(isset($resource_like->id)){

            $resource_rate = ResourceRating::
            where([['user_id','=',$user->id],['resource_id','=',$request->resource_id]])
            ->first();
            
            $resource_rate->total_rating = $resource_rate->total_rating - $resource_like->rating;
            $resource_rate->total_votes = $resource_rate->total_votes - 1;
            $resource_rate->save();
            
            $resource_like->rating =$request->rating;

            $resource_like->save();

            $resource_rate->total_rating = $resource_rate->total_rating + $request->rating;
            $resource_rate->total_votes = $resource_rate->total_votes + 1;
            $resource_rate->save();

        }else{
            ResourceLike::Create(
                ['resource_id' => $request->resource_id, 'user_id' => $user->id,'rating' => $request->rating]
            );

            $resource_rate = ResourceRating::
            where([['user_id','=',$user->id],['resource_id','=',$request->resource_id]])
            ->first();
            if(isset($resource_rate->id)){
                $resource_rate->total_rating = $resource_rate->total_rating + $request->rating;
                $resource_rate->total_votes = $resource_rate->total_votes + 1;

                $resource_rate->save();
            }else{
                ResourceRating::updateOrCreate(
                    ['resource_id' => $request->resource_id, 'user_id' => $user->id,'total_rating' => $request->rating,'total_votes'=>1]
                );
            }
        }
        

        return response()->json(['status' => 1, 'message' => 'Gracias por tu calificación','debug'=>$resource_rate]);
    }
    public function setRating(Request $request)
    {
        Rating::updateOrCreate(
            ['resource_id' => $request->resource_id, 'user_id' => $request->user_id],
            ['rating' => $request->rating]
        );

        return response()->json(['status' => 1, 'message' => 'Gracias por tu calificación']);
    }

}
