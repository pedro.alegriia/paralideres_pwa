<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Service\Sanitizable;
use DB;
use App\Gallery;

class GalleryController extends Controller
{
    use Sanitizable;

    public function __construct()
    {}

    /**
     * Display a listing of the collections.
     *
     * @return JsonResponse
     */
/*     public function GetOrders()
    {
        $data['resource_header'] = true;
        $data['gallery_user'] = DB::table('gallery')
            ->where(['status'=> true, 'type' => 'user'])
            ->select('gallery.id', 'gallery.image', 'gallery.type')
            ->get();

        $data['gallery_visitant'] = DB::table('gallery')
            ->where(['status'=> true, 'type' => 'visitant'])
            ->select('gallery.id', 'gallery.image', 'gallery.type')
            ->get();

        return response()->json([
            'success' => true,
            'code' => 200,
            'message' => 'Lista cargada correctamente',
            'data' => $data
        ]);
    } */

    public function UpdateOrder(Request $request)
    {
        //update by typo
        $request->validate([
            'gallery_user' => 'required|array',
            'gallery_visitant' => 'required|array'
        ]);

        $gallery_user = $request->gallery_user;
        $gallery_visitant = $request->gallery_visitant;

        foreach($gallery_user as $key => $item){
            dd($key,$item);
            $image = Gallery::findOrFail($item);
            $image->update([
                'order' => $key,
            ]);
        }

        foreach($gallery_visitant as $key => $item){
            $image = Gallery::findOrFail($item);
            $image->update([
                'order' => $key,
            ]);
        }
        return response()->json([
            'success' => true,
            'code' => 200,
            'message' => 'Actualizado correctamente',
            'data' => []
        ]);
    }
}
