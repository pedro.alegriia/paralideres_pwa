<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Collection;
use App\Models\Resource;
use App\Service\Sanitizable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Requests\Collection\CollectionCreateRequest;
use App\Service\CommonService;
use App\Models\User;
use DB;
use Storage;
use App\Models\ResourceCollection;


class CollectionController extends Controller
{
    use CommonService,Sanitizable;

    public function __construct()
    {
        //$this->middleware('auth:api', ['except' => ['index', 'show']]);
    }

    /**
     * Display a listing of the collections.
     *
     * @return JsonResponse
     */
    public function index()
    {
        return response()->json(Collection::all(), 200);
    }


    /**
     * Store a new collection.
     *
     * @param CollectionCreateRequest $request
     * @return JsonResponse
     */


    public function save(Request $request){
        try {
            if($request->id == 0){
                $user = User::find(Auth::user()->id);
                $collection = new Collection([
                    'user_id' => $user->id,
                    'label' => $request->label,
                    'slug' => '',
                    'description' => $request->description,
                ]);
                $collection->save();
                if(isset($request->resources)){

                  

                    $collection->resources()->sync(collect(json_decode($request->resources))->toArray());
                   

                }
                if(isset($request->categories)){
                    $collection->categories()->sync(collect(json_decode($request->categories))->toArray());

                }

              
                return $this->setResponse($collection->id, 'success', 'OK', '200', 'Mensaje de éxito',
                'Colección creada con éxito.');
            }else{
                $collection = Collection::findOrFail($request->id );
                $collection->label = $request->label;
                $collection->slug = $request->slug;
                $collection->description = $request->description;
              
                if(isset($request->resources)){
                    $collection->resources()->sync(collect(json_decode($request->resources))->toArray());

                    /* $relacion = new ResourceCollection();
                    $relacion->resource_id = $request->resources;
                    $relacion->collection_id = $collection->id;
                    $relacion->updated_at = '2021-02-11 03:31:22';
                    $relacion->created_at = '2021-02-11 04:31:22';
                    $relacion->save(); */
                }
                if(isset($request->categories)){
                    $collection->categories()->sync(collect(json_decode($request->categories))->toArray());

                }
                return $this->setResponse($collection->id, 'success', 'OK', '200', 'Mensaje de éxito',
                'Colección actualizada con éxito.');
            }
        }catch (\Exception $exception) {
            app('log')->info($exception->getMessage());
            return $this->setResponse($exception->getMessage(), 'error', 'ERROR', '409', 'Mensaje de error',
                'Ocurrio un problema al intentar guardar la colección.');
        }
        
    }
    
    
    public function all(Request $request)
    {
        $user = User::find(Auth::user()->id);
        $collections = Collection::where([['user_id','=',$user->id]])->get();
        
        foreach($collections as $collection){
            $categories = $collection->Categories;
        }
        
        return $this->setResponse($collections, 'success', 'OK', '200', '', '');

    }

    public function publicGet(Request $request)
    {
        $collections = Collection::all();
        $where=[];
        $data = $this->paginator($request->page,$request->num_results,'\Collection',$where,'id','DESC');
        $collections = $data;
        foreach($collections->data as $collection){
            //$collection->Categories;
            $collection->User;
            $user = User::find($collection->user_id);
            if(isset($user->id)){
                $user_path = $user->profile();
                $collection->username = $user->username;
                $userArray = array_merge($user_path->toArray(),$user->toArray());
                $userArray['username'] = $this->getUserName($userArray['username'],$user->id);
                $collection->user_info = $userArray;
            }else{
                $resource->username = 'N/A';
            }
            $collection->total_resources = $collection->resourcesCount();
        }

        return $this->setResponse($collections, 'success', 'OK', '200', '', '');

    }

    public function getResourceByCollection($id,Request $request)
    {
        $collection = Collection::findOrFail($id);
        $response=[];
        $array=[$id];
        $where=[];
        $response_arr= $this->arrayPaginator($request->page,$request->num_results,'\Collection','\Resource',$array,$where,'\ResourceCollection','collection_id','resource_id');
        $user = User::find($collection->user_id);
        if(isset($user->id)){
            $user_path = $user->profile();
            $collection->username = $user->username;
            $userArray = array_merge($user_path->toArray(),$user->toArray());
            $userArray['username'] = $this->getUserName($userArray['username'],$user->id);
            $collection->user_info = $userArray;
        }else{
            $resource->username = 'N/A';
        }
        if(isset($response_arr->data)){
            foreach($response_arr->data as $resource){
                
           
                $categories = $resource->getCategories();
                $total_res = 0;
                foreach ($categories as $category){
                    $total_res = $total_res+$category->tot_recources;
                }
                $resource->total_resources = $total_res;
                $resource->categories_info = $categories;
                $resource->getRandomImage();
                $downloads = $resource->downloadsCount();
                $resource->downloads = $downloads;
                $resource->shared = rand(1,100);
                
                
                if(isset($user->id)){
                    $user_path = $user->profile();
                    $resource->username = $user->username;
                    $user = $resource->User;
                    //$userArray = array_merge($user_path->toArray(),$user->toArray());
                    $resource->user_info = $user;
                }else{
                    $resource->username = 'N/A';
                }
                array_push($response,$resource);
            }
            $collection->total_pages = $response_arr->total_pages;
            $collection->resources = $response;
        }else{
            $collection->total_pages =0;
            $collection->resources = [];
        }
        
        
        return $this->setResponse($collection, 'success', 'OK', '200', '', '');
    }

    public function getById($id)
    {
        $collection = Collection::findOrFail($id);
        $collection->Categories;
        $collection->Resources;
        return $this->setResponse($collection, 'success', 'OK', '200', '', '');
    }

    public function getResourceByCategories($id,Request $request)
    {
        $resources = DB::table('resources')->where([['slug','LIKE','%'.$request->slug.'%']])->orwhere([['title','LIKE','%'.$request->slug.'%']])->get();
        $collection = Collection::find($id);
        $array_categ = [];
        $reponseArray = [];
        $arrayCategResource = [];
        // $reponseArray = [];
        //array_push($reponseArray,$resources);
        /******
        $collection = Collection::find($id);
        $categories = $collection->Categories;
        $array_categ = [];
        $reponseArray = [];
        $arrayCategResource = [];
        foreach($categories as $category){

            array_push($array_categ,$category->id);
        }
        * */
        foreach($resources as $resource){
            $resource = Resource::find($resource->id);
            //$categories = $resource->Categories;
            //foreach($categories as $categ){
                //if(in_array($categ->id,$array_categ)){
                    if(!in_array($resource->id,$arrayCategResource)){
                        array_push($arrayCategResource,$resource->id);
                        array_push($reponseArray,$resource);
                    }
                    
              //  }
            //}
            
        }

        return $this->setResponse($reponseArray, 'success', 'OK', '200', '', '');

    }
    /**
     * Display the specified resource.
     *
     * @param $slug
     * @return JsonResponse
     */

    public function show($slug)
    {
        $collection = Collection::with(['categories', 'resources'])->where('slug', $slug)->first();
        return response()->json(['status' => 1, 'collection' => $collection], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */

    public function update(CollectionCreateRequest $request, $id)
    {
        $collection = Collection::findOrFail($id);
        $collection->label = $request->label;
        $collection->slug = $this->toSlug($request->label);
        $collection->description = $request->description;
        $collection->category_id = $request->category;
        $collection->save();

        return response()->json($collection, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */

    public function destroy($id)
    {
        return response()->json(Collection::findOrFail($id)->delete(), 200);
    }

   

    public function getResources($collection_id)
    {
        $resources = Resource::select(['id', 'title'])->get();
        $collection_resources = Collection::find($collection_id)->resources;

        return response()->json(['resources' => $resources, 'collection_resources' => $collection_resources], 200);
    }

    public function getResourcesByCollection($collection_id)
    {
        $collection_resources = Collection::find($collection_id)->resources;

        return response()->json(['collection_resources' => $collection_resources], 200);
    }

    public function syncResources(Request $request)
    {
        $collection = Collection::findOrFail($request->collection_id);
        $collection->resources()->sync($request->resources);

        return response()->json(['status' => 1, 'message' => 'Los cambios han sido guardados'], 200);
    }

    public function recovery_s3_names(){
        $documents = Storage::disk('s3')->allFiles('');
        $array_notfound = [];
        $array_found = [];
        foreach($documents as $document){
            $cadena = $document;
            $cadena_array = explode('/',$cadena);
            $slug = "";
            
            if(count($cadena_array)>1){
                $slug = $cadena_array[1];
            }else{
                $slug = $cadena_array[0];
            }
    
            $resource = Resource::where('attachment',$slug)->first();
            if(isset($resource->id)){
                $resource->attachment = $cadena;
                $resource->save();
            }
        }

        return "Success";
    }
}
