<?php

namespace App\Http\Controllers\Api\V1;

use App\Service\CommonService;

use App\Models\User;
use App\Models\UserProfile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use App\Http\Requests\Auth\AuthenticateRequest;
use Illuminate\Support\Facades\Log;
use Spatie\Permission\Models\Role;
use Session;
use Validator;
use Mail;
use Illuminate\Support\Str;
use App\Models\Resource;
use Illuminate\Support\Facades\Password;

class AuthenticateController extends Controller
{
    use ThrottlesLogins, CommonService;

    public function register(Request $request)
    {
        $v = Validator::make($request->all(), [
            'username' => 'required|min:3',
            'email' => 'required|email|unique:users',
            'password'  => 'required|min:3|confirmed',
        ]);
        if ($v->fails())
        {
            return response()->json([
                'status' => 'error',
                'errors' => $v->errors()
            ], 422);
        }
        $user = new User();
        $arrayUserName = explode('@',$request->username);
        $user->username = $arrayUserName[0];
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->rol_id = 7;
        $user->is_active = 0;
       // $random1 = Str::random(30);
       // $user->activation_token = $random1;
        $user->save();
        //Mail::to($user->email)->send(new \App\Mail\UserActivationCode($user));
        Mail::to($user->email)->send(new \App\Mail\UserActivationCode($user));
        $profile = new UserProfile();        
        $profile->user_id = $user->id;
        $profile->save();

        
        return response()->json(['status' => 'success'], 200);
    }

    /**
     * Login user and return a token
     */
    public function login(Request $request)
    {
        $user = User::where('email', $request['email'])->first();

        if (!$user) {
            return response()->json(['status' => 0, 'message' => 'Credenciales inválidas'], 403);
        }

        if (!$user->is_active) {
            return response()->json(['status' => 0, 'message' => 'Debes confirmar el correo de activación'], 403);
        }
        if ($user) {
            $credentials = $request->only('email', 'password');

            if ($token = $this->guard()->attempt($credentials)) {
                return response()->json(['status' => 'success'], 200)->header('Authorization', $token);
            }
            return response()->json(['error' => 'login_error'], 401);
        }



       // $credentials = $request->only('email', 'password');
       // if ($token = $this->guard()->attempt($credentials)) {
         //   return response()->json(['status' => 'success'], 200)->header('Authorization', $token);
       // }
       // return response()->json(['error' => 'login_error'], 401);
    }

        /**
     * Get authenticated user
     */
    public function user(Request $request)
    {
        $user = User::find(Auth::user()->id);
        $rol = Role::find($user->rol_id);
        $user->rol = $rol->name;
        $user->profile = $user->profile();
        return response()->json([
            'status' => 'success',
            'data' => $user
        ]);
    }

    /**
     * Refresh JWT token
     */
    public function refresh()
    {
        if ($token = $this->guard()->refresh()) {
            return response()
                ->json(['status' => 'successs'], 200)
                ->header('Authorization', $token);
        }
        return response()->json(['error' => 'refresh_token_error'], 401);
    }

    
        /**
     * Return auth guard
     */
    private function guard()
    {
        return Auth::guard();
    }

    public function authenticate(AuthenticateRequest $request)
    {
        $input = $request->input();
        if ($lockedOut = $this->hasTooManyLoginAttempts($request)) {

            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);

        }

        $user = User::where('email', $input['email'])->first();

        if (!$user) {
            return response()->json(['status' => 0, 'message' => 'Credenciales inválidas'], 403);
        }

        if (!$user->is_active) {
            return response()->json(['status' => 0, 'message' => 'Debes confirmar el correo de activación'], 403);
        }

        if ($user) {
            $credentials = $request->only('email', 'password');

            if ($this->guard()->attempt($credentials, true)) {
                $user = Auth::user();
                //$access_token = $user->createToken(env('PASSPORT_CLIENT_SECRET'))->accessToken;
                //Session::put('access_token', $access_token);
                return response()->json(['status' => 1, 'message' => 'Bienvenido a ParaLideres!', 'user' => $user]);
                return redirect('/')->with('success', 'Bienvenido a ParaLideres!');
                //$success['access_token'] = $access_token;
                /*return $this->setResponse($success, 'success', 'OK', '200', 'Success!',
                    'Bienvenido a ' . env('APP_NAME'));*/
            } else {
                $this->incrementLoginAttempts($request);
                return response()->json(['status' => 0, 'message' => 'Credenciales incorrectas'], 402);
                return $this->setResponse([], 'error', 'NotOK', 403, 'Credenciales incorrectas', '');
            }
        } else {
            $this->incrementLoginAttempts($request);
            return redirect()->back()->with('error', 'Debes activar tu cuenta. Revisa tu bandeja de correo.');
            /*return $this->setResponse([], 'error', 'NotOK', '500', 'Error!',
                'Debes activar tu cuenta. Revisa tu bandeja de correo.');*/
        }

    }

    /**
     * Redirect the user after determining they are locked out.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */

    protected function sendLockoutResponse(Request $request)
    {
        $seconds = $this->secondsRemainingOnLockout($request);
        return response()->json($this->getLockoutErrorMessage($seconds), 403);
    }


    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */

    public function username()
    {
        return property_exists($this, 'username') ? $this->username : 'email';
    }

    
    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function logout(Request $request)
    {
        //$request->user()->token()->revoke();
        $this->guard()->logout();
        //$request->session()->flush();
        //$request->session()->regenerate();

        return $this->setResponse([], 'success', 'OK', '200', 'Success!', 'Has cerrado sesión');
    }
    public function forgetPassword()
    {
        $credentials = request()->validate(['email' => 'required|email']);


        
        Password::sendResetLink($credentials);
        $this->setResponse([], 'success', 'OK', '200', 'Success!', 'Reset password link sent on your email id.');
    }

   
}

