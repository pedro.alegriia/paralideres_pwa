<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Ad;
use App\Models\Resource;
use App\Models\Category;
use App\Models\ResourceTag;
use App\Models\ResourceDownload;
use App\Models\User;
use App\Models\Like;
use App\Models\Collection;
use App\Models\Tag;
use App\Models\UserProfile;
use App\Notifications\ResourceCreated;
use App\Notifications\ResourceUpdated;
use App\Service\Notifiable;
use App\Service\Sanitizable;
use Carbon\Carbon;
use Storage;
use DB;
use Mail;
use App\Service\CommonService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Resource\ResourceCreateRequest;
use App\Http\Requests\Resource\ResourceDeleteRequest;
use App\Http\Requests\Resource\ResourceUploadRequest;
use App\Http\Requests\Resource\ResourceUpdateRequest;
use App\Http\Requests\Resource\ResourceAddToCollectionRequest;

//use Illuminate\Support\Facades\Validator;
use Validator;

class ResourceController extends Controller
{
    use CommonService,Sanitizable, Notifiable;

    protected $auth;

    /**
     * Create a new controller instance.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
      
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        // Create Request to Handle this limit Type
        $limit = intval($request->limit);
        $limit = $limit > 0 && $limit < 20 ? $limit : 20;

        $user_id = ($this->auth) ? $this->auth->id : null;
        $tag_id = ($request->has('tag_id')) ? $request->tag_id : null;
        $tag_slug = ($request->has('tag_slug')) ? $request->tag_slug : null;
        $category_id = ($request->has('category_id')) ? $request->category_id : null;
        $author = ($request->has('author')) ? $request->author : null;
        $author_id = 0;

        if ($request->has('cat_slug') && !empty($request->cat_slug)) {
            $category_id = DB::table('categories')->where('slug', $request->cat_slug)->pluck('id');
        }

        if ($request->has('author') && !empty($request->author)) {
            $authorInfo = DB::table('users')->select('id')->where('username', $author)->where('is_active',
                1)->get('id')->toArray();
            if (count($authorInfo) > 0) {
                $author_id = $authorInfo[0]->id;
            }
        }

        $search_text = ($request->has('search_text')) ? $request->search_text : null;

        if ($author != null) {
            $resources = Resource::select('resources.*')
                ->where('resources.user_id', $author_id)
                ->with(
                    [
                        'likesCount',
                        'category',
                        'user',
                        'likes' => function ($q) use ($user_id) {
                            $q->where('user_id', $user_id);
                        }
                    ]);
        } else {
            $resources = Resource::select('resources.*')
                ->with(
                    [
                        'likesCount',
                        'category',
                        'user',
                        'likes' => function ($q) use ($user_id) {
                            $q->where('user_id', $user_id);
                        }
                    ]);
        }

        if (!empty($tag_id) || !empty($tag_slug)) {
            $resources->join('resource_tag', 'resource_tag.resource_id', '=', 'resources.id')
                ->join('tags', 'tags.id', '=', 'resource_tag.tag_id')
                ->where(function ($q) use ($tag_id, $tag_slug) {
                    $q->where('tags.id', $tag_id)->orWhere('tags.slug', $tag_slug);
                });
        }

        if (!empty($category_id)) {
            $resources->where('resources.category_id', $category_id);
        }

        if (!empty($search_text)) {
            $resources->where('resources.title', 'like', '%' . $search_text . '%');
        }

        $resources = $resources
            ->orderBy('resources.created_at', 'desc')
            ->Paginate($limit);

        $resources->appends('tag_id', $tag_id);
        $resources->appends('tag_slug', $tag_slug);
        $resources->appends('category_id', $category_id);
        $resources->appends('search_text', $search_text);

        return $this->setResponse($resources, 'success', 'OK', '200', '', '');
    }
    public function all(Request $request)
    {
        $user = User::find(Auth::user()->id);
        $resources = Resource::where([['user_id','=',$user->id]])->get();
        foreach($resources as $resource){
            $resource->Categories;
            
        }
        return $this->setResponse($resources, 'success', 'OK', '200', '', '');

    }

    public function publicGet(Request $request)
    {
        $where=[['published',True]];
        $resources = $this->paginator($request->page,$request->num_results,'\Resource',$where,'id','DESC');
        foreach($resources->data as $resource){
            $categories = $resource->getCategories();
            $total_res = 0;
            foreach ($categories as $category){
                $total_res = $total_res+$category->tot_recources;
            }
            $resource->total_resources = $total_res;
            $resource->categories_info = $categories;
            $resource->getRandomImage();
            $downloads = $resource->downloadsCount();
            $resource->downloads = $downloads;
            $resource->shared = rand(1,100);
            
            $user = User::find($resource->user_id);
            if(isset($user->id)){
                $user_path = $user->profile();
                $resource->username = $user->username;
                //dd($user->username);
                $user = $resource->User;
                $userArray = array_merge($user_path->toArray(),$user->toArray());
                
                $userArray['username'] = $this->getUserName($userArray['username'],$user->id);

                $resource->user_info = $userArray;
            }else{
                $resource->username = 'N/A';
            }
        }
        return $this->setResponse($resources, 'success', 'OK', '200', '', '');

    }

    public function publicHome(Request $request)
    {
        $where=[['published',True]];
        $resources = $this->paginator($request->page,$request->num_results,'\Resource',$where,'id','DESC');
        foreach($resources->data as $resource){
            $categories = $resource->getCategories();
            $total_res = 0;
            foreach ($categories as $category){
                $total_res = $total_res+$category->tot_recources;
            }
            $resource->total_resources = $total_res;
            $resource->categories_info = $categories;
            $resource->getRandomImage();
            $downloads = $resource->downloadsCount();
            $resource->downloads = $downloads;
            $resource->shared = rand(1,100);
            
            $user = User::find($resource->user_id);
            if(isset($user->id)){
                $user_path = $user->profile();
                $resource->username = $user->username;
                //dd($user->username);
                $user = $resource->User;
                $userArray = array_merge($user_path->toArray(),$user->toArray());
                
                $userArray['username'] = $this->getUserName($userArray['username'],$user->id);

                $resource->user_info = $userArray;
            }else{
                $resource->username = 'N/A';
            }
        }
        return $this->setResponse($resources, 'success', 'OK', '200', '', '');
        /*
        $lastResources = Resource::latest()->first();
        $array_ids = [];
        $resources = [];
        $random = [];
        if(isset($lastResources->id)){
            if($lastResources->id < 12){
                for ($i = 1; $i <= 12; $i++) {
                    $rand = rand(1,$lastResources->id);
                    $resource = Resource::find($rand);
            
                    array_push($resources,$resource);
                    array_push($random,$rand);
                    $resource = null;
                }
                
            }else{
                for ($i = 1; $i <= 12; $i++) {
                    $flag = False;
                    $id = $this->getRandomResource($flag,$array_ids);
                    if($id == null){
                        $i=$i-1;
                    }else{
                        array_push($array_ids,$id);
                    }
                    
                }
                $resources = Resource::whereIn('id', $array_ids)->get();
            }
        }
       
        if(sizeof($resources)>0){
            foreach($resources as $resource){
                $categories = $resource->getCategories();
                $total_res = 0;
                foreach ($categories as $category){
                    $total_res = $total_res+$category->tot_recources;
                }
                $resource->total_resources = $total_res;
                $resource->categories_info = $categories;
                $resource->getRandomImage();
                $downloads = $resource->downloadsCount();
                $resource->downloads = $downloads;
                $resource->shared = rand(1,100);
                
                $user = User::find($resource->user_id);
                if(isset($user->id)){
                    $user_path = $user->profile();
                    $resource->username = $user->username;
                    $user = $resource->User;
                    $userArray = array_merge($user_path->toArray(),$user->toArray());
                    $userArray['username'] = $this->getUserName($userArray['username'],$user->id);
                    $resource->user_info = $userArray;
                }else{
                    $resource->username = 'N/A';
                }
                
            }
        }
        
        
        return $this->setResponse($resources, 'success', 'OK', '200', '', '');*/

    }

    
    protected function getRandomResource($flag,$array_ids){
        $lastResources = Resource::latest()->first();
        $resource = Resource::find(rand(1,$lastResources->id));
        
        if(isset($resource->id) && $resource->id != null){
            $response_cat = $resource->comprobeCategoriesVisible();
            if(!in_array($resource->id,$array_ids) && $response_cat == True){
                $flag = True;
            }else{
                $flag = False;
            }
            
        }else{
            $flag = False;
        }
        
        if($flag == True){
            return $resource->id;
        }else{
            $this->getRandomResource($flag,$array_ids);
        }
    }
    public function topTwelve(){
        $lastResources = Resource::latest()->first();
        $array_ids = [];
        $resources = [];
        $random = [];
        if(isset($lastResources->id)){
            if($lastResources->id < 12){
                for ($i = 1; $i <= 12; $i++) {
                    $rand = rand(1,$lastResources->id);
                    $resource = Resource::find($rand);
                    array_push($resources,$resource);
                    array_push($random,$rand);
                    $resource = null;
                }
                
            }else{
                for ($i = 1; $i <= 12; $i++) {
                    $id_ran = rand(1,$lastResources->id);
                    $resource = Resource::find($id_ran);
                    if(isset($resource->id)){
                        $categories = $resource->getCategories();
                        if(count($categories)<=0){
                            $flag_test = True;
                            foreach ($categories as $category){
                                if($category->visible == 0){
                                    $flag_test = False;
                                }
                            }
                            if($flag_test == True){
                                array_push($array_ids,rand(1,$id_ran));

                            }else{
                                
                                $i--;
                            }
    
                        }else{
                            $i--;
                        }
                    }else{
                        $i--;
                    }
                    
    
                }
                $resources = Resource::whereIn('id', $array_ids)->get();
            }
        }
        
        if(sizeof($resources)>0){
            foreach($resources as $resource){
                $categories = $resource->getCategories();
                $total_res = 0;
                foreach ($categories as $category){
                    $total_res = $total_res+$category->tot_recources;
                }
                $resource->total_resources = $total_res;
                $resource->categories_info = $categories;
                
                $resource->getRandomImage();
                $downloads = $resource->downloadsCount();
                $resource->downloads = $downloads;
                $resource->shared = rand(1,100);
                $resource->User;
                $user = User::find($resource->user_id);
                if(isset($user->id)){
                    $user_path = $user->profile();
                    $resource->username = $user->username;
                    $user = $resource->User;
                    $userArray = array_merge($user_path->toArray(),$user->toArray());
                    $userArray['username'] = $this->getUserName($userArray['username'],$user->id);
                    $resource->user_info = $userArray;
                }else{
                    $resource->username = 'N/A';
                }
            }
        }
        
        
        return $this->setResponse($resources, 'success', 'OK', '200', '', '');

    }

    public function getResourceByCategories(Request $request){
        $categories = json_decode($request->input('categories'));
        $response=[];
        
        $where=[[]];
        $response_arr = $this->arrayPaginator($request->page,$request->num_results,'\Category','\Resource',$categories,$where,'\ResourceCategory','category_id','resource_id');
        foreach($response_arr->data as $resource){
            $categories = $resource->getCategories();
            $total_res = 0;
            foreach ($categories as $category){
                $total_res = $total_res+$category->tot_recources;
            }
            $resource->total_resources = $total_res;
            $resource->categories_info = $categories;
            $resource->getRandomImage();
            $downloads = $resource->downloadsCount();
            $resource->downloads = $downloads;
            $resource->shared = rand(1,100);
            
            $user = User::find($resource->user_id);
            if(isset($user->id)){
                $user_path = $user->profile();
                $resource->username = $user->username;
                $user = $resource->User;
                $userArray = array_merge($user_path->toArray(),$user->toArray());
                $userArray['username'] = $this->getUserName($userArray['username'],$user->id);
                $resource->user_info = $userArray;
            }else{
                $resource->username = 'N/A';
            }
            array_push($response,$resource);
        }
        $response_arr->data = $response;
        return $this->setResponse($response_arr, 'success', 'OK', '200', '', '');
    }
    public function allAuthors(){
        $authors_id = DB::table('resources')->select('user_id')->groupBy('user_id')->get();
        $array_ids = [];
        foreach ($authors_id as $ids){
            array_push($array_ids,$ids->user_id);
            
        }
        
        $authors = User::whereIn('id', $array_ids)->select('id','username')->get();
        foreach ($authors as $auth){
            $profile = $auth->profile();
            $auth->profile = $profile;
        }
        return $this->setResponse($authors, 'success', 'OK', '200', '', '');

    }
    public function ResourcesByAuthor($id, Request $request)
    {   
        $user_response = User::select('username')->find($id);
        $user = User::find($id);
        $where=[['published',True],['user_id',$user->id]];
        $resources = $this->paginator($request->page,$request->num_results,'\Resource',$where,'id','DESC');
        foreach($resources->data as $resource){
            $categories = $resource->getCategories();
            $total_res = 0;
            foreach ($categories as $category){
                $total_res = $total_res+$category->tot_recources;
            }
            $resource->total_resources = $total_res;
            $resource->categories_info = $categories;
            $resource->getRandomImage();
            $downloads = $resource->downloadsCount();
            $resource->downloads = $downloads;
            $resource->shared = rand(1,100);
            
            $user = User::find($resource->user_id);
            if(isset($user->id)){
                $user_path = $user->profile();
                $resource->username = $user->username;
                $user = $resource->User;
                $userArray = array_merge($user_path->toArray(),$user->toArray());
                $userArray['username'] = $this->getUserName($userArray['username'],$user->id);
                $resource->user_info = $userArray;
            }else{
                $resource->username = 'N/A';
            }
        }
        return $this->setResponse($resources, 'success', 'OK', '200', '', '');
    }
    public function getById($id)
    {
        $resource = Resource::findOrFail($id);
        $categories = $resource->getCategories();
        $total_res = 0;
        foreach ($categories as $category){
            $total_res = $total_res+$category->tot_recources;
        }
        $resource->total_resources = $total_res;
        $resource->categories_info = $categories;
        $resource->getRandomImage();

        $downloads = $resource->downloadsCount();
        $resource->downloads = $downloads;
        $resource->shared = rand(1,100);

        $user = User::find($resource->user_id);
        $user_path = $user->profile();
        $resource->username = $user->username;
        $user = $resource->User;
        $userArray = array_merge($user_path->toArray(),$user->toArray());
        $userArray['username'] = $this->getUserName($userArray['username'],$user->id);
        $resource->user_info = $userArray;
        return $this->setResponse($resource, 'success', 'OK', '200', '', '');
    }
    /**
     * Home page resource search option
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(Request $request)
    {
        
        $pageNo = isset($request->pageNo) ? $request->pageNo : 1;
        $limit = 12;
        $skip = ($pageNo - 1) * $limit;
        $where = [['title', 'like', '%' . '""' . '%']];

        if(isset($request->title) && $request->title != ''){
            $where = [['title', 'like', '%' . $request->title . '%']];
        }
        
        $resources = $this->paginatorSearchCustom($request->page,$request->num_results,'\Resource',$where,'id','DESC',$request->category_id,$request->author_id);
        foreach($resources->data as $resource){
            $categories = $resource->getCategories();
            $total_res = 0;
            foreach ($categories as $category){
                $total_res = $total_res+$category->tot_recources;
            }
            $resource->total_resources = $total_res;
            $resource->categories_info = $categories;
            $resource->getRandomImage();
            $downloads = $resource->downloadsCount();
            $resource->downloads = $downloads;
            $resource->shared = rand(1,100);
            
            $user = User::find($resource->user_id);
            if(isset($user->id)){
                $user_path = $user->profile();
                $resource->username = $user->username;
                $user = $resource->User;
                $userArray = array_merge($user_path->toArray(),$user->toArray());
                $userArray['username'] = $this->getUserName($userArray['username'],$user->id);
                $resource->user_info = $userArray;
            }else{
                $resource->username = 'N/A';
            }
        }
        /*$resources = Resource::where($where)
            ->limit($limit)
            ->skip($skip)
            ->orderBy('created_at', 'desc')
            ->get();
        */
        return $this->setResponse($resources, 'success', 'OK', '200', '', '');
    }

    public function slugAuthors(Request $request){
        $authors_select =DB::table('users')->select('id')->groupBy('id')->where([['username', 'like', '%' . $request->slug . '%']])->get();
        $authors_array = [];
        $authors_id = DB::table('resources')->select('user_id')->where([['published',True]])->groupBy('user_id')->get();
        
        foreach ($authors_select as $aut){
            array_push($authors_array,$aut->id);
            
        }
        $array_ids = [];
        foreach ($authors_id as $ids){
            
            if(in_array($ids->user_id,$authors_array)){
                array_push($array_ids,$ids->user_id);
            }
            
            
        }
        $authors = User::whereIn('id', $array_ids)->select('id','username')->get();
        foreach ($authors as $auth){
            $profile = $auth->profile();
            $auth->profile = $profile;
        }
        return $this->setResponse($authors, 'success', 'OK', '200', '', '');
    }

    public function new_store(Request $request){
        /*$request->validate([
            'title' => 'required|max:50',
            'categories' => 'array|min:1|max:2',
            'slug' =>'required',
            'content' =>'required',
            'attachment.*' => 'required|mimes:pdf,doc,docx,ppt,pptx,rtf,txt|max:10240',
        ], [
            'categories' => 'Debes elegir al menos una categoría'
        ], [
            'title' => 'titulo',
            'categories' => 'categoria',
        ]);*/
     
        return $this->attachFile($request);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {

        $user = Auth::user()->email;
        if (!$request->has('step')) {
            return $this->setResponse([], 'error', '', '422', '', 'No step defined');
        }

        switch ($request->step) {
            case 1:
                $request->validate([
                    'title' => 'required|max:50',
                    'categories' => 'array|min:1|max:2',
                ], [
                    'categories' => 'Debes elegir al menos una categoría'
                ], [
                    'title' => 'titulo',
                    'categories' => 'categoria',
                ]);
                
                return $this->setResponse([], 'success', 'OK', '200', '', '');
                break;
            case 2:
                $path = null;

                $request->validate([
                    'editor_content' => 'required|min:20',
                ], [
                    'editor_content.required' => 'Debes escribir un resumen con 20 caracteres como mínimo',
                    'editor_content.min' => 'Debes escribir un resumen con 20 caracterés como mínimo',
                ], [
                    'editor_content' => 'resumen',
                ]);

                if ($request->hasFile('attach')) {
                    $this->validate($request,
                        ['attach.*' => 'required|mimes:pdf,doc,docx,ppt,pptx,rtf,txt|max:10240',],
                        [
                            'attach.required' => 'El archivo es requerido',
                            'attach.mimes' => 'El tipo de archivo es inválido',
                            'attach.max' => 'El archivo no puede ser superior a de 10MB',
                        ],
                        ['attach' => 'archivo']
                    );

                    $path = Storage::putFileAs('temp_resource_file', $request->file('attach'), uniqid() . '.' . $request->attach->getClientOriginalExtension());
                }

                Mail::to('pedro.alegriia@gmail.com')->send(new \App\Mail\ResourceCreated('pedro_alegriia@outlook.com'));
                return $this->setResponse(['path' => $path], 'success', 'OK', '200', '', '');
                break;
            case 3:
                return $this->attachFile($request);
                break;
        }

        return null;
    }

    public function findOrCreateTags($tags, $decoded = true)
    {
        if ($decoded) {
            $tags_ids = collect($tags)->map(function ($tag) {
                if (is_array($tag) && key_exists('id', $tag)) {
                    return $tag['id'];
                } elseif (is_array($tag) && !key_exists('id', $tag)) {
                    $new_tag = Tag::firstOrCreate(['label' => $tag['label']], ['slug' => str_replace(' ', '-', $tag['label'])]);
                    return $new_tag->id;
                } elseif (is_string($tag)) {
                    $new_tag = Tag::create([
                        'label' => $tag,
                        'slug' => str_replace(' ', '-', $tag),
                    ]);
                    return $new_tag->id;
                }
            });

            return $tags_ids;
        }

        $tags = collect(json_decode($tags));
        $tags_ids = $tags->map(function ($tag) {
            if (is_object($tag)) {
                $tag = Tag::firstOrCreate(
                    ['label' => $tag->label],
                    ['slug' => str_replace(' ', '-', $tag->label)]
                );
                return $tag->id;
            } else if (is_string($tag)) {
                $tag = Tag::firstOrCreate(
                    ['label' => $tag],
                    ['slug' => str_replace(' ', '-', $tag)]
                );
                return $tag->id;
            }
        });

        return $tags_ids;
    }

    public function attachFile(Request $request)
    {
        $user = User::find(Auth::user()->id);

        if($request->id == 0){
            $resource = Resource::create([
                'user_id' => $user->id,
                'title' => $request->title,
                'slug' => $request->slug,
                'video_url' => ($request->video_url && $request->video_url != 'null') ? $request->video_url : null,
                'content' => $request->content, 
            ]);
            
            $array_categ = explode(",", $request->categories);
            $resource->categories()->sync($array_categ);
           
            //$tags = $this->findOrCreateTags($request->tags, false);
            //$resource->tags()->sync($tags);
            
            //$this->auth->notify(new ResourceCreated($resource));
            //$this->notifyPublishers($resource);
            $filename = $request->attachment->getClientOriginalName();
            $path = 'docs/'.$filename;
            $result = Storage::disk('s3')->put('docs', $request->file('attachment'));
            //Storage::putFileAs('temp_resource_file', $request->file('attachment'), uniqid() . '.' . $request->attachment->getClientOriginalExtension());
            try {
                if (isset($result)) {
                    $resource->attachment = $result;
                    $resource->save();
                   
                }
            } catch (\Exception $exception) {
                app('log')->info($exception->getMessage());
                return $this->setResponse([], 'error', 'ERROR', '409', 'Mensaje de error',
                    'Ocurrio un problema al intentar guardar el archivo adjunto.');
            }
            //Mail::to('pedro.alegriia@gmail.com')->send(new \App\Mail\ResourceCreated('pedro_alegriia@outlook.com'));
            //Mail::to($user->email)->send(new \App\Mail\ResourceCreated($user,$resource));

            Mail::to($user->email)->send(new \App\Mail\ResourceCreated($user,$resource));
            return $this->setResponse([], 'success', 'OK', '200', 'Mensaje de éxito',
                'Recurso creado con éxito.');
                
           // Mail::to('pedro.alegriia@gmail.com')->send(new \App\Mail\ResourceCreated('no-replay@savas.live'));
        }else{
            $resource = Resource::find($request->id);
            $resource->title = $request->title;
            $resource->slug =$request->slug;
            $resource->video_url =($request->video_url && $request->video_url != 'null') ? $request->video_url : null;
            $resource->content =$request->content;
            $resource->save();
            
            $array_categ = explode(",", $request->categories);
            $resource->categories()->sync($array_categ);
            $result = Storage::disk('s3')->put('docs', $request->file('attachment'));
            try {
                if (isset($result)) {
                    $resource->attachment = $result;
                    $resource->save();
                    
                }
            } catch (\Exception $exception) {
                app('log')->info($exception->getMessage());
                return $this->setResponse([], 'error', 'ERROR', '409', 'Mensaje de error',
                    'Ocurrio un problema al intentar guardar el archivo adjunto.');
            }
            
            return $this->setResponse([], 'success', 'OK', '200', 'Mensaje de éxito',
                'Recurso actualizado con éxito.');

        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param $param
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($param)
    {
        $resource = Resource::with('tags', 'likesCount', 'user.profile', 'downloads', 'categories');

        if (is_int($param)) {
            $resource->where('id', $param);
        } else {
            $resource->where('slug', $param);
        }

        $resource = $resource->first();

        if (!$resource) {
            return response()->json(['status' => 0, 'message' => 'Recurso no encontrado'], 404);
        }

        $likedByAuthUser = DB::table('resource_likes')
            ->where('resource_id', $resource->id)
            ->where('user_id', auth()->id())
            ->whereNull('deleted_at')
            ->first();

        $resource->userLike = (boolean)$likedByAuthUser;
        $resource->avatar = $resource->user->getFirstMediaUrl('profiles');

        return response()->json($resource, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ResourceDeleteRequest $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(ResourceDeleteRequest $request, $id)
    {
        $resource = Resource::find($id);

        if ($resource) {
            return response()->json($resource->delete(), 200);
        } else {
            return response()->json('Error', 500);
        }
    }

    public function upload(ResourceUploadRequest $request, $id)
    {
        $resource = Resource::find($id);
        $previous_attachment = $resource->attachment;

        $file = $request->file('resource');

        $extension = $file->guessExtension();
        $filename = $resource->slug . '_' . uniqid($id) . '.' . $extension;

        $path = storage_path('resources_files/');

        $file->move($path, $filename);

        if ($file) {
            $resource->attachment = $filename;
            $resource->save();

            if ($previous_attachment) {
                Storage::delete('resources_files/' . $previous_attachment);
            }

            return response()->json(['attachment' => $resource->attachment], 200);
        } else {
            return response()->json($file, 500);
        }
    }

    public function file($id)
    {
        
        $resource = Resource::find($id);
        $resource->downloads()->firstOrCreate([
            'user_id' => Auth::id() ?? 0,
        ]);
        /* $path =Storage::disk('s3')->get($resource->attachment);
        $header = [
            'Content-Type' => 'application/*',
        ];
        return response()->download($path, 'doc', $header); */

        $headers = [
            'Content-Type'        => 'Content-Type: application/*',
            'Content-Disposition' => 'attachment; filename="'. $resource->attachment .'"',
        ];

        return Storage::disk('s3')->download($resource->attachment);        
    }

    public function like($id)
    {
        $resource = Resource::find($id);
        $like = $resource->likes()->where('user_id', Auth::id())->first();

        if ($like) {
            $like->delete();
            $message = "Has quitado un like";
            $action = 'unlike';
        } else {
            $resource->likes()->create(['user_id' => Auth::id()]);
            $message = "Gracias por tu like";
            $action = 'like';
        }

        return response()->json(['status' => 1, 'message' => $message, 'action' => $action]);
    }

    public function addToCollection(ResourceAddToCollectionRequest $request, $id)
    {
        $resource = Resource::find($id);
        $collection = Collection::find($request->collection_id);

        $collection->resources()->save($resource);
        return response()->json('', 200);
    }

    public function remove(Request $request)
    {
        if (Auth::check()) {
            $userInfo = Auth::user();
            $input = $request->input();
            $validator = Validator::make($input, [
                'id' => 'required'
            ]);
            if ($validator->fails()) {
                $rv = array(
                    "status" => 5000,
                    "data" => $validator->messages()
                );
                return json_encode($rv, true);
            }
            //=================
            // Check
            //=================
            $resourseModel = new Resource();
            $check = $resourseModel
                ->where('id', $input['id'])
                ->where('user_id', $userInfo['id'])
                ->get()->first();
            if ($check != null) {
                $resourseModel = new Resource();
                $resourseModel
                    ->where('id', $input['id'])
                    ->where('user_id', $userInfo['id'])
                    ->delete();
                $rv = array(
                    'status' => 2000,
                    'data' => 'Resource has been removed successfully'
                );
                return json_encode($rv, true);

            } else {
                $rv = array(
                    'status' => 5000,
                    'data' => 'Invalid Request'
                );
                return json_encode($rv, true);
            }

        } else {
            $rv = array(
                'status' => 5000,
                'data' => 'Authentication failed.'
            );
            return json_encode($rv, true);
        }
    }

    public function getLatestResources()
    {
        $latestResources = Resource::with('categories')
            ->published()
            ->orderBy('id', 'desc')
            ->limit(5)
            ->get();

        return response()->json(['status' => 1, 'latest' => $latestResources], 200);
    }

    public function getLatestResourcesByAuthor($id)
    {
        $resourcesCount = Resource::published()->publishedBy($id)->count();
        $latestResources = Resource::with(['categories', 'tags'])
            ->published()
            ->publishedBy($id)
            ->orderBy('id', 'desc')
            ->limit(5)
            ->get();

        return response()->json(['status' => 1, 'latest' => $latestResources, 'count' => $resourcesCount], 200);
    }

    public function getRankedResourcesByAuthor(Request $request, $id)
    {
        $rankedResources = User::find($id)
            ->rankedResources()
            ->with(['categories', 'tags'])
            ->orderBy('id', 'desc');

        $rankedCount = $rankedResources->count();

        if ($request->limit) {
            $rankedResources->limit($request->limit);
        }

        $rankedResources = $rankedResources->get();

        return response()->json(['status' => 1, 'ranked' => $rankedResources, 'count' => $rankedCount], 200);
    }

    public function getAds()
    {
        $ad = Ad::where('starts_at', '<=', Carbon::now())
            ->where('ends_at', '>=', Carbon::now())
            ->orderByRaw("RAND()")
            ->first();

        if ($ad) {
            $ad->update(['impressions' => $ad->impressions + 1]);
        }

        return response()->json(['status' => 1, 'ad' => $ad], 200);
    }

    public function linker($uuid)
    {
        $ad = Ad::where('uuid', $uuid)->firstOrFail();
        $ad->click();

        return redirect($ad->link);
    }

    public function searchTags(Request $request)
    {
        $tags = Tag::where('label', 'like', "%{$request->search}%")->get();
        return response()->json(['status' => 1, 'tags' => $tags], 200);
    }

    public function searchAuthors(Request $request)
    {
        $authors = UserProfile::with('user.resources')
            ->where('fullname', 'like', "%{$request->search}%")
            ->limit(25)
            ->get();

        $response = collect([]);
        $authors->each(function ($author) use ($response) {
            if (count($author->user->resources)) {
                $response->push($author);
            }
        });

        return response()->json(['status' => 1, 'authors' => $response], 200);
    }

    public function getAuthor($user_id)
    {
        $user = User::with('profile')->findOrFail($user_id);

        return response()->json(['status' => 1, 'author' => $user], 200);
    }

    public function fileDelete(Request $request)
    {
        $resource = Resource::find($request->resource_id);
        $media = $resource->getMedia('resources');
        $media[0]->delete();

        return response()->json(['status' => 1, 'message' => 'El archivo ha sido eliminado'], 200);
    }

    public function newUpdate(Request $request)
    {
        $user = User::find($request->user_id);

        $resource = Resource::findOrFail($request->id);

        $resource->update([
            'title' => $request->title,
            'video_url' => $request->video_url,
            'published' => 0,
            'content' => $request->editor_content,
        ]);

        $resource->categories()->sync(collect(json_decode($request->categories))->pluck('id')->toArray());

        $tags = $this->findOrCreateTags($request->tags, false);
        $resource->tags()->sync($tags);

        try {
            if ($request->has('file_path')) {
                $given_name = $resource->addMedia(storage_path('app/') . $request->file_path)
                    ->toMediaCollection('resources', 's3');
                $resource->attachment = $given_name;
            }

            $resource->save();
        } catch (\Exception $exception) {
            return $this->setResponse([], 'error', 'ERROR', '409', 'Mensaje de error',
                'Ocurrio un problema al intentar guardar el archivo adjunto.');
        }

        $user->notify(new ResourceUpdated($resource));
        $this->notifyPublishers($resource);

        return $this->setResponse([], 'success', 'OK', '200', 'Mensaje de éxito',
            'Recurso creado con éxito.');
    }

    public function downloadContent($slug)
    {
        $resource = Resource::where('slug', $slug)->firstOrFail();

        return view('dashboard.resources.download', compact('resource'));
    }

    public function downloadAttachment($slug)
    {
        $resource = Resource::where('slug', $slug)->firstOrFail();
        $resource->downloads()->firstOrCreate([
            'user_id' => auth()->id() ?? 0,
        ]);
        
        $exists = $resource->getFirstMedia('resources');

        if (!$exists) {
            return null;
            return view('dashboard.resources.download', compact('resource'));
        }

        return $resource->getFirstMedia('resources');
    }

    public function getResourcesByQuery(Request $request)
    {
        $resources = Resource::where('title', 'like', "%{$request->search}%")->get();

        return response()->json(['status' => 1, 'resources' => $resources], 200);
    }
}
