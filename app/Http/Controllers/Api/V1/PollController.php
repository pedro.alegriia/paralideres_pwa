<?php

namespace App\Http\Controllers\Api\V1;

use Auth;
use Carbon\Carbon;
use App\Models\Poll;
use App\Models\User;

use App\Models\PollOption;
use App\Models\PollVote;
use App\Service\CommonService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Poll\PollCreateRequest;
use App\Http\Requests\Poll\PollVoteRequest;
use stdClass;
class PollController extends Controller
{
    use CommonService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        /*$this->middleware('auth:api', [
            'except' => [
                'index',
                'show',
                'last'
            ]
        ]);*/
    }

    public function index()
    {
        return response()->json(Poll::simplePaginate(10));
    }

    public function show($id)
    {
        return response()->json(Poll::with('options')->findOrFail($id), 200);
    }
    public function getActivePoll(Request $request)
    {
        $user = User::find(Auth::user()->id);
        $poll = Poll::where([['active','=',True]])->first();
        $hasVoted = Auth::user()->pollVote()
            ->where('poll_id', $poll->id)
            ->first();
        if(isset($poll->id)){
            if($hasVoted == null){
                $poll->hasVoted = False;
            }else{
                $poll->hasVoted = True;
            }
            
            $poll->Options;
            return $this->setResponse($poll, 'success', 'OK', '200', '', '');

        }else{
            return $this->setResponse([], 'success', 'OK', '200', '', '');
        }
    }
    public function last(Request $request)
    {
        $today = Carbon::now()->format('Y-m-d');
        $poll = Poll::with(
            [
                'options' => function ($query) {
                    $query->where('option', '<>', '');
                }
            ]
        )->orderBy('created_at', 'asc')
            ->where('active', '=', '1')
            ->whereDate('date_from', '<=', $today)
            ->whereDate('date_to', '>=', $today)
            ->first();

        if ($poll) {
            foreach ($poll->options as $option) {
                $option->count = PollVote::where('poll_id', $poll->id)
                    ->where('poll_options_id', $option->id)
                    ->count();
            }

            $poll->voted = false;
            if (Auth::check()) {
                $voted = $poll->votes->where('user_id', $request->user()->id);
                $poll->voted = (boolean)count($voted);
            }
        }

        return $this->setResponse($poll, 'success', 'OK', '200', '', '');
    }

    public function store(PollCreateRequest $request)
    {
        $poll = Poll::create([
            'question' => $request->question,
            'date_from' => $request->date_from,
            'date_to' => $request->date_to,
            'active' => $request->active
        ]);

        $options = array_map(function ($option, $key) {
            return new PollOption([
                'option' => $option,
                'index' => $key
            ]);
        }, $request->options, array_keys($request->options));

        $poll->options()->saveMany($options);

        $poll->load('options');

        return response()->json($poll, 200);
    }

    public function update(PollCreateRequest $request, $id)
    {
        $poll = Poll::find($id);

        $poll->question = $request->question;
        $poll->date_from = $request->date_from;
        $poll->date_to = $request->date_to;
        $poll->active = $request->active;

        $poll->options()->delete();

        $options = array_map(function ($option, $key) {
            return new PollOption([
                'option' => $option,
                'index' => $key
            ]);
        }, $request->options, array_keys($request->options));

        $poll->options()->saveMany($options);
        $poll->save();
        $poll->load('options');

        return response()->json($poll, 200);
    }
    public function sendPoll(Request $request)
    {
        $user = User::find(Auth::user()->id);
        $hasVoted = Auth::user()->pollVote()
            ->where('poll_id', $request->poll_id)
            ->first();
        $message = "Tu voto ya habia sido registrado";
        if (!$hasVoted) {
            $pollVote = new PollVote([
                'poll_id' => $request->poll_id,
                'poll_options_id' => $request->poll_options_id
            ]);

            Auth::user()->pollVote()->save($pollVote);
            $message = "Gracias por tu voto!";
        }

        return response()->json(['status' => 1, 'message' => $message], 200);
    }
    public function GetStatisticsPoll()
    {
        $statistics = PollVote::where([['poll_id','=',1]])->get();
        $data = new stdClass;
        $total_polls = 0;
        foreach($statistics as $statistic){
            $detail = new stdClass;
            $name = $statistic->poll_options_id;
            if(isset($data->$name)){
                $detail = $data->$name;
                $detail->number = $detail->number+1;
                $data->$name = $detail;
            }else{
                $detail->number = 1;
                $option = PollOption::find($name);
                $detail->name = $option->option;
                $data->$name = $detail;
            }
            $total_polls = $total_polls+1;
        }
        foreach($data as $dt){
            $dt->porcent = round((($dt->number*100)/$total_polls),2);
        }
        return response()->json(['status' => 1, 'data' => $data], 200);

    }
    public function vote(PollVoteRequest $request, $id)
    {
        $hasVoted = Auth::user()->pollVote()
            ->where('poll_id', $id)
            ->first();

        $message = "Tu voto ya habia sido registrado";

        if (!$hasVoted) {
            $pollVote = new PollVote([
                'poll_id' => $id,
                'poll_options_id' => $request->option
            ]);

            Auth::user()->pollVote()->save($pollVote);
            $message = "Gracias por tu voto!";
        }

        return response()->json(['status' => 1, 'message' => $message], 200);
    }

    public function result($id)
    {
        $poll = Poll::with('options.votes', 'options.votes.user')->findOrFail($id);
        return $this->setResponse($poll, 'success', 'OK', '200', 'Vota el éxito', 'Gracias! Tu voto ha sido enviado');
    }
}
