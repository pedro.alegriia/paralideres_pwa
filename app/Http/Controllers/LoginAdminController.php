<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service\CommonService;

use App\Models\User;
use App\Models\UserProfile;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use App\Http\Requests\Auth\AuthenticateRequest;
use Illuminate\Support\Facades\Log;
use Spatie\Permission\Models\Role;
use Session;
use Validator,Redirect,Response;
use App\Models\Resource;
use Illuminate\Support\Facades\Password;

use Tymon\JWTAuth\Exceptions\JWTException;

use  JWTAuth;
use \stdClass;

class LoginAdminController extends Controller
{
   
       public function loginAdminView()
       {
            return view('/auth.loginAdmin');
       }


       private function guard()
       {
           return Auth::guard();
       }
   
       public function loginAdmin(Request $request)
       {
           $input = $request->input();
           $user = $request->only('email', 'password');
           if (!$user) {
            return redirect('admin_login')->with('error','Email-Address And Password Are Wrong.');
           }
           if ($user) {
               $credentials = $request->only('email', 'password');
   
               if ($this->guard()->attempt($credentials, true)) {
                   //$user = Auth::user();

                  // return Redirect::to("admin/encuestas")->withSuccess('Oppes! You have entered invalid credentials');
                   return view('dashboard.index');
                   //return redirect('admin/encuestas');
               } else {
                return redirect('admin_login')->with('error','Email-Address And Password Are Wrong.');
                
               }
           } else {
            
               return redirect()->back()->with('error', 'Err.');
              
           }
   
       }



    public function loginAdmin2(Request $request) {


        try {


            $response = new stdClass;
            $data = new stdClass;
            //validate incoming request 
            $this->validate($request, [
                'email' => 'required|string',
                'password' => 'required|string',
            ]);

            $credentials = $request->only(['email', 'password']);
                $jwt_token = null;
            if (! $jwt_token = JWTAuth::attempt($credentials)) {
                return response()->json(['message' => 'Unauthorized'], 401);
            }
            $user = User::where([['email',$request->input('email')]])->first();


          //  $permission_get =  DB::table('permissions')->get();
           // $model_has_permissions = DB::table('model_has_permissions')->where('model_id', $user->id)->get();
            
           $model_has_permissions = user::query()
           ->with(array('permissions' => function($query) {
               $query->select('id','name');
           }))
           ->get();

          // return route('admin/encuestas');
            //$model_has_permissions = User::with('permissions')->get();
            //$model_has_permissions = user::with(['permissions', 'roles'])->get();
           // $response->success  = True;
            //$response->message  = "Succes login";
            //$user->token        = $jwt_token;
            //$user->token_type   = 'Bearer';
            //$data->user         = $user;
            //$data->permission         = $model_has_permissions;
           // $response->data = $data;
            //return response()->json($response);
            //$this->respondWithToken($token);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e,
                'entity' => 'login',
                'action' => 'login',
                'result' => 'failed'
            ], 409);
        }

        
    }


       
}
