<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class CollectionController extends Controller
{
    public function index()
    {
        return view('collections.index');
    }

    public function show(Request $request, $slug)
    {
        $data['resource_header'] = true;
        //$user_id = ($this->auth) ? $this->auth->id : null;

        $data['collection'] = Collection::with([
            'categories',
            'resources',
        ])->where('slug', $slug)
            ->firstOrFail();

        /*if ($request->user()) {
            $data['resource']->read()->firstOrCreate([
                'user_id' => $user_id,
            ]);
        }*/

        /*$author = User::with('profile')
            ->where('id', $data['resource']->user_id)
            ->get()
            ->toArray();*/

        /*if (count($author)) {
            $data['author'] = $author[0];
        }*/

        /*$data['latestResources'] = Co::with('categories')
            ->orderBy('id', 'desc')
            ->limit(5)
            ->get();*/

        //$data['tags'] = Tag::orderBy('id', 'desc')->get();

        return view('collections.show')->with($data);
    }

    public function search(Request $request)
    {
        //dd($request->all());
        //return Cache::remember($request->getQueryString(), 30, function () use ($request) {
        $page = isset($request->page) ? $request->page : 1;
        $limit = 12;
        $offset = ($page - 1) * $limit;

        $collections = Collection::onlySearch($request->search);

        if ($request->has('author')) {
            $resources = $collections->publishedBy($request->author);
        }

        /*if ($request->has('category') && $category_id) {
            $resources->join('resource_category', function ($join) use ($category_id) {
                $join->on('resources.id', '=', 'resource_category.resource_id')
                    ->where('resource_category.category_id', $category_id);
            })->select('resources.*');
        } else if ($request->has('category') && !$category_id) {
            return response()->json(['status' => 0, 'resources' => null], 200);
        }*/

        $count = $collections->count();
        $collections = $collections
            ->with([
                'categories',
                //'resources',
            ])->limit($limit)
            ->offset($offset)
            ->orderBy('created_at', 'desc')
            ->get();

        app('log')->info("Count: {$count}");

        return response()->json(['status' => 1, 'collections' => $collections, 'count' => $count, 'page' => $page], 200);
        //});
    }
}
