<?php

namespace App\Http\Controllers\Web;

use App\CollectionCategory;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Collection;
use App\Models\User;
use Freshbitsweb\Laratables\Laratables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminCollectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $collections = Collection::with(['categories', 'user'])->paginate(15);
        return view('dashboard.collections.index', compact('collections'));
    }

    public function getDatatable(Request $request)
    {
        return Laratables::recordsOf(Collection::class);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('dashboard.collections.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $collection = Auth::user()->collections()->create([
            'label' => $request->label,
            'slug' => str_random(5) . '-' . str_replace(' ', '-', $request->label),
            'description' => $request->description,
        ]);

        $collection->categories()->sync($request->categories);

        return redirect()->to('admin/collections')->with('success', 'La colección ha sido creada');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $slug
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $categories = Category::all();

        $collection = Collection::where('slug', $slug)
            ->with(['user', 'categories'])
            ->firstOrFail();

        return view('dashboard.collections.edit', compact('categories', 'collection'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $collection = Collection::findOrFail($id);

        $collection->update([
            'label' => $request->label,
            'description' => $request->description,
        ]);

        $collection->categories()->sync($request->categories);

        return redirect()->route('collections.index')->with('success', 'La colección fue actualizada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function resources($collection_id)
    {
        $collection = Collection::find($collection_id);

        return view('dashboard.collections.resources', compact('collection'));
    }
}
