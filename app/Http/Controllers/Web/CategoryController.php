<?php

namespace App\Http\Controllers\Web;

use App\Models\Category;
use App\Service\CommonService;
use App\Http\Controllers\Controller;
use App\Http\Requests\Category\CategoryCreateRequest;
use Freshbitsweb\Laratables\Laratables;


class CategoryController extends Controller
{
    use CommonService;

    protected $auth;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        $categories = Category::all();
      //  $categories = Category::visible()
        //    ->orderBy('label')
          //  ->get();
        return view('dashboard.category.index')->with(compact('categories'));
    }

 
    public function index()
    {
        $categories = Category::with('collections')
            ->visible()
            ->orderBy('label')
            ->get();

        return response()->json(['status' => 1, 'categories' => $categories], 200);
    }

    public function edit($id)
    {
         $category = Category::find($id);
     
        return view('dashboard.category.edit')->with(compact('category'));
    }

    public function store(CategoryCreateRequest $request)
    {
        $category = Category::create([
            'label' => $request->label,
            'slug' => $request->slug,
            'description' => $request->description
        ]);

        return response()->json($category, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param $param
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
          $category = Category::find($id);
        //$response = Category::where('slug', $param)->firstOrFail();
        //$response->resources = $response->resources()->simplePaginate(15);

          return view('dashboard.category.show')->with(compact('category'));
        //return response()->json($response, 200);
    }

    public function resources($param)
    {
        $user_id = ($this->auth) ? $this->auth->id : null;
        $response = Category::where('slug', $param)
            ->firstOrFail()
            ->resources($user_id)
            ->Paginate(15);

        return $this->setResponse($response, 'success', 'OK', '200', '', '');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CategoryCreateRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryCreateRequest $request, $id)
    {
        $category = Category::findOrFail($id);
        $category->label = $request->label;
        $category->slug = $request->slug;
        $category->description = $request->description;
        $category->save();

        return response()->json($category, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return response()->json(Category::findOrFail($id)->delete(), 200);
    }
}
