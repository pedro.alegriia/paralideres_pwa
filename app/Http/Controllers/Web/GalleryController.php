<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Freshbitsweb\Laratables\Laratables;
use App\Gallery;
use Illuminate\Support\Facades\DB;

class GalleryController extends Controller
{
    public function __construct()
    {
        //only-admin
    }

    public function index()
    {
        $gallery = Gallery::all();
        return view('dashboard.gallery.index')->with(compact('gallery'));
    }

    //create DataTable with Laratable Custom Action
    public function getDatatable()
    {
        return Laratables::recordsOf(Gallery::class, null);
    }

    public function show($id){

        $gallery = Gallery::with('media')->find($id);
        $gallery['image'] = $gallery->media[0]->getUrl();
        return view('dashboard.gallery.show');
    }

    /**
     * Return all gallery images active
     */
    public function order()
    {

        /***
        $query_user = Gallery::where(['status'=> true, 'type' => 'user'])
        ->get();

        $gallery_user = $query_user->transform(function ($item, $key) {
            $item['image'] = $item->media[0]->getUrl();
            return $item;
        });

        $query_visitant =  Gallery::with('media')
        ->where(['status'=> true, 'type' => 'visitant'])
        ->get();

        $gallery_visitant = $query_visitant->transform(function ($item, $key) {
            $item['image'] = $item->media[0]->getUrl();
            /* dd($item);

            return $item;
        });
        **/

        return view('dashboard.gallery.order');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //order
        return view('dashboard.gallery.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate image
        $request->validate([
            'image' => 'dimensions:min_width=1356,min_height=516
            |mimes:jpeg,jpg,png
            |required
            |max:5000',
            'type' => 'required',
            'status' => 'required',
        ]);
        //custom validation response message

        $image = new Gallery();
        $image->image = $request->image;
        $image->status = $request->status;
        $image->type = $request->type;


        $image->addMediaFromRequest('image')
        ->toMediaCollection('gallery');

        $image->save();
        //notify changes

        return redirect()->to('gallery.index')->with('success', 'Imagén creada correctamente');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $slug
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $image = Gallery::with('media')
            ->find($id);
        $image->image = $image->media[0]->getUrl();
        return view('dashboard.gallery.edit', compact( 'image' ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'image' => 'dimensions:min_width=1356,min_height=516
            |mimes:jpeg,jpg,png
            |required
            |max:5000',
            'type' => 'required',
            'status' => 'required',
        ]);

        $gallery = Gallery::findOrFail($id);

        $gallery->update([
            'type' => $request->type,
            'status' => $request->status,
        ]);

        //update image from the request
        if ($request->has('image') && $request->file('image')->isValid()) {
            $gallery->getFirstMedia('gallery')->delete();
            $gallery->addMediaFromRequest('image')
                ->toMediaCollection('gallery', 's3');
        }
        return redirect()->route('gallery.index')->with('success', 'Actualización exitosa');
    }

    public function change($id){
        $gallery = Gallery::findOrFail($id);

        $gallery->update([
            'status' => $gallery->status ? false: true,
        ]);
        return redirect()->route('gallery.index')->with('success', 'Actualización exitosa');
    }

    public function destroy($id){

    }
}
