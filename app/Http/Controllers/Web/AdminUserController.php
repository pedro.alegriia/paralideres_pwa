<?php

namespace App\Http\Controllers\Web;

use App\User;
use App\Notifications\Users\AvatarChangeResponse;
use Freshbitsweb\Laratables\Laratables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Traits\HasRoles;
use Auth;
use phpDocumentor\Reflection\Types\Null_;

class AdminUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function index()
    {

        
        $users= DB::table('users')->select('users.id', 'users.username','users.is_active', 'users.email','roles.id as role_id', 'roles.name as role')
        ->join('model_has_roles', function ($join) {
           // $join->on('users.id', 'model_has_roles.model_id');
                 //->where('model_has_roles.model_type', User::class);
        }
        )
        ->join('roles', 'model_has_roles.role_id', 'roles.id')
       // ->where(['roles.username' == 0])
        ->paginate(15);
        

       // $users = user::with(['roles'])->paginate(15);


        //return $model_has_permissions = user::query()
        //->with(array('roles' => function($query) {
          //  $query->select('id','name');
        //}))
        //->get();

    
        //return User::join('model_has_roles', 'users.id', '=', 'model_has_roles.model_id')
                        //->join('roles', 'model_has_roles.role_id', '=', 'roles.id')
                        //->where('model_type', '=', 'App\\User');
//dd($users);
        return view('dashboard.user.index', compact('users'));
    }

    public function getDatatable(Request $request)
    {
        return Laratables::recordsOf(User::class);
    }



    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function edit($id)
    {
        $user = User::find($id);


            $rolesByUser = DB::table('model_has_roles')->where([['model_id', '=', $id]])->first();

            $permissionByUser = DB::table('model_has_permissions')->where([['model_id', '=', $id]])->get();
            

        //$rolesByPermission = Permission::where('role_id','=',$rolesByUser)->get();
          //  $rolesByPermission =   DB::table('model_has_permissions')->where('role_id',$rolesByUser)->first();
            //$roles = $user->getRoleNames();
//$array = j//son_decode($rolesByPermission);
       // $role = Role::find($id);
           // $rolePermissions = Permission::join("role_has_permissions","role_has_permissions.permission_id","=","permissions.id")
               // ->where("role_has_permissions.role_id",$id)
                //->get();
      // dd($rolesByUser);

        $roles = Role::all();
        $permission_groups = Permission::get()->groupBy('category');
//dd($permissionByUser);
        return view('dashboard.user.edit', compact('user','roles','permission_groups','rolesByUser','permissionByUser'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        //dd($request);
/***
        dd($request);
        $user = User::findOrFail($id);
       //dd($request->roles);
        if ($request->roles) {
            DB::table('model_has_roles')->where('model_id',$id)->delete();


          DB::table('model_has_roles')->insert([
            'role_id' => $request->roles,
            'model_type' => 'App\User',
            'model_id' =>  $request->user_id,
        ]);

* */
        $user = User::find($id);
       // $user->update($input);

       DB::table('model_has_roles')->where('model_id',$id)->delete();

       DB::table('model_has_roles')->insert([
        'role_id' => $request->roles,
        'model_type' => 'App\Models\User',
        'model_id' => $request->model_id,
    ]);

    $user->permissions()->detach();
    if ($request->filled('permissions')) {
        $user->givePermissionTo($request->permissions);
    }
  //$otro =   DB::table('role_has_permissions')->where('role_id',$id)->get();

    //dd($otro);
    //DB::table('model_has_roles')->insert([
      //  'role_id' => $request->roles,
       // 'model_type' => 'App\Models\User',
        //'model_id' => $request->model_id,
    //]);
     //  DB::table('model_has_roles')
       //   ->where('model_id', $request->roles)
         //   ->update();
           // $user->assignRole($request->input('roles'));
           // $user->assignRole($request->roles);
          //  $role_name =DB::table('model_has_roles')->where([['id','=',$roles->role_id]])->first();
           // $user->role_id=$roles->role_id;
            //$user->removeRole($role_name->name);
            //$user->assignRole($request->roles);



       
     
        //$user->save();
 //dd($request);
            //$user->assignRole($request->roles);
       // }
/****
        if ($request->roles) {
            $this->removeAllRolesToUser($user);
            collect($request->roles)->each(function ($elem, $index) use ($user) {
                $user->assignRole(str_replace("'", "", $index));
            });
        }

        if ($request->permissions) {
            $this->removeAllPermissionsToUser($user);
            collect($request->permissions)->each(function ($elem, $index) use ($user) {
                $user->givePermissionTo(str_replace("'", "", $index));
            });
        }

        $user->is_active = $request->status;
        if ($request->is_active && !$user->verified) {
            $user->verified = true;
        }
*/

      

        return redirect('/admin/usuarios');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function removeAllRolesToUser($user)
    {
        foreach ($user->getRoleNames() as $role) {
            $user->removeRole($role);
        }

        return true;
    }

    public function removeAllPermissionsToUser($user)
    {
        foreach ($user->getAllPermissions() as $permission) {
            $user->revokePermissionTo($permission);
        }

        return true;
    }

    public function manageAvatar(Request $request)
    {
        $user = User::with('tempAvatar')->findOrFail($request->user_id);

        if ($request->status === 'accept') {
            $user->addMediaFromUrl($user->tempAvatar->getFirstMediaUrl('temp_avatars'))
                ->toMediaCollection('profiles', 's3');
        }

        $user->update(['default_avatar' => null]);
        $user->tempAvatar->delete();
        $user->notify(new AvatarChangeResponse($request->status, $request->comment));

        return redirect()->back()->with('success', 'Los cambios han sido guardados');
    }


      //Model route binding
   public function showEmail(){

    //$passwordRecoveryQuestion = PasswordRecoveryQuestion::where('id',$passwordRecoveryQuestion)->get();
    //debug("object",$passwordRecoveryQuestion);
    //$passwordRecoveryQuestion->load('questions.answer');
    return view('auth.passwords.reset');
}
}
