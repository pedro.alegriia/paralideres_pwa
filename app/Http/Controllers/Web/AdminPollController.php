<?php

namespace App\Http\Controllers\Web;

use App\Models\Poll;
use App\Models\PollOption;
use App\Models\User;
use Freshbitsweb\Laratables\Laratables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Auth;
class AdminPollController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function index()
    {
        if (Auth::check())
        {
            $encuesta = Poll::orderBy('date_from','DESC')->get();
            return view('dashboard.polls.index')->with(compact('encuesta'));
        }
      
    }

    public function getDatatable(Request $request)
    {
        return Laratables::recordsOf(Poll::class);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.polls.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {


        if ($request) {
            $getPoll = Poll::latest('question')->first();
            $getPoll->active = 0;
            $getPoll->save();

            $poll = Poll::create([
                'question' => $request->question,
                'date_from' => $request->date_from,
                'date_to' => $request->date_to,
                'active' => 1,
                'former_id' => 0,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }


        foreach ($request->responses as $index => $option) {
            $poll->options()
                ->create(
                    [
                        'option' => $option,
                        'index' => $index,
                    ]
                );
        }


        return redirect()->route('encuestas.index')->with('success', 'La encuesta ha sido creada');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $poll = Poll::with(['options' => function ($query) {
            $query->orderBy('index', 'asc');
        }])->findOrFail($id);
        $decode = json_decode($poll->options);
        //$decode = $poll->options;
       //dd($poll);
        return view('dashboard.polls.edit', compact('poll','decode'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->only('question', 'date_from', 'date_to'));

        $poll = Poll::findOrFail($id);
        $poll->update($request->only('question', 'date_from', 'date_to'));

        $pollOption = PollOption::findOrFail($poll->id);
        $poll->update($request->only('option'));

        /****
        foreach ($request->options as $key => $option) {
            if (array_key_exists('deleted', $option)) {
                PollOption::find($option['id'])->delete();
                continue;
            }

            if (array_key_exists('id', $option)) {
                PollOption::find($option['id'])
                    ->update(['option' => $option['option']]);
            } else {
                $poll->options()
                    ->create([
                        'option' => $option['option'],
                        'index' => $key,
                    ]);
            }
        } ***/
        return redirect('admin/encuestas')->with('success','La encuesta ha sido actualizada');
        //return response()->json(['status' => 1, 'message' => 'La encuesta ha sido actualizada']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Poll::destroy($id);
        return redirect()->back()->with('success', 'La encuesta ha sido eliminada');
    }

    public function status($id)
    {
        $poll = Poll::findOrFail($id);
        $poll->active = !$poll->active;
        $poll->save();

        return redirect()->route('encuestas.index')->with('success', 'La encuesta ha sido actualizada');
    }
}
