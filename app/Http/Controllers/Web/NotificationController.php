<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getUnreadNotifications(Request $request)
    {
        $notifications = $request->user()->unreadNotifications;

        return response()->json(['status' => 1, 'notifications' => $notifications], 200);
    }
}
