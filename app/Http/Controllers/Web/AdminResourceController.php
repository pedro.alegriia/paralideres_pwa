<?php

namespace App\Http\Controllers\Web;

use App\Events\PublishedResourceEvent;
use App\Models\Category;
use App\Models\Resource;
use App\Models\collection;
use App\Models\User;
use App\Notifications\ResourcePublished;
/* use App\Service\Searchable;
use Elasticsearch\ClientBuilder; */
use Freshbitsweb\Laratables\Laratables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use phpDocumentor\Reflection\Types\Resource_;

class AdminResourceController extends Controller
{
    //use Searchable;

    protected $client;

    /**
     * AdminResourceController constructor.
     */
    public function __construct()
    {
       /*  $this->client = ClientBuilder::create()
            ->setHosts([env('ELASTICSEARCH_HOST')])
            ->build(); */
    }


    public function index(Request $request)
    {

           //->orderBy('published')->paginate(15);
         //->where('published','=', 0)
        
        $resources = Resource::with('user', 'categories')
        
        ->orderBy('published', 'ASC')
        ->paginate(15);
       
       // dd($resources);

      /***
        $resources = Resource::with(array(
            'categories' => function($query)
            {
                $query->whereHas('categories.slug' ,'=' ,'no-mostrar');
                $query->orderBy('categories.created_at', 'DESC');
            }))
            ->orderBy('published','ASC')
            ->get();
      dd($resources);
* */
      

        return view('dashboard.resources.index', compact('resources'));
    }

    public function getDatatable(Request $request)
    {
        return Laratables::recordsOf(Resource::class);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $resource = Resource::find($id);
       // $categories = Category::all();
        return redirect('app/resource');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $resource = Resource::find($id);
        $categories = Category::all();
        return redirect('app/resource');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        Resource::destroy($id);

        return back()->with('success', 'El recurso ha sido eliminado');
    }

    public function recover($id)
    {
        Resource::onlyTrashed()->findOrFail($id)->restore();

        return back()->with('success', 'El recurso ha sido restaurado');
    }

    public function togglePublished($id)
    {
        $resource = Resource::findOrFail($id);

        if (env('APP_ENV', 'no-production') == 'production') {
            if ($resource->published) {
                $this->deleteResource($resource);
            } else {
                $this->indexResource($resource);
                $resource->user->notify(new ResourcePublished($resource));
            }
        }

        $resource->published = !$resource->published;
        $resource->save();

      //  event(new PublishedResourceEvent($resource));

        return redirect()->route('recursos.index')->with('success', 'El recurso ha sido actualizado');
    }
}
