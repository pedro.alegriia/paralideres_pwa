<?php

namespace App\Http\Controllers\Web;

use App\Models\Category;
use App\Models\Resource;
use App\Models\ResourceDownload;
use App\Models\Tag;
use App\Models\User;
use App\Service\Searchable;
use Elasticsearch\ClientBuilder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class ResourceController extends WebController
{
    //use Searchable;

    protected $client;

    /**
     * Display all resources
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        //return view('resources.index');
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Request $request, $slug)
    {
        //dd($slug);
        $data['resource_header'] = true;
        $user_id = ($this->auth) ? $this->auth->id : null;

        $data['resource'] = Resource::with([
            'tags',
            'likesCount',
            'user',
            'downloads',
            'categories',
            'likes' => function ($query) use ($user_id) {
                $query->where('user_id', $user_id);
            }
        ])
            ->where('slug', $slug)
            ->firstOrFail();

        if ($request->user()) {
            $data['resource']->read()->firstOrCreate([
                'user_id' => $user_id,
            ]);
        }

        $author = User::with('profile')
            ->where('id', $data['resource']->user_id)
            ->get()
            ->toArray();

        if (count($author)) {
            $data['author'] = $author[0];
        }

        $data['latestResources'] = Resource::with('categories')
            ->orderBy('id', 'desc')
            ->limit(5)
            ->get();

        $data['tags'] = Tag::orderBy('id', 'desc')->get();

        return view('resources.show')->with($data);
    }

    public function create()
    {
        return view('resources.create');
    }

    public function edit($slug)
    {
        $resource = Resource::where('slug', $slug)->firstOrFail();
        return view('resources.edit', compact('resource'));
    }

    /**
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function download($slug)
    {
        //Check if user exceeded 10 downloads in the last 7 days
        $count = ResourceDownload::where('user_id', auth()->id())
            ->where('created_at', '>', now()->subDays(7))
            ->count();

        if (Auth::user()->level === 1 && $count > 10) {
            return view('dashboard.resources.failed-download');
        } else if (Auth::user()->level === 2 && $count > 25) {
            return view('dashboard.resources.failed-download');
        }

        $resource = Resource::where('slug', $slug)->firstOrFail();
        $resource->downloads()->create([
            'user_id' => auth()->id() ?? 0,
        ]);

        $exists = $resource->getFirstMedia('resources');

        if (!$exists) {
            return view('dashboard.resources.download', compact('resource'));
        }

        return $resource->getFirstMedia('resources');
    }

    public function search(Request $request)
    {
        //return Cache::remember('SEARCH_' . $request->getQueryString(), 5, function () use ($request) {

        $limit = env('SEARCH_QUERY_LIMIT');
        $page = $request->has('page') ? $request->page : 1;

        if (!$request->has('author') &&
            !$request->has('year') &&
            !$request->has('category') &&
            !$request->has('tag') &&
            $request->has('search') &&
            $request->search != "") {

          /*   $this->client = ClientBuilder::create()
                ->setHosts([env('ELASTICSEARCH_HOST')])
                ->build(); */

            $response = $this->searchOnES($request->search, $limit, $page);

            $count = $response['count'];

            $resources_id = $response['hits']->map(function ($resource) {
                return $resource['_id'];
            });

            $resources = Resource::whereIn('id', $resources_id)
                ->published()
                ->with(['categories', 'user.profile', 'likes', 'downloads'])
                ->limit($limit)
                ->orderBy('created_at', 'desc')
                ->get();

            $resources->each(function ($resource) {
                $resource->avatar = $resource->user->getFirstMediaUrl('profiles');
            });

            return response()->json(['status' => 1, 'resources' => $resources, 'count' => $count, 'page' => $page], 200);
        }

        $resources = Resource::onlySearch($request->search);

        if ($request->has('author')) {
            $resources->publishedBy($request->author);
        }

        if ($request->has('year')) {
            $resources->whereYear('created_at', $request->year);
        }

        if ($request->has('category') && $request->category != "") {
            $category = Category::where('slug', $request->category)->first();

            if (!$category || $request->category == 'no-mostrar') {
                return response()->json(['status' => 0, 'message' => 'Category not found'], 404);
            }

            $resources->join('resource_category', function ($join) use ($category) {
                $join->on('resources.id', '=', 'resource_category.resource_id')
                    ->where('resource_category.category_id', $category->id);
            })->select('resources.*');
        }

        if ($request->has('tag') && $request->tag) {
            $tag = Tag::where('label', $request->tag)->firstOrFail();

            if (!$tag) {
                return response()->json(['status' => 0, 'resources' => null], 404);
            }

            $resources->join('resource_tag', function ($join) use ($tag) {
                $join->on('resources.id', '=', 'resource_tag.resource_id')
                    ->where('resource_tag.tag_id', $tag->id);
            })->select('resources.*');
        }

        $count = $resources->published()->count();
        $resources = $resources->published()
            ->with([
                'categories',
                'user.profile',
                'likes',
                'downloads',
                'tags'
            ])->limit(12)
            ->offset(($page - 1) * 12)
            ->orderBy('created_at', 'desc')
            ->get();

        $resources->each(function ($resource) {
            $resource->avatar = $resource->user->getFirstMediaUrl('profiles');
        });

        return response()->json(['status' => 1, 'resources' => $resources, 'count' => $count, 'page' => $page], 200);

        //});

    }
}
