<?php

namespace App\Http\Controllers\Web;

use App\Badge;
use App\Models\Category;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
class HomeController extends WebController
{
    /**
     * HomeController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth')->only(['acceptTerms']);
    }

    /**
     * Display the home page
     * @return $this
     */
    public function index()
    {
        /*$data['categories'] = Category::selectRaw('categories.*, count(resources.id) as total_resource')
            ->join('resources', 'resources.category_id', '=', 'categories.id')
            ->groupBy('categories.id')
            ->orderBy('total_resource', 'desc')
            ->limit(4)
            ->get();*/

        return view('layouts.app');
        return view('home');
    }

    public function activateUser($id)
    {

      

        $user = User::findOrfail($id);
        $user->is_active = 1;
        $user->save();
       // $circuit->update($request->all());
        //dd($circuit);
        return redirect('/')->with('success', 'Ya puedes iniciar sesión');

        //dd($id);

       // $random1 = Str::random(30);
       // $user->activation_token = $random1;

  //     if ($id ==true ) {
    //    $user = User::find($id);
      //  dd($id);

       //}
    
      // $affected = DB::table('users')
       //->where('id','=', $id)
       //->update(['is_active' => 1]);
         //     return redirect('/')->with('success', 'Ya puedes iniciar sesión');
            //$user = User::where('id', '=', $id)->first();

           // dd($user);
           // $user = User::find($id);
            //$user->is_active = 1;
            //$user->save();
       
        //$user = User::where('id', '=',$id)->first();
        //$activation_null = User::where('activation_token', '==', NULL)->first();
/***
        if (!$user ) {
            return redirect('/otroto')->with('success', 'Ya puedes iniciar sesión');
        }else {
           $user_get = User::find($id);
           $user->is_active = 1;
           $random1 = Str::random(30);
           $user->activation_token = $random1;
           $save->save();
           return redirect('/entraaregister')->with('success', 'Activación exitosa, ya puedes iniciar sesión');
        }
        * */
/***
        if (!$user) {
            return redirect('account/activation'. $token)->with('error', 'Por favor registrate');
        }

        if ($user->is_active == 1) {
            return redirect('account/activation'. $token)->with('success', 'Ya puedes iniciar sesión');
        }

        $user->is_active = 1;
        $user->save();

        /** Asignamos badge de Usuario **/
       // $user->badge()->attach(Badge::where('name', 'Usuario')->first());
    
        
    }

    public function activation($token)
    {
        dd($token);
        $rv = array();
        $userCheck = new User();
        $tokenCheck = $userCheck->where('is_active', 0)->where('activation_token', $token)->get()->toArray();
        $tokenReCheck = $userCheck->where('is_active', 1)->where('activation_token', $token)->get()->toArray();
        if (count($tokenReCheck) > 0) {
            $rv = array(
                "status" => 2000,
                "msg" => 'Your account has already been activated. Please login now'
            );
        } else {
            if (count($tokenCheck) > 0) {
                $activeUser = new User();
                $activeUser->where('is_active', 0)
                    ->where('activation_token', $token)
                    ->update(['is_active' => 1]);
                $rv = array(
                    "status" => 2000,
                    "msg" => 'Congratulation! Your account has been activated successfully. Please login now'
                );
            } else {
                $rv = array(
                    "status" => 5000,
                    "msg" => 'Invalid activation token. Please try with valid token'
                );
            }
        }
        return view('auth.login')->with('data', $rv);
    }

    public function acceptTerms(Request $request)
    {
        Auth::user()->update(['accept_terms' => 1]);

        return response()->json(['status' => 1, 'message' => 'Gracias por aceptar'], 200);
    }

    public function badges(Request $request)
    {
        $badges = Badge::select(['id', 'name', 'icon_name'])->get();

        return response()->json(['status' => 1, 'badges' => $badges], 200);
    }
}