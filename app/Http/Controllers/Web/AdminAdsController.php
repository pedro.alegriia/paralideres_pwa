<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\Ad\StoreAd;
use App\Models\Ad;
use App\Models\AdClick;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;

class AdminAdsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ads = Ad::with('clicks')->paginate(10);

        return view('dashboard.ads.index', compact('ads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.ads.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreAd $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function store(StoreAd $request)
    {
        $ad = Ad::create([
            'user_id' => auth()->id(),
            'uuid' => Uuid::uuid4()->toString(),
            'name' => $request->name,
            'description' => $request->description,
            'link' => $request->link,
            'starts_at' => $request->starts_at,
            'ends_at' => $request->ends_at,
        ]);

       // $ad->addMediaFromRequest('banner')
         //   ->toMediaCollection('ads', 's3');

        return redirect('/admin/ads');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ad $ads
     * @return \Illuminate\Http\Response
     */
    public function show(Ad $ads)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Ad $ads
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $ad = Ad::find($id);

        return view('dashboard.ads.edit', compact('ad'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function update(Request $request, $id)
    {
        $ad = Ad::findOrFail($id);
        $ad->update([
            'user_id' => auth()->id(),
            'name' => $request->name,
            'description' => $request->description,
            'link' => $request->link,
            'starts_at' => $request->starts_at,
            'ends_at' => $request->ends_at,
        ]);

        if ($request->has('banner') && $request->file('banner')->isValid()) {
            $ad->getFirstMedia('ads')->delete();
            $ad->addMediaFromRequest('banner')
                ->toMediaCollection('ads', 's3');
        }

        return redirect('/admin/ads');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Ad $ads
     * @return void
     */
    public function destroy(Ad $ads)
    {
        //
    }
}
