<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    public function resetPassLink(Request $request)
    {
        $response = $this->broker()->sendResetLink(['email' => $request->get('email')]);
        if ($response) {
            return view('auth.passwords.reset')->with('message', 'We have e-mailed your password reset link!');
        }
    }

   //Model route binding
   public function show(){

    //$passwordRecoveryQuestion = PasswordRecoveryQuestion::where('id',$passwordRecoveryQuestion)->get();
    //debug("object",$passwordRecoveryQuestion);
    //$passwordRecoveryQuestion->load('questions.answer');
    return view('auth.passwords.reset');
}

    
}
