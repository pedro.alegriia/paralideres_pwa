<?php

namespace App\Http\Requests\Ad;

use Illuminate\Foundation\Http\FormRequest;

class StoreAd extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'description' => 'sometimes|string',
            'link' => 'url|required',
            'starts_at' => 'required|date',
            'ends_at' => 'required|date',
            'banner' => 'required|image',
        ];
    }

    /*public function messages()
    {
        return [
            'name.required' => ''
        ];
    }*/

}
