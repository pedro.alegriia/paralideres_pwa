<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePassword extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'old' => 'required|string',
            'password' => 'required|string|regex:/^(?=.*\d)(?=.*[a-z])(?=.*[!@#$%^&*])(?=.*[A-Z]).{8,20}$/',
            'password_confirmation' => 'required|string',
        ];
    }

    public function messages()
    {
        return [
            'old.required' => 'La contraseña actual es obligatoria',
            'old.string' => 'La contraseña actual deben ser caracteres',
            'password.required' => 'La nueva contraseña es obligatoria',
            'password.regex' => 'El password no cumple los requisitos mencionados',
            'password_confirmation.required' => 'La confirmación de contraseña es obligatoria',
            'password_confirmation.confirmed' => 'La confirmación de contraseña no es igual',
        ];
    }
}
