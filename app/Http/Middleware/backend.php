<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class backend
{

    
    
     // Override handle method
     public function handle($request, Closure $next, ...$guards)
     {
        if(!Auth::check()){
            return redirect('/admin_login');
        }
        
        
        return $next($request);
     }
}
