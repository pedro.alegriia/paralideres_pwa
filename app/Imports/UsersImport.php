<?php

namespace App\Imports;

use App\Models\OldUser;
use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class UsersImport implements ToModel, WithHeadingRow, WithBatchInserts, WithChunkReading, ShouldQueue, SkipsOnError
{
    /**
     * @param array $row
     *
     * @return User
     */
    public function model(array $row)
    {
        return new User([
            'username' => $row['email'] . '-' . str_random(5),
            'email' => $row['email'],
            'password' => bcrypt($row['password']),
            'former_pwd' => $row['password'],
            'former_id' => $row['id'],
            'is_active' => 1,
            'verified' => 1,
        ]);

        /*return new OldUser([
            'id' => $row['id'],
            'first_name' => $row['first_name'],
            'last_name' => $row['last_name'],
            'password' => $row['password'],
            'email' => $row['email'],
            'first_visit' => $row['first_visit'],
            'bday' => $row['bday'],
            'sex' => $row['sex'],
            'm_status' => $row['m_status'],
            'work_type' => $row['work_type'],
            'city' => $row['city'],
            'state' => $row['state'],
            'country' => $row['country'],
            'main_language' => $row['main_language'],
            'phone' => $row['phone'],
            'security_level' => $row['security_level'],
            'picturen' => $row['picture'],
            'otherinfo' => $row['otherinfo'],
            'last_log' => $row['last_log'],
            'show_info' => $row['show_info'],
            'receive_emails' => $row['receive_emails'],
            'status_user' => $row['status_user'],
        ]);*/
    }

    public function batchSize(): int
    {
        return 2000;
    }

    public function chunkSize(): int
    {
        return 2000;
    }

    public function onError(\Throwable $e)
    {
        // Handle the exception how you'd like.
        app('log')->info($e->getMessage());
    }
}
