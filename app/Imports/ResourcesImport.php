<?php

namespace App\Imports;

use App\Models\Resource;
use App\Models\User;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Throwable;

class ResourcesImport implements ToModel, WithHeadingRow, SkipsOnError
{
    /**
     * @param array $row
     *
     * @return Resource
     */
    public function model(array $row)
    {
        $user_id = User::where('former_id', $row['user_id'])->first()->id ?? 1;
        return new Resource([
            'user_id' => $user_id,
            'former_id' => $row['page_id'] ?? null,
            'title' => $row['page_title'] ?? null,
            'slug' => str_replace(' ', '-', $row['page_title']) . str_random(6),
            'review' => $row['blurb'] ?? null,
            'content' => $row['body'] ?? null,
            'attachment' => $row['pic'] ?? null,
            'video_url' => null,
            'published' => $row['isposted'] ?? null,
            'former_section_id' => $row['section_id'] ?? null,
        ]);
    }

    /**
     * @param Throwable $e
     */
    public function onError(Throwable $e)
    {
        app('log')->info($e->getMessage());
    }
}
