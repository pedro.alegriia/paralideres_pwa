<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = [
        'id',
        'name',
        'display_name',
        'guard_name',
    ];

    //public function users()
      //  {
        //    return $this->belongsToMany(User::class)->withTimestamps();
       // }


        public function permissions() {

            return $this->belongsToMany(Permission::class,'roles_permissions');
                
         }
         
         public function users() {
         
            return $this->belongsToMany(User::class,'users_roles');
                
         }

}
