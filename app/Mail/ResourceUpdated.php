<?php

namespace App\Mail;

use App\Models\Resource;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ResourceUpdated extends Mailable
{
    use Queueable, SerializesModels;
    public $user, $resource;

    /**
     * Create a new message instance.
     *
     * @param User $user
     * @param Resource $resource
     */
    public function __construct(User $user, Resource $resource)
    {
        $this->user = $user;
        $this->resource = $resource;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Publicación recibida en ParaLideres')
            ->view('emails.resources.updated');
    }
}
