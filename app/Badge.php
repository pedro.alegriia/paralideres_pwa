<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Badge extends Model
{
    protected $table = 'badges';
    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsToMany(User::class, 'user_badge');
    }
}
