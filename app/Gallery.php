<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;


class Gallery extends Model
{
    use Notifiable;

    protected $table = 'gallery';
    protected $guarded = ['id'];
    protected $fillable = ['id', 'order', 'type','image', 'status'];
    protected $hidden = [
        'created_at',
        'updated_at',
        'former_id'
    ];


    public function registerMediaCollections()
    {
        $this->addMediaCollection('gallery')
            ->singleFile();
    }


    /**
     * Eager load media items of the role for displaying in the datatables.
     *
     * @return callable
     */
    public static function laratablesRoleRelationQuery()
    {
        return function ($query) {
            $query->with('media');
        };
    }

    /**
     * Set user full name on the collection.
     *
     * @param \Illuminate\Support\Collection
     * @return \Illuminate\Support\Collection
     */
    public static function laratablesModifyCollection($gallerys)
    {
        return $gallerys->map(function ($gallery) {
            $gallery['image'] = $gallery->media[0]->getUrl();
            return $gallery;
        });
    }


    public static function laratablesCustomStatus($gallery)
    {
        $gallery = Gallery::FindOrFail($gallery->id);
        return view('partials.laratables.gallery.status', compact('gallery'))->render();
    }

    public static function laratablesCustomImage($gallery)
    {
        $gallery = Gallery::FindOrFail($gallery->id);
        return view('partials.laratables.gallery.image', compact('gallery'))->render();
    }

    //custom action buttons to pre-render View
    public static function laratablesCustomAction($gallery)
    {
        $gallery = Gallery::FindOrFail($gallery->id);
        return view('partials.laratables.gallery.action', compact('gallery'))->render();
    }
}
