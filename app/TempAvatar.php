<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class TempAvatar extends Model 
{
    //use HasMediaTrait;

    protected $table = 'temp_avatars';
    protected $fillable = ['user_id'];

    public function registerMediaCollections()
    {
        $this->addMediaCollection('temp_avatars')
            ->singleFile();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
