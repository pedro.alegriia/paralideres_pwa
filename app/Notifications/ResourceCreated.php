<?php

namespace App\Notifications;

use App\Models\Resource;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;

class ResourceCreated extends Notification
{
    use Queueable;

    public $resource;

    /**
     * Create a new notification instance.
     *
     * @param User $user
     * @param Resource $resource
     */
    public function __construct(Resource $resource)
    {
        $this->resource = $resource;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     * This notifies to the author and the editor about the new created resource
     * @param mixed $notifiable
     * @return \App\Mail\ResourceCreated
     */
    public function toMail($notifiable)
    {
        return (new \App\Mail\ResourceCreated($notifiable, $this->resource))->to($notifiable->email);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'message' => 'Has creado un nuevo recurso',
        ];
    }
}
