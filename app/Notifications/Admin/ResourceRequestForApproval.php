<?php

namespace App\Notifications\Admin;

use App\Models\Resource;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ResourceRequestForApproval extends Notification implements ShouldQueue
{
    use Queueable;

    public $resource;

    /**
     * Create a new notification instance.
     *
     * @param Resource $resource
     */
    public function __construct(Resource $resource)
    {
        $this->resource = $resource;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = env('APP_URL') . "/app/resources/{$this->resource->slug}";

        return (new MailMessage)
            ->subject('Nuevo recurso publicado en PL')
            ->greeting('Hola!')
            ->line('¡Un nuevo recurso ha sido publicado!')
            ->line('Es necesario que tengas abierta tu sesión para poder dirigir adecuadamente el enlace')
            ->action('Ver nuevo recurso', $url)
            ->salutation('¡Saludos!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'title' => 'Recurso en espera de aprobación',
            'message' => "{$this->resource->title} ha sido actualizado. Favor de aprobar o rechazar.",
            'type' => 'resource',
            'link' => "/app/resources/{$this->resource->slug}",
        ];
    }
}
