<?php

namespace App\Notifications;

use App\Mail\GenericNotificationEmail;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class SendEbookToUser extends Notification
{
    use Queueable;

    public $ebook;

    /**
     * Create a new notification instance.
     *
     * @param array $ebook
     */
    public function __construct(array $ebook)
    {
        $this->ebook = $ebook;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return GenericNotificationEmail
     */
    public function toMail($notifiable)
    {
        $email = [
            'recipient' => $notifiable->profile->fullname,
            'primary' => "Gracias a tu participación queremos obsequiarte el e-book \"{$this->ebook['name']}\"",
            'url' => $this->ebook['url'],
            'label_url' => 'DESCARGAR EBOOK',
        ];
        return (new GenericNotificationEmail($email))->to($notifiable->email);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
