<?php

namespace App\Notifications\Users;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class AvatarChangeResponse extends Notification
{
    use Queueable;

    public $status, $message;

    /**
     * Create a new notification instance.
     *
     * @param $status
     * @param $message
     */
    public function __construct($status, $message)
    {
        $this->status = $status;
        $this->message = $message;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if ($this->status === 'accept') {
            $url = env('APP_URL') . "/app/perfil/{$notifiable->id}";

            return (new MailMessage)
                ->line('Gracias por actualizar tu imagen de perfil')
                ->action('Ver mi perfil', $url)
                ->line($this->message);
        }

        return (new MailMessage)
            ->line('Gracias por intentar actualizar tu imagen de perfil')
            ->action('Intentar con otra imagen', url('/app/cuenta'))
            ->line("No hemos podido aprobar tu solicitud debido a: {$this->message}");
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
