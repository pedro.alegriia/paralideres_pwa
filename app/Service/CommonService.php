<?php

/**
 * CommonService
 * @author : Tarek Monjur
 * @email : tarekmonjur@gmail.com
 */

namespace App\Service;

use App\Models\User;
use stdClass;

trait CommonService
{
    /**
     * @description set the ajax response message
     * @param array $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function setResponse($data, $status, $type, $code, $title, $message)
    {
        $sendData['data'] = $data;
        $sendData['status'] = $status;
        $sendData['statusType'] = $type;
        $sendData['code'] = $code;
        $sendData['title'] = $title;
        $sendData['message'] = $message;

        return response()->json($sendData, $code);
    }

    public function response($status, $content, $message, $code)
    {
        return response()->json(['status' => $status, 'content' => $content, 'message' => $message], $code);
    }

    public function paginatorSearchCustom($page,$total_reg,$model,$where,$orderField,$ordeyBy,$category,$author)
    {
        
        $model_name = 'App\Models'.$model;
        $resources = $model_name::where($where)->orderBy($orderField, $ordeyBy)->orWhere('user_id', '=', $author)->get();
        $resource_array = [];
        $max = sizeof($resources);
        $book_obj = new stdClass;
        $book_obj->total_pages = round($max/$total_reg);
        if($book_obj->total_pages == 0){
            $book_obj->total_pages = 1;
        }
        if($page > 1){
            $init = ($page-1)*$total_reg;
            $final = ($page*$total_reg);
        }else{
            $init = 0;
            $final = $page*$total_reg;
        }
        for($i = $init; $i < $final; ++$i) {
            if($i < sizeof($resources)){
                array_push($resource_array,$resources[$i]);
            }
            
        }
        $book_obj->data  = $resource_array;
        return $book_obj;        
    }



    public function paginatorSearch($page,$total_reg,$model,$where,$orderField,$ordeyBy)
    {
        
        $model_name = 'App\Models'.$model;
        $resources = $model_name::where($where)->orderBy($orderField, $ordeyBy)->get();
        $resource_array = [];
        $max = sizeof($resources);
        $book_obj = new stdClass;
        $book_obj->total_pages = round($max/$total_reg);
        if($book_obj->total_pages == 0){
            $book_obj->total_pages = 1;
        }
        if($page > 1){
            $init = ($page-1)*$total_reg;
            $final = ($page*$total_reg);
        }else{
            $init = 0;
            $final = $page*$total_reg;
        }
        for($i = $init; $i < $final; ++$i) {
            if($i < sizeof($resources)){
                array_push($resource_array,$resources[$i]);
            }
            
        }
        $book_obj->data  = $resource_array;
        return $book_obj;        
    }

    public function paginator($page,$total_reg,$model,$where,$orderField,$ordeyBy)
    {
        
        $model_name = 'App\Models'.$model;
        $resources = $model_name::where($where)->orderBy($orderField, $ordeyBy)->get();
        $resource_array = [];
        $max = sizeof($resources);
        $book_obj = new stdClass;
        $book_obj->total_pages = round($max/$total_reg);
        if($book_obj->total_pages == 0){
            $book_obj->total_pages = 1;
        }
        if($page > 1){
            $init = ($page-1)*$total_reg;
            $final = ($page*$total_reg);
        }else{
            $init = 0;
            $final = $page*$total_reg;
        }
        for($i = $init; $i < $final; ++$i) {
            if($i < sizeof($resources)){
                array_push($resource_array,$resources[$i]);
            }
            
        }
        $book_obj->data  = $resource_array;
        return $book_obj;        
    }

    public function arrayPaginator($page,$total_reg,$model,$relation_model,$array,$where,$relation_table,$relation_id,$relation_comp)
    {

        $model_name = 'App\Models'.$model;
        $table_name_rel = 'App\Models'.$relation_table;
        $relation_model_name = 'App\Models'.$relation_model;
        $array_gl= [];
        foreach($array as $element){
            
            $element_model = $model_name::find($element);
            
            $results = $table_name_rel::where([[$relation_id,$element_model->id]])->get();
            $array_gl = array_merge($array_gl,$results->toArray());
  
        }
        $max = sizeof($array_gl);
        $book_obj = new stdClass;
        $book_obj->total_pages = round($max/$total_reg);
        if($book_obj->total_pages == 0){
            $book_obj->total_pages = 1;
        }
        foreach($array_gl as $result){
            $resource_array = [];
            if($page > 1){
                $init = ($page-1)*$total_reg;
                $final = ($page*$total_reg+1);
            }else{
                $init = 0;
                $final = $page*$total_reg;
            }
            for($i = $init; $i < $final; ++$i) {
                if($i < sizeof($array_gl)){
                    $element = $relation_model_name::find($array_gl[$i][$relation_comp]);

                    array_push($resource_array,$element);
                }
            }
            $book_obj->data  = $resource_array;
            
            return $book_obj; 
        }


               
    }

    public function getUserName($username,$id){
        $user = User::find($id);
        $arrayUserName = explode('@',$username);
        if(sizeof($arrayUserName)>1){
            $exist = User::where([['username',$arrayUserName[0]]])->first();
            if(isset($exist->id)){
                $user->username = $arrayUserName[0].$user->id;
            }else{
                $user->username = $arrayUserName[0];
            }
            
            $user->save();
            return $arrayUserName[0];
        }else{
            return $arrayUserName[0];
        }
    }
}