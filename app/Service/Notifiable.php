<?php

namespace App\Service;

use App\Models\User;
use App\Notifications\Admin\ResourceRequestForApproval;
use Illuminate\Support\Facades\Notification;
use Spatie\Permission\Models\Role;

trait Notifiable
{
    public function notifyPublishers($resource)
    {
        Notification::send($this->getAdmins(), (new ResourceRequestForApproval($resource))->delay(now()->addSeconds(30)));
        return true;
    }

    public function getPublishers()
    {
        return User::whereIn('email', ['admin@paralideres.org'])->get();
    }

    public function getAdmins()
    {
        return Role::findByName('admin')->users;
    }
}
