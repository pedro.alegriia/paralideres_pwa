<?php


namespace App\Service;


trait Sanitizable
{
    /**
     * Snakecase a string
     * @param $slug
     * @return string
     */
    public function toSlug($slug)
    {
        $slug = strtolower(preg_replace('/[\s-]/', '_', $slug));
        $slug = str_replace('Á', 'A', $slug);
        $slug = str_replace('á', 'a', $slug);
        $slug = str_replace('É', 'E', $slug);
        $slug = str_replace('é', 'e', $slug);
        $slug = str_replace('í', 'i', $slug);
        $slug = str_replace('Í', 'I', $slug);
        $slug = str_replace('Ó', 'O', $slug);
        $slug = str_replace('ó', 'o', $slug);
        $slug = str_replace('Ú', 'U', $slug);
        $slug = str_replace('ú', 'u', $slug);
        $slug = str_replace('Ñ', 'N', $slug);
        $slug = str_replace('ñ', 'n', $slug);
        $slug = str_replace('/', '-', $slug);
        $slug = str_replace(':', '-', $slug);
        $slug = str_replace(';', '-', $slug);
        $slug = str_replace('.', '-', $slug);
        $slug = str_replace(',', '-', $slug);
        $slug = str_replace('¿', '-', $slug);
        $slug = str_replace('?', '-', $slug);
        $slug = str_replace('(', '-', $slug);
        $slug = str_replace(')', '-', $slug);

        return $slug;
    }
}
