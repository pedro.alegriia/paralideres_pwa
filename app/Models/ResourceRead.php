<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ResourceRead extends Model
{
    use SoftDeletes;

    protected $table = 'resource_reads';
    protected $guarded = ['id'];

    public function resource()
    {
        return $this->belongsTo(Resource::class);
    }
}
