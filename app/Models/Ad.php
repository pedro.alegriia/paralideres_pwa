<?php

namespace App\Models;

use App\Models\AdClick;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;


class Ad extends Model
{
    use SoftDeletes;

    protected $table = 'ads';
    protected $guarded = ['id'];
    protected $appends = ['banner'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getBannerAttribute()
    {
        return $this->getFirstMediaUrl('ads');
    }

    public function clicks()
    {
        return $this->hasMany(AdClick::class);
    }

    public function click()
    {
        $this->clicks()->create([
            'user_id' => Auth::id() ?? 0,
            'ad_id' => $this->id,
        ]);
    }
}
