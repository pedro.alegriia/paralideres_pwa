<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    protected $table = 'user_profiles';
    protected $guarded = ['id'];

    public function getFullNameAttribute($value)
    {
        return html_entity_decode(strip_tags($value));
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }
    public function topResources($id)
    {
        $response = Resource::where([['user_id','=',$id]])->limit(5)->orderBy('created_at', 'desc')->get();
        return $response;
    }

    public function lastResources($id)
    {
        $response = Resource::where([['user_id','=',$id]])->limit(5)->orderBy('created_at', 'asc')->get();
        return $response;
    }

    public function countResources($id)
    {
        $response = Resource::where([['user_id','=',$id]])->count();
        return $response;
    }
}
