<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResourceLike extends Model
{
    protected $table = 'resource_likes';
    protected $guarded = ['id'];
}
