<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResourceCollection extends Model
{
    protected $table = 'resource_collection';
    protected $guarded = ['id'];
}
