<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OldUser extends Model
{
    protected $table = 'oldusers';
    protected $guarded = [];
    public $timestamps = false;
}
