<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Poll extends Model
{
    protected $table = 'polls';
    protected $hidden = ['pivot', 'created_at', 'updated_at', 'deleted_at'];
    protected $fillable = ['question', 'active', 'date_from', 'date_to'];
    protected $dates = [
        'date_from',
        'date_to',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    // protected $dateFormat = 'd/m/Y H:i:s';

    protected $dateFormat = 'Y-m-d H:i:s';

    public function options()
    {
        return $this->hasMany('App\Models\PollOption');
    }

    public function users()
    {
        return $this->belongsToMany('App\Models\User');
    }

    public function votes()
    {
        return $this->hasMany(PollVote::class);
    }

    public function getDateFromAttribute($value)
    {
        return Carbon::parse($value)->format('Y-m-d');
    }

    public function getDateToAttribute($value)
    {
        return Carbon::parse($value)->format('Y-m-d');
    }

    public static function laratablesCustomPeriod($poll)
    {
        $poll = Poll::find($poll->id);
        return "{$poll->date_from} - {$poll->date_to}";
    }

    public static function laratablesCustomStatus($poll)
    {
        $poll = Poll::find($poll->id);
        return view('partials.laratables.polls.status', compact('poll'))->render();
    }

    public static function laratablesCustomAction($poll)
    {
        $poll = Poll::find($poll->id);
        return view('partials.laratables.polls.action', compact('poll'))->render();
    }
}
