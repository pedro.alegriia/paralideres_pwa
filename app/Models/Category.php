<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;


class Category extends Model
{


    protected $table = 'categories';
    protected $fillable = ['label', 'slug', 'description','icon','color_icon','status'];
    protected $hidden = [
        'created_at',
        'updated_at',
        'former_id'
    ];


    public function registerMediaCollections()
    {
        $this->addMediaCollection('categories')
            ->toMediaCollection();
    }

    public function collections()
    {
        return $this->belongsToMany(Collection::class, 'collection_category', 'category_id', 'collection_id');
    }

    public function resources()
    {
        return $this->belongsToMany(Resource::class, 'resource_category', 'category_id', 'resource_id');
    }


    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function scopeVisible($query)
    {
        return $query->where('visible', 1);
    }
    /**
     * Additional columns to be loaded for datatables.
     *
     * @return array
     */
    public static function laratablesAdditionalColumns()
    {
        return ['design'];
    }

    //custom action buttons to pre-render View
    public static function laratablesCustomDesign($category)
    {
        $category = DB::table('category')->where('id', $category->id)->first();
        return view('partials.laratables.category.design', compact(' $category '))->render();
    }

    //custom action buttons to pre-render View
    public static function laratablesCustomAction($category)
    {
        $category = DB::table('category')->where('id', $category->id)->first();
        return view('partials.laratables.category.action', compact(' $category '))->render();
    }


    public function resourcesCount()
    {
        
        $total = DB::table('resource_category')
        ->selectRaw('count(*) as total')
        ->where('category_id', $this->id)
        ->get();
        return $total[0]->total;
    }

    
}
