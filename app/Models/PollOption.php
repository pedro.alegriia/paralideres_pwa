<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PollOption extends Model
{
    use SoftDeletes;

    protected $table = 'poll_options';
    protected $guarded = ['id'];
    protected $hidden = ['poll_id'];

    public function poll()
    {
        return $this->belongsTo('App\Models\Poll');
    }

    public function votes()
    {
        return $this->hasOne('App\Models\PollVote', 'poll_options_id', 'id')
            ->selectRaw('poll_options_id, count(*) as total');
    }
}
