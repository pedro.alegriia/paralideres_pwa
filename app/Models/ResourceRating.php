<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResourceRating extends Model
{
    protected $table = 'ratings';
    protected $guarded = ['id'];
}
