<?php

namespace App\Models;

use App\Rating;
use App\Models\ImageCategory; 
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use App\Models\Category;

class Resource extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];
    protected $hidden = ['pivot', 'deleted_at'];
    protected $appends = ['local_attachment', 's3_attachment', 'rating'];

    public function registerMediaCollections()
    {
        $this->addMediaCollection('resources')->singleFile();
    }

    public function getTitleAttribute($value)
    {
        return html_entity_decode(strip_tags($value));
    }

    public function getReviewAttribute($value)
    {
        return html_entity_decode(strip_tags($value));
    }

    /*public function getContentAttribute($value)
    {
        return html_entity_decode(strip_tags($value));
    }*/

    public function user()
    {
        return $this->belongsTo('App\Models\User')
            ->select('users.id', 'users.username', 'user_profiles.fullname', 'user_profiles.image', 'users.created_at', 'users.level',)
            ->leftJoin('user_profiles', 'user_profiles.user_id', '=', 'users.id');
    }

    public function getCategories(){
        $array_rel = [];
        $relations = DB::table('resource_category')->where([['resource_id',$this->id]])->get();
        foreach ($relations as $relation){
            $category = Category::find($relation->category_id);
            $arr = $category->attributesToArray();
            if($arr['visible'] == 1){
                $tot_recources = $category->resourcesCount();
                $category->total_resources = $tot_recources;
                $this->getIconPath($category);
                array_push($array_rel,$category);
            }
        }
        return $array_rel;
    }
    public function comprobeCategoriesVisible(){
        $response_bool = False;
        $relations = DB::table('resource_category')->where([['resource_id',$this->id]])->get();
        foreach ($relations as $relation){
            $category = Category::find($relation->category_id);
            $arr = $category->attributesToArray();
            if($arr['visible'] == 1){
                $response_bool = True;
                
            }
        }
        return $response_bool;
    }
    public function categories()
    {
        $categories = $this->belongsToMany(Category::class, 'resource_category', 'resource_id', 'category_id');
        
        return $categories;
    }

    public function getIconPath($category)
    {     
        if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') { 
            $protocol = 'https://';
          }else{
            $protocol = 'http://';
          }
        $base_url = $protocol.request()->getHttpHost().'/storage/icons/';
        $url= $base_url.$category->icon;
        $category->icon = $url;
        
        

    }
    public function getRandomImage()
    {
        if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') { 
            $protocol = 'https://';
          }else{
            $protocol = 'http://';
          }
        
        $base_url = $protocol.request()->getHttpHost().'/storage/categories/';

        $base_url_default = $protocol.request()->getHttpHost().'/storage/categories/devocionales/devocionales_02.jpg';
        foreach($this->categories as $category){
            $image = ImageCategory::where([['category_id',$category->id]])->inRandomOrder()->first();
          //  dd($image);
            if(isset($image->id)){
                $this->randomImage = $base_url.$image->image;
            }else{
                $this->randomImage = $base_url_default;
            }
        }
        
    }

    /**
     * Get all of the resource's likes.
     */

    public function likes()
    {
        return $this->hasMany('App\Models\Like');
    }

    /**
     * Get all of the resource's likes.
     */
    public function likesCount()
    {
        return $this->likes()
            ->selectRaw('resource_id, count(*) as total')->groupBy('resource_id');
    }

    public function collection()
    {
        return $this->belongsToMany(Collection::class, 'resource_collection', 'resource_id', 'collection_id');
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'resource_tag', 'resource_id', 'tag_id');
    }

    public function downloads()
    {
        return $this->hasMany('App\Models\ResourceDownload');
    }

    public function downloadsCount()
    {
        $total = DB::table('resource_downloads')
        ->selectRaw('count(*) as total')
        ->where('resource_id', $this->id)
        ->get();
        return $total[0]->total;
       
    }
    public function scopeOnlySearch($query, $search)
    {
        if ($search) {
            return $query->where('title', 'like', '%' . $search . '%')
                ->orWhere('review', 'like', '%' . $search . '%')
                ->orWhere('content', 'like', '%' . $search . '%');
        }

        return $query;
    }

    public function scopeOnlyCategory($query, $category_id)
    {
        if ($category_id) {
            return $query->where('category_id', $category_id);
        }

        return $query;
    }

    public function scopePublished($query)
    {
        return $query->where('published', 1);
    }

    public function scopePublishedBy($query, $author)
    {
        return $query->where('user_id', $author);
    }

    public function getLocalAttachmentAttribute()
    {
        return env('APP_URL') . "/storage/resources/{$this->attachment}";
    }

    /*public function getS3AttachmentAttribute()
    {
        return "https://apocalo-pl.s3.amazonaws.com/{$this->attachment}";
    }*/
    
    public function getS3AttachmentAttribute()
    {
        return 1;
        //return $this->getFirstMediaUrl('resources');
    }

    public function read()
    {
        return $this->hasMany(ResourceRead::class);
    }

    public static function laratablesUserRelationQuery()
    {
        return function ($query) {
            $query->with('resources');
        };
    }

    public static function laratablesCustomCategories($resource)
    {
        return $resource->categories->implode('label', ', ');
    }

    public static function laratablesCustomStatus($resource)
    {
        $resource = DB::table('resources')->where('id', $resource->id)->first();
        return view('partials.laratables.status', compact('resource'))->render();
    }

    public static function laratablesCustomAction($resource)
    {
        $resource = DB::table('resources')->where('id', $resource->id)->first();
        return view('partials.laratables.action', compact('resource'))->render();
    }

    public function ratings()
    {
        return $this->hasMany(Rating::class);
    }

    public function getRatingAttribute()
    {
        $ratings = $this->ratings;
        if ($ratings->count() == 0) {
            return 0;
        }

        $carry = $ratings->pluck('total_rating')->sum();
        return floor($carry / $ratings->count());
    }
}
