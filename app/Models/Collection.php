<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Collection extends Model
{
    use SoftDeletes;

    protected $table = 'collections';
    //protected $guarded = ['id'];
      protected $fillable = ['user_id','label','slug','description','status','former_id','former_parent_id'];

    

    /*public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }*/

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'collection_category', 'collection_id', 'category_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User')
            ->select('users.id', 'users.username', 'user_profiles.fullname', 'user_profiles.image', 'users.created_at', 'users.level',)
            ->leftJoin('user_profiles', 'user_profiles.user_id', '=', 'users.id');
    }

    public function resources()
    {
        return $this->belongsToMany(Resource::class, 'resource_collection', 'collection_id','resource_id');
    }

    public function resourcesCount()
    {
        $total = DB::table('resource_collection')
        ->selectRaw('count(*) as total')
        ->where('collection_id', $this->id)
        ->get();
    
        return $total[0]->total;
       
    }

    public function scopePublishedBy($query, $author)
    {
        return $query->where('user_id', $author);
    }

    public function scopeOnlySearch($query, $search)
    {
        if ($search) {
            return $query->where('label', 'like', '%' . $search . '%')
                ->orWhere('description', 'like', '%' . $search . '%');
        }

        return $query;
    }

    public static function laratablesUserRelationQuery()
    {
        return function ($query) {
            $query->with('collections');
        };
    }

    public static function laratablesCustomCategories($collection)
    {
        return $collection->categories->implode('label', ', ');
    }

    public static function laratablesCustomAction($collection)
    {
        $collection = DB::table('collections')->where('id', $collection->id)->first();
        return view('partials.laratables.collections.action', compact('collection'))->render();
    }
}
