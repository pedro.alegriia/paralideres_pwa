<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResourceTag extends Model
{
    protected $table = 'resource_tag';
    protected $guarded = ['id'];

    public function resource()
    {
        return $this->belongsTo(Resource::class);
    }
}
