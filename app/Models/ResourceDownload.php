<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResourceDownload extends Model
{
    protected $table = 'resource_downloads';
    protected $guarded = ['id'];

    public function resource()
    {
        return $this->belongsTo(Resource::class);
    }
}
