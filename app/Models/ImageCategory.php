<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ImageCategory extends Model
{
    protected $table = 'image_categories';
    //protected $guarded = ['id'];
      protected $fillable = ['image','category_id'];
}
