<?php

namespace App\Models;

use App\Badge;
use App\Mail\ResetPassword;
use App\Rating;
use App\TempAvatar;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Support\Facades\Mail;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Resource;
use App\Models\UserProfile;
use StdClass;
//use Spatie\MediaLibrary\HasMedia\HasMedia;
//use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

use Spatie\MediaLibrary\Models\Media;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use HasApiTokens, Notifiable, SoftDeletes, HasRoles, CanResetPassword;

    protected $table = 'users';
    //protected $guarded = ['id'];
    protected $fillable = ['id','is_active','email'];

    public function registerMediaCollections()
    {
        $this->addMediaCollection('profiles')
            ->singleFile();
    }

    public function tempAvatar()
    {
        return $this->hasOne(TempAvatar::class);
    }

    /*public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->width(200)
            ->height(200)
            ->performOnCollections('profiles');
    }*/

    public function sendPasswordResetNotification($token)
    {
        Mail::to($this->email)
            ->send(new ResetPassword($token));
    }

    /**
     * Get the user profile.
     */
    public function profile()
    {
        if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') { 
            $protocol = 'https://';
          }else{
            $protocol = 'http://';
          }
        $profile = UserProfile::select('*')->where([['user_id',$this->id]])->first();
        if(isset($profile->image) && $profile->image != null){
            if(str_contains($profile->image,'profile/')){
                $base_url = $protocol.request()->getHttpHost().'/storage'.'/'.$profile->image;
                $profile->image_path = $base_url;
            }else{
                $base_url = $protocol.request()->getHttpHost().'/images'.'/'.$profile->image;
                $profile->image_path = $base_url;
            }
            
    
        }else{
            $base_url = $protocol.request()->getHttpHost().'/images/PL_ÍCONOS-68.png';
            $profile->image_path = $base_url;
        }
        return $profile;
    }

    /**
     * Get all of the user's resources.
     */
    public function resources()
    {
        return $this->hasMany('App\Models\Resource');
    }

    /**
     * Get all of the user's likes.
     */
    public function likes()
    {
        return $this->hasMany('App\Models\Like');
    }

    /**
     *  Get all the user collections
     */
    public function collections()
    {
        return $this->hasMany('App\Models\Collection');
    }

    /**
     *  Get all the user polls
     */
    public function polls()
    {
        return $this->belongsToMany('App\Models\Poll');
    }

    public function badges()
    {
        return $this->belongsToMany(Badge::class, 'user_badge');
    }

    /**
     *  Get all the user votes
     */
    public function pollVote()
    {
        return $this->hasMany('App\Models\PollVote');
    }

    public static function laratablesCustomFullname($user)
    {
        return $user->profile->fullname;
    }

    public static function laratablesCustomRoles($user)
    {
        return $user->getRoleNames()->implode('', ',');
    }

    public static function laratablesCustomStatus($user)
    {
        $user = User::find($user->id);
        return view('partials.laratables.users.status', compact('user'))->render();
    }

    public static function laratablesCustomAction($user)
    {
        $user = User::find($user->id);
        return view('partials.laratables.users.action', compact('user'))->render();
    }

    public function rating()
    {
        return $this->hasMany(Rating::class);
    }

    public function rankedResources()
    {
        return $this->belongsToMany(Resource::class, 'ratings')->withPivot('rating');
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    
    public function getJWTCustomClaims()
    {
        return [];
    }
}
