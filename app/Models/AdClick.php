<?php

namespace App\Models;

use App\Models\Ad;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdClick extends Model
{
    use SoftDeletes;

    protected $table = 'ad_clicks';
    protected $guarded = ['id'];

    public function ad()
    {
        return $this->belongsTo(Ad::class);
    }
}
