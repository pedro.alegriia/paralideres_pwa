<?php

namespace App\Listeners;

use App\Badge;
use App\Models\User;
use App\Notifications\SendEbookToUser;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateUserLevelListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param object $event
     * @return void
     */
    public function handle($event)
    {
        $user = $event->resource->user;
        $count = $user->resources()->published()->count();

        if ($count >= 1 && $count <= 5) {
            $level = 2;
        } else if ($count >= 6 && $count <= 15) {
            $level = 3;
        } else if ($count > 15) {
            $level = 4;
        }

        /** Falta agregar la lógica de asignación de insignias por nivel */
        if ($user->level < $level && $level == 2) {
            $user = User::find($user->id);
            $ebook = [
                'name' => 'Somos Novios',
                'url' => 'https://paralideres.org',
            ];
            $user->notify(new SendEbookToUser($ebook));
            $user->badges()->attach(Badge::where('name', 'Autor')->first());
        } else if ($user->level < $level && $level == 3) {
            $user = User::find($user->id);
            $ebook = [
                'name' => 'Conocete a ti mismo',
                'url' => 'https://paralideres.org',
            ];
            $user->notify(new SendEbookToUser($ebook));
            $user->badges()->attach(Badge::where('name', 'Colaborador')->first());
        } else if ($user->level < $level && $level == 3) {
            User::find($user->id)->badges()->attach(Badge::where('name', 'Estrella')->first());
        }

        $user->update([
            'level' => $level,
        ]);
    }
}
