<?php

namespace App\Listeners;

use App\Events\RegisteredUser;
use App\Mail\UserActivationCode;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendActivationEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RegisteredUser  $event
     * @return void
     */
    public function handle(RegisteredUser $event)
    {
        Log::info($event->user);
        Mail::to($event->user->email)
            ->send(new UserActivationCode($event->user));
    }
}
