<?php

use Illuminate\Database\Seeder;

class InitialBadgeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $badges = [
            [
                'name' => 'Miembro',
                'icon_name' => 'level_1',
                'description' => 'Insignia obtenida por registrarse en ParaLíderes',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Autor',
                'icon_name' => 'level_2',
                'description' => 'Insignia obtenida por haber publicado un recurso en ParaLíderes',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Colaborador',
                'icon_name' => 'level_3',
                'description' => 'Insignia obtenida por haber publicado más de 6 recursos en ParaLíderes',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Estrella',
                'icon_name' => 'level_4',
                'description' => 'Insignia obtenida por haber publicado más de 15 recursos en ParaLíderes',
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ];

        \App\Badge::insert($badges);
    }
}
