<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\DB;

class RolesAndPermissionsSeeder extends Seeder
{
 
    public function run()
    {
        $admin = Role::create(['name' => 'admin', 'hierarchy' => 1]);
        
        $editor = Role::create(['name' => 'editor', 'hierarchy' => 2]);
        $collaborator = Role::create(['name' => 'collaborator', 'hierarchy' => 3]);
        $moderator = Role::create(['name' => 'moderator', 'hierarchy' => 4]);
        $team_lead = Role::create(['name' => 'team_lead', 'hierarchy' => 5]);
        $team_member = Role::create(['name' => 'team_member', 'hierarchy' => 6]);
        $user = Role::create(['name' => 'user', 'hierarchy' => 7]);
        Permission::create(['name' => 'create resource', 'category' => 'resource']);
        $edit_resource = Permission::create(['name' => 'edit resource', 'category' => 'resource']);
        Permission::create(['name' => 'edit own resource', 'category' => 'resource']);
        Permission::create(['name' => 'ban resource', 'category' => 'resource']);
        Permission::create(['name' => 'delete resource', 'category' => 'resource']);
        Permission::create(['name' => 'delete own resource', 'category' => 'resource']);

        $create_poll = Permission::create(['name' => 'create poll', 'category' => 'poll']);
        $edit_poll = Permission::create(['name' => 'edit poll', 'category' => 'poll']);
        $delete_poll = Permission::create(['name' => 'delete poll', 'category' => 'poll']);
        $vote_poll = Permission::create(['name' => 'vote poll', 'category' => 'poll']);

        Permission::create(['name' => 'create category', 'category' => 'resource category']);
        Permission::create(['name' => 'edit category', 'category' => 'resource category']);
        Permission::create(['name' => 'delete category', 'category' => 'resource category']);

        Permission::create(['name' => 'create tag', 'category' => 'resource tag']);
        Permission::create(['name' => 'edit tag', 'category' => 'resource tag']);
        Permission::create(['name' => 'delete tag', 'category' => 'resource tag']);

        Permission::create(['name' => 'create role', 'category' => 'role']);
        Permission::create(['name' => 'edit role', 'category' => 'role']);
        Permission::create(['name' => 'delete role', 'category' => 'role']);

        Permission::create(['name' => 'create permission', 'category' => 'permission']);
        Permission::create(['name' => 'edit permission', 'category' => 'permission']);
        Permission::create(['name' => 'delete permission', 'category' => 'permission']);

        $editor->givePermissionTo($create_poll);
        $editor->givePermissionTo($edit_resource);
        $this->assignAllPermissionsToAdminRole($admin);

        //User::where('email', 'admin@paralideres.org')->first()->assignRole('admin');
        //User::where('email', 'dorasepal@gmail.com')->first()->assignRole('admin');
        //User::where('email', 'editor@paralideres.org')->first()->assignRole('editor');
        //User::where('email', 'collaborator@paralideres.org')->first()->assignRole('collaborator');
        //User::where('email', 'moderator@paralideres.org')->first()->assignRole('moderator');
        //User::where('email', 'team_lead@paralideres.org')->first()->assignRole('team_lead');
        //User::where('email', 'team_member@paralideres.org')->first()->assignRole('team_member');
    }

    public function assignAllPermissionsToAdminRole($admin)
    {
        Permission::all()
            ->each(function ($permission) use ($admin) {
                $admin->givePermissionTo($permission);
            });
    }
}
