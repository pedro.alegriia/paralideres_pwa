<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
class UsersTableSeeder extends Seeder
{

    public function run()
    {
        $user = User::create([
            'username' => 'Admin',
            'email' => 'admin@paralideres.org',
            'password' => bcrypt('secreto'),
            'former_pwd' => '',
            'is_active' => true,
            'activation_token' => bcrypt('token')
        ]);
        DB::table('user_profiles')->insert([
            'user_id' => $user->id
        ]);

        $user2 = User::create([
            'username' => 'User',
            'email' => 'user@paralideres.org',
            'password' => bcrypt('secreto'),
            'former_pwd' => '',
            'is_active' => true,
            'activation_token' => bcrypt('token')
        ]);
    }
}
