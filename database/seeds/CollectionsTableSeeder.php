<?php

use App\Models\Category;
use App\Models\Collection;
use App\Models\Resource;
use Illuminate\Database\Seeder;

class CollectionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Collection::class, 20)->create();

        Collection::get()->each(function ($collection) {
            $ids = Resource::orderByRaw("RAND()")->limit(rand(0, 5))->get()->pluck('id')->toArray();
            $collection->resources()->sync($ids);
        });

        Collection::all()->each(function ($collection) {
            $ids = Category::orderByRaw("RAND()")->limit(rand(1, 3))->get()->pluck('id')->toArray();
            $collection->categories()->sync($ids);
        });
    }
}
