<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(CategoryTableSeeder::class);
        $this->call(TagTableSeeder::class);
        $this->call(RolesAndPermissionsSeeder::class);
        $this->call(PollTableSeeder::class);
        $this->call(AdsTableSeeder::class);
        $this->call(GalleryTableSeeder::class);
        // Default Factory
        //otro
        // factory(App\Models\User::class, 10)->create();
        // factory(App\Models\Category::class, 5)->create();
        factory(App\Models\Collection::class, 50)->create();

        factory(App\Models\Resource::class, 50)->create();
        $this->call(ResourceCategoriesTableSeeder::class);
        //$this->call(ResourceTagsTableSeeder::class);
        $this->call(CollectionsTableSeeder::class);
        $this->call(ImageCategoriesTableSeeder::class);


    }
}
