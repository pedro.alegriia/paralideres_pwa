<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        DB::table('categories')->delete();
        DB::table('categories')->insert([
            [
                'label' => 'Dinámicas y Juegos',
                'slug' => 'dinamicas-y-juegos',
                'description' => '',
                'visible'=>1,
                'former_id' => 29,
                'icon'=>'icon_dinamicas.png',
                'icon_color'=>'#FFAE2B',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'label' => 'Sketch y Teatro',
                'slug' => 'sketch-y-teatro',
                'description' => '',
                'visible'=>1,
                'former_id' => 73,
                'icon'=>'icon_teatro.png',
                'icon_color'=>'#F1C50E',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'label' => 'Estudios Biblicos',
                'slug' => 'estudios-biblicos',
                'description' => '',
                'visible'=>1,
                'former_id' => 67,
                'icon'=>'icon_estudios_biblicos.png',
                'icon_color'=>'#E77A3D',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'label' => 'Vídeos',
                'slug' => 'videos',
                'description' => '',
                'visible'=>1,
                'former_id' => null,
                'icon'=>'icon_videos.png',
                'icon_color'=>'#E2E2E2',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'label' => 'Capacitación',
                'slug' => 'capacitacion',
                'description' => '',
                'visible'=>1,
                'former_id' => 20,
                'icon'=>'icon_capacitacion.png',
                'icon_color'=>'#4A6274',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'label' => 'Para padres',
                'slug' => 'para-padres',
                'description' => '',
                'visible'=>1,
                'former_id' => 200,
                'icon'=>'icon_para_padres.png',
                'icon_color'=>'#E77A3D',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'label' => 'Consejos y Tips',
                'slug' => 'consejos-y-tips',
                'description' => '',
                'visible'=>1,
                'former_id' => 22,
                'icon'=>'icon_consejos_tips.png',
                'icon_color'=>'#2D6DC9',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'label' => 'Cultura Juvenil',
                'slug' => 'cultura-juvenil',
                'description' => '',
                'visible'=>1,
                'former_id' => null,
                'icon'=>'icon_cultura_juvenil.png',
                'icon_color'=>'#2BBE9F',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'label' => 'Devocionales',
                'slug' => 'devocionales',
                'description' => '',
                'visible'=>1,
                'icon'=>'icon_para_padres.png',
                'icon_color'=>'#6699CC',
                'former_id' => 91,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'label' => 'Desarrollo Personal',
                'slug' => 'desarrollo-personal',
                'description' => '',
                'visible'=>1,
                'former_id' => null,
                'icon'=>'icon_desarollo_personal.png',
                'icon_color'=>'#CB614C',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'label' => 'Reflexiones',
                'slug' => 'reflexiones',
                'description' => '',
                'visible'=>1,
                'former_id' => null,
                'icon'=>'icon_reflexiones.png',
                'icon_color'=>'#C594C5',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'label' => 'Libros',
                'slug' => 'libros',
                'description' => '',
                'visible'=>1,
                'former_id' => 341,
                'icon'=>'icon_libros.png',
                'icon_color'=>'#2BC880',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'label' => 'NO MOSTRAR',
                'slug' => 'no-mostrar',
                'description' => '',
                'visible'=>0,
                'former_id' => 13,
                'icon'=>'',
                'icon_color'=>'',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'label' => 'Misceláneo',
                'slug' => 'miscelaneo',
                'description' => '',
                'visible'=>1,
                'former_id' => 14,
                'icon'=>'icon_micelanio.png',
                'icon_color'=>'#2A80B9',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'label' => 'No identificado',
                'slug' => 'no-identificado',
                'description' => '',
                'visible'=>0,
                'former_id' => 14,
                'icon'=>'',
                'icon_color'=>'',
                'created_at' => now(),
                'updated_at' => now()
            ],
        ]);
    }
}
