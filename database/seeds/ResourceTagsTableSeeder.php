<?php

use App\Models\Resource;
use App\Models\Tags;
use Illuminate\Database\Seeder;

class ResourceTagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Resource::all()->each(function ($resource) {
            $ids = Tags::orderByRaw("RAND()")->limit(rand(0, 2))->get()->pluck('id')->toArray();
            $resource->tags()->sync($ids);
        });
    }
}
