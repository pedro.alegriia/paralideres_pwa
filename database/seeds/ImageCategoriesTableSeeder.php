<?php

use Illuminate\Database\Seeder;
use App\Models\ImageCategory;

class ImageCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        DB::table('image_categories')->delete();
        DB::table('image_categories')->insert([
            [
                'image' => 'dinamicas/dinámicas-y-juegos_01.jpg', 'category_id' => 1
            ],
            [
                'image' => 'dinamicas/dinámicas-y-juegos_02.jpg', 'category_id' => 1
            ],
            [
                'image' => 'dinamicas/dinámicas-y-juegos_03.jpg', 'category_id' => 1
            ],
            [
                'image' => 'dinamicas/dinámicas-y-juegos_04.jpg', 'category_id' => 1
            ],
            [
                'image' => 'dinamicas/dinámicas-y-juegos_05.jpg', 'category_id' => 1
            ],
            [
                'image' => 'teatro/sketch-y-teatro_01.jpg', 'category_id' => 2
            ],
            [
                'image' => 'teatro/sketch-y-teatro_02.jpg', 'category_id' => 2
            ],
            [
                'image' => 'teatro/sketch-y-teatro_03.jpg', 'category_id' => 2
            ],
            [
                'image' => 'teatro/sketch-y-teatro_04.jpg', 'category_id' => 2
            ],
            [
                'image' => 'teatro/sketch-y-teatro_05.jpg', 'category_id' => 2
            ],
            [
                'image' => 'estudios/estudios-bíblicos_01.jpg', 'category_id' => 3
            ],
            [
                'image' => 'estudios/estudios-bíblicos_02.jpg', 'category_id' => 3
            ],
            [
                'image' => 'estudios/estudios-bíblicos_03.jpg', 'category_id' => 3
            ],
            [
                'image' => 'estudios/estudios-bíblicos_04.jpg', 'category_id' => 3
            ],
            [
                'image' => 'estudios/estudios-bíblicos_05.jpg', 'category_id' => 3
            ],
              [
                'image' => 'video/video_01.jpg', 'category_id' => 4
            ],
            [
                'image' => 'video/video_02.jpg', 'category_id' => 4
            ],
            [
                'image' => 'video/video_03.jpg', 'category_id' => 4
            ],
            [
                'image' => 'video/video_04.jpg', 'category_id' => 4
            ],
            [
                'image' => 'video/video_05.jpg', 'category_id' => 4
            ],
            [
                'image' => 'capacitacion/capacitación_01.jpg', 'category_id' => 5
            ],
              [
                'image' => 'capacitacion/capacitación_02.jpg', 'category_id' => 5
            ],
            [
                'image' => 'capacitacion/capacitación_03.jpg', 'category_id' => 5
            ],
              [
                'image' => 'capacitacion/capacitación_04.jpg', 'category_id' => 5
            ],
            [
                'image' => 'capacitacion/capacitación_05.jpg', 'category_id' => 5
            ],
            [
                'image' => 'padres/para-padres_01.jpg', 'category_id' => 6
            ],
            [
                'image' => 'padres/para-padres_02.jpg', 'category_id' => 6
            ],
            [
                'image' => 'padres/para-padres_03.jpg', 'category_id' => 6
            ],
            [
                'image' => 'padres/para-padres_04.jpg', 'category_id' => 6
            ],
            [
                'image' => 'padres/para-padres_05.jpg', 'category_id' => 6
            ],
            [
                'image' => 'consejos/consejos-y-tips_01.jpg', 'category_id' => 7
            ],
            [
                'image' => 'consejos/consejos-y-tips_02.jpg', 'category_id' => 7
            ],
            [
                'image' => 'consejos/consejos-y-tips_03.jpg', 'category_id' => 7
            ],
            [
                'image' => 'consejos/consejos-y-tips_04.jpg', 'category_id' => 7
            ],
            [
                'image' => 'consejos/consejos-y-tips_05.jpg', 'category_id' => 7
            ],
            [
                'image' => 'cultura/cultura-juvenil_01.jpg', 'category_id' => 8
            ],
            [
                'image' => 'cultura/cultura-juvenil_02.jpg', 'category_id' => 8
            ],
            [
                'image' => 'cultura/cultura-juvenil_03.jpg', 'category_id' => 8
            ],
            [
                'image' => 'cultura/cultura-juvenil_04.jpg', 'category_id' => 8
            ],
            [
                'image' => 'cultura/cultura-juvenil_05.jpg', 'category_id' => 8
            ],
            [
                'image' => 'devocionales/devocionales_01.jpg', 'category_id' => 9
            ],
            [
                'image' => 'devocionales/devocionales_02.jpg', 'category_id' => 9
            ],
            [
                'image' => 'devocionales/devocionales_03.jpg', 'category_id' => 9
            ],
            [
                'image' => 'devocionales/devocionales_04.jpg', 'category_id' => 9
            ],
            [
                'image' => 'devocionales/devocionales_05.jpg', 'category_id' => 9
            ],
            [
                'image' => 'desarrollo/desarrollo-personal_01.jpg', 'category_id' => 10
            ],
            [
                'image' => 'desarrollo/desarrollo-personal_02.jpg', 'category_id' => 10
            ],
            [
                'image' => 'desarrollo/desarrollo-personal_03.jpg', 'category_id' => 10
            ],
            [
                'image' => 'desarrollo/desarrollo-personal_04.jpg', 'category_id' => 10
            ],
            [
                'image' => 'desarrollo/desarrollo-personal_05.jpg', 'category_id' => 10
            ],

            [
                'image' => 'reflexiones/reflexión_01.jpg', 'category_id' => 11
            ],
            [
                'image' => 'reflexiones/reflexión_02.jpg', 'category_id' => 11
            ],
            [
                'image' => 'reflexiones/reflexión_03.jpg', 'category_id' => 11
            ],
            [
                'image' => 'reflexiones/reflexión_04.jpg', 'category_id' => 11
            ],
            [
                'image' => 'reflexiones/reflexión_05.jpg', 'category_id' => 11
            ],

            [
                'image' => 'libros/libros_01.jpg', 'category_id' => 12
            ],
            [
                'image' => 'libros/libros_02.jpg', 'category_id' => 12
            ],
            [
                'image' => 'libros/libros_03.jpg', 'category_id' => 12
            ],
            [
                'image' => 'libros/libros_04.jpg', 'category_id' => 12
            ],
            [
                'image' => 'libros/libros_05.jpg', 'category_id' => 12
            ],


        ]);

    }
}
