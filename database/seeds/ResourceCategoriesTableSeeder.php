<?php

use App\Models\Category;
use App\Models\Resource;
use Illuminate\Database\Seeder;

class ResourceCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Resource::all()->each(function ($resource) {
            $ids = Category::orderByRaw("RAND()")->limit(rand(1, 3))->get()->pluck('id')->toArray();
            $resource->categories()->sync($ids);
        });
    }
}
