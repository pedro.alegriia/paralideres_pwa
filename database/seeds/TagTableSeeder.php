<?php

use App\Models\Tag;
use Illuminate\Database\Seeder;

class TagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tags = [
            ['label' => 'Iglesia', 'slug' => 'iglesia'],
            ['label' => 'Lideres', 'slug' => 'lideres'],
            ['label' => 'Enseñanza', 'slug' => 'enseñanza'],
            ['label' => 'Jovenes', 'slug' => 'jovenes'],
            ['label' => 'Internet', 'slug' => 'internet'],
            ['label' => 'Para lideres', 'slug' => 'para-lideres'],
            ['label' => 'Tecnología', 'slug' => 'tecnología'],
            ['label' => 'Recursos bíblicos', 'slug' => 'recursos-biblicos'],
        ];

        DB::table('tags')->insert($tags);
    }
}
