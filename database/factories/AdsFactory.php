<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Ad::class, function (Faker $faker) {
   
    return [
    	'uuid' => $faker->uuid,
        'user_id' => 1,
        'name' => $faker->name,
        'description' => $faker->sentences(3, true),
        'link' =>  $faker->sentences(3, true),
        'impressions' => 0,
        'starts_at' => $faker->dateTimeThisMonth()->format('Y-m-d H:i:s'),
        'ends_at' => $faker->dateTimeThisMonth()->format('Y-m-d H:i:s'),

    ];
});
 