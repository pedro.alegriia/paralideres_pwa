<?php

use Faker\Generator as Faker;

$factory->define(App\Models\PollVote::class, function (Faker $faker) {
    return [
        'user_id' => random_int(1,2),
        'poll_id' => random_int(1,10),
        'poll_options_id' => random_int(1,50),
    ];
});
