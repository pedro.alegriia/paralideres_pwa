<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Resource::class, function (Faker $faker) {
    $title = $faker->sentences(1, true);
    $title_slug = str_replace(' ', '-', $title);
    return [
        'user_id' => rand(1, 6),
        'title' => $title,
        'review' => $faker->sentences(5, true),
        'slug' => $title_slug,
        'attachment' => '',
        'content' => $faker->paragraph(100, true),
        'published' => rand(0, 1),
    ];
});
