<?php

use Faker\Generator as Faker;

$factory->define(App\Gallery::class, function (Faker $faker) {
   
    return [
    
        'order' => 1,
        'type' =>  $faker->sentences(3, true),
        'image' =>  $faker->image('public/storage/images',640,480, null, false),
        'status' => 0,
        

    ];
});
