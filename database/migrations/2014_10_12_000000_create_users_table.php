<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username')->unique();
            $table->string('email', 50)->unique();
            $table->string('password');
            $table->string('former_pwd')->nullable();
            $table->integer('former_id')->unsigned()->nullable();
            $table->rememberToken();
            $table->boolean('subscribe')->default(false);
            $table->boolean('is_active')->default(false);
            $table->string('activation_token')->nullable();
            $table->boolean('verified')->default(false);
            $table->string('verification_token')->nullable();
            $table->integer('rol_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
