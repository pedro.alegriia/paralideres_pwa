<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resources', function (Blueprint $table) {
            $table->increments('id');
            //$table->unsignedInteger('category_id');
            $table->integer('user_id')->unsigned();
            $table->string('title');
            $table->string('slug');
            $table->text('review')->nullable();
            $table->mediumText('content')->nullable();
            $table->string('attachment')->nullable();
            $table->text('video_url')->nullable();
            $table->boolean('published')->default(0);
            $table->integer('former_id')->unsigned()->nullable();
            $table->integer('former_section_id')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('resources');
    }
}
