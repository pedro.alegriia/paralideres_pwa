<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePollsTable extends Migration
{
  
    public function up()
    {
        Schema::create('polls', function (Blueprint $table) {
            $table->increments('id');
            $table->string('question');
            $table->date('date_from');
            $table->date('date_to');
            $table->boolean('active');
            $table->integer('former_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('poll_options', function (Blueprint $table) {
            $table->increments('id');
            $table->string('option');
            $table->integer('index')->unsigned();
            $table->integer('poll_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('poll_votes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('poll_id')->unsigned()->index();
            $table->integer('poll_options_id')->unsigned()->index();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('poll_votes');
        Schema::drop('poll_options');
        Schema::drop('polls');
    }
}
