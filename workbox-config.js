module.exports = {
  "globDirectory": "public/",
  "globPatterns": [
    "**/*.{css,scss,svg,less,eot,ttf,woff,woff2,js,config,json,md,yml,html,png,ico,otf,jpg,webp,gif,php,txt,pdf}"
  ],
  "swDest": "public/sw.js"
};