{
    module.exports = {
      printWidth: 150,
  
      singleQuote: true,
  
      trailingComma: 'none',
  
      bracketSpacing: true,
  
      jsxBracketSameLine: false,
  
      semi: true,
  
      requirePragma: false,
  
      proseWrap: 'preserve',
  
      arrowParens: 'always'
    };
  }