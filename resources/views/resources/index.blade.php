@extends('layouts.layout')
@section('content')
    {{--<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-559b5a0c386679ae"
            async="async">
    </script>--}}

    @php
        $tag_slug = (isset($_GET['tag'])) ? $_GET['tag'] : '';
        $cat_slug = (isset($_GET['category'])) ? $_GET['category'] : '';
        $author = (isset($_GET['author'])) ? $_GET['author'] : '';
    @endphp

    <resources-list></resources-list>
@endsection

@section('scripts')
    {{--<script>
        var search_text = '<?php $s = (isset($_GET['search_text'])) ? $_GET['search_text'] : ''; echo 'search_text=' . $s;?>';
        var tag_slug = '<?php echo '&tag_slug=' . $tag_slug;?>';
        var cat_slug = '<?php echo '&cat_slug=' . $cat_slug;?>';
        var author = '<?php echo '&author=' . $author;?>';
    </script>--}}
    <{{--script type="text/javascript" src="{{asset('js/resource_list.js')}}?js={{uniqid()}}"></script>--}}
@endsection
