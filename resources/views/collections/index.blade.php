@extends('layouts.layout')
@section('content')
    {{--<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-559b5a0c386679ae"
            async="async">
    </script>--}}

    @php
        $tag_slug = (isset($_GET['tag'])) ? $_GET['tag'] : '';
        $cat_slug = (isset($_GET['category'])) ? $_GET['category'] : '';
        $author = (isset($_GET['author'])) ? $_GET['author'] : '';
    @endphp

    <collections-list></collections-list>
@endsection
