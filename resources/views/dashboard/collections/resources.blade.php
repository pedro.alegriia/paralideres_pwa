@extends('layouts.dashboard')

@section('content')
    <div id="collections-list" class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Colecciones</a></li>
                    <li class="breadcrumb-item active">Administrar recursos</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body">
                        @include('dashboard.partials.flash-message')
                        <h4 class="card-title">Recursos de una colección</h4>
                        <div class="card-body">
                            <input type="hidden" id="collection_id" value="{{$collection->id}}">
                            <div class="row">
                                <div class="col">
                                    <p>Colección: <span>{{$collection->label}}</span></p>
                                    <p>Categorías:
                                        <span>{{$collection->categories->pluck('label')->implode(', ')}}</span></p>
                                    <p>Número de recursos: {{$collection->resources->count()}}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col border border-dark py-3">
                                    <h3>Todos los recursos</h3>
                                    <hr>
                                    <div class="row">
                                        <div class="col-xs-12 col-md-9">
                                            <input type="text" class="form-control" v-model="query"
                                                   v-on:keyup.enter="getResourcesByQuery"
                                                   placeholder="Escribe el nombre del recurso">
                                        </div>
                                        <div class="col-xs-12 col-md-3 mt-xs-2 mt-md-0">
                                            <button class="btn btn-secondary btn-block" :disabled="searching"
                                                    v-if="searching">
                                                <i class="fa fa-spinner fa-spin"></i>
                                            </button>
                                            <button class="btn btn-success btn-block" @click="getResourcesByQuery"
                                                    v-else>
                                                Buscar
                                            </button>
                                        </div>
                                    </div>
                                    <div style="height: 50vh; overflow-y: scroll;">
                                        <table class="table mt-3">
                                            <tr v-if="query !== '' && !searching && !resources.length">
                                                <td class="text-center">No sé encontró ninguna coincidencia</td>
                                            </tr>
                                            <tr v-for="(resource, index) in resources">
                                                <td>@{{resource.title}}</td>
                                                <td>
                                                    <button :class="[resource.added ? 'btn-secondary' :  'btn-warning',  'btn-sm float-right']"
                                                            :disabled="resource.added"
                                                            v-text="resource.added ? 'Agregado' : 'Agregar'"
                                                            @click="addToCollectionResources(resource)">
                                                    </button>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="col border border-dark py-3">
                                    <h3>Recursos de esta colección</h3>
                                    <div style="height: 50vh; overflow-y: scroll;">
                                        <table class="table">
                                            <tr v-for="(resource, index) in collection_resources">
                                                <td>@{{resource.title}}</td>
                                                <td>
                                                    <button class="btn-danger btn-sm float-right"
                                                            @click="addToResources(resource)">Quitar
                                                    </button>
                                                </td>
                                            </tr>
                                            <tr v-show="!collection_resources.length">
                                                <td>Esta colección no tiene recursos</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col text-center pt-5">
                                    <button class="btn btn-lg btn-success" @click="saveChanges">Guardar cambios
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Row -->

    </div>
    @include('dashboard.partials.footer')
@endsection
@section('script')
    <script>
        const app = new Vue({
            el: '#collections-list',
            data() {
                return {
                    resources: [],
                    collection_resources: [],
                    query: '',
                    searching: false,
                }
            },
            mounted() {
                let collection_id = document.querySelector('#collection_id').value;
                this.getResourcesByCollection(collection_id)
            },
            methods: {
                async getResourcesByCollection(collection_id) {
                    let response = await axios.get(`/webapi/collections/${collection_id}/resources`);
                    console.log(response);
                    this.collection_resources = response.data.collection_resources.sort(this.compare);
                },
                async getResourcesByQuery() {
                    this.searching = true;
                    let response = await axios.get(`/webapi/resources-by-query?search=${this.query}`);
                    let resources = response.data.resources;

                    resources = resources.map(resource => {
                        resource.added = this.collection_resources.map(resource => resource.id).indexOf(resource.id) >= 0;
                        return resource;
                    });

                    this.resources = response.data.resources.sort(this.compare);
                    this.searching = false;
                },
                addToCollectionResources(targetResource) {
                    this.resources.forEach(resource => {
                        if (resource.id === targetResource.id) {
                            resource.added = true;
                        }
                    });
                    this.collection_resources.push(targetResource);
                    this.collection_resources.sort(this.compare);
                },
                removeFromResources(targetResource) {
                    this.resources = this.resources.filter(resource => {
                        return resource.id !== targetResource.id;
                    });
                },
                addToResources(targetResource) {
                    this.removeFromCollection(targetResource);
                    this.resources = this.resources.map(resource => {
                        resource.added = this.collection_resources.map(resource => resource.id).indexOf(resource.id) >= 0;
                        return resource;
                    });
                },
                removeFromCollection(targetResource) {
                    this.collection_resources = this.collection_resources.filter(resource => {
                        return resource.id !== targetResource.id;
                    });
                },
                saveChanges() {
                    let collection_id = document.querySelector('#collection_id').value;
                    let data = {
                        collection_id: collection_id,
                        resources: this.collection_resources.map(resource => resource.id),
                    };

                    axios.post('/webapi/collections/sync/resources', data)
                        .then(res => {
                            console.log(res);
                            if (res.data.status) {
                                //alert(res.data.message);
                                Swal.fire({
                                    title: res.data.message,
                                    icon: 'success',
                                    timer: 3000,
                                });
                            }
                        });
                },
                compare(a, b) {
                    const A = a.title.toUpperCase();
                    const B = b.title.toUpperCase();

                    let comparison = 0;

                    if (A > B) {
                        comparison = 1;
                    } else if (A < B) {
                        comparison = -1;
                    }

                    return comparison;
                }
            },
        });
    </script>
@endsection
