@extends('layouts.dashboard')

@section('content')
    <div id="collections-list" class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                {{--<h3 class="text-themecolor mb-0 mt-0">Recursos</h3>--}}
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Colecciones</a></li>
                    <li class="breadcrumb-item active">Listado</li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body">
                        @include('dashboard.partials.flash-message')
                        <h4 class="card-title">Listado de colecciones</h4>
                         <table id="example" class="table table-striped table-bordered" style="width: 100%;">
                            <thead>
                            <tr>
                                <th>Titulo</th>
                                <th>Categoria</th>
                                <th>Autor</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach ($collections as $collection)
                                <tr>
                                    <td>{{$collection->label}}</td>
                                    <td>{{$collection->categories->pluck('label')->implode(', ')}}</td>
                                    <td>{{$collection->user->username}}</td>
                                    <td>
                                        <a class="btn btn-info btn-sm" target="_blank"
                                            href="{{route('collections.show', $collection->slug)}}"
                                           href="/app/collections/{{$collection->slug}}">
                                            <i class="mdi mdi-eye"></i> Ver
                                        </a>
                                        <a class="btn btn-warning btn-sm"
                                           href="/admin/collections/{{$collection->slug}}/edit">
                                            <i class="mdi mdi-pencil"></i> Editar
                                        </a>
                                        <a class="btn btn-success btn-sm"
                                           href="/admin/collections/resources/{{$collection->id}}">
                                            <i class="mdi mdi-playlist-check"></i> Recursos
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        
                    </div>
                </div>
            </div>
        </div>

        {{--<div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body">
                        @include('dashboard.partials.flash-message')
                        <h4 class="card-title">Listado de colecciones</h4>
                        <table class="table table-striped table-hover table-responsive-md">
                            <thead>
                            <tr>
                                <th>Titulo</th>
                                <th>Categoria</th>
                                <th>Autor</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($collections as $collection)
                                <tr>
                                    <td>{{$collection->label}}</td>
                                    <td>{{$collection->categories->pluck('label')->implode(', ')}}</td>
                                    <td>{{$collection->user->username}}</td>
                                    <td>
                                        <a class="btn btn-info btn-sm" target="_blank"
                                           --}}{{--href="{{route('collections.show', $collection->slug)}}"--}}{{--
                                           href="/app/collections/{{$collection->slug}}">
                                            <i class="mdi mdi-eye"></i> Ver
                                        </a>
                                        <a class="btn btn-warning btn-sm"
                                           href="/admin/collections/{{$collection->slug}}/edit">
                                            <i class="mdi mdi-pencil"></i> Editar
                                        </a>
                                        <a class="btn btn-success btn-sm"
                                           href="/admin/collections/resources/{{$collection->id}}">
                                            <i class="mdi mdi-playlist-check"></i> Recursos
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{$collections->links()}}
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>--}}
    </div>
    @include('dashboard.partials.footer')
@endsection
@section('script')
    <script>
 $("#exampleModal").modal("show"); $("#exampleModal").css("z-index", "1042");
</script>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" defer/>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js" defer ></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js" defer></script>
<script type="text/javascript">
  $(document).ready( function () {
    $('#example').DataTable();
} );
</script>
@endsection
