@extends('layouts.dashboard')

@section('content')
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                {{--<h3 class="text-themecolor mb-0 mt-0">Recursos</h3>--}}
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Colecciones</a></li>
                    <li class="breadcrumb-item active">Crear</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body">
                        @include('dashboard.partials.flash-message')
                        <h4 class="card-title">Editar colección</h4>
                        {{--<h6 class="card-subtitle"> All with bootstrap element classies </h6>--}}
                        <form id="form-create-poll" class="mt-4" action="{{route('collections.update', $collection->id)}}" method="POST">
                            {{method_field('PUT')}}
                            {{csrf_field()}}
                            <div class="form-group">
                                <label>Titulo</label>
                                <input type="text" class="form-control" name="label" aria-describedby="emailHelp"
                                       value="{{$collection->label}}"/>
                            </div>
                            <div class="form-group">
                                <label>Categorías</label>
                                <select class="select2 mb-2 select2-multiple" multiple="multiple" style="width: 100%"
                                        name="categories[]" data-placeholder="Elige al menos una categoría">
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{$category->label}}</option>
                                    @endforeach
                                </select>
                                <input type="hidden" id="categories"
                                       value="{{$collection->categories->pluck('id')->implode(',')}}">
                            </div>
                            <div class="form-group">
                                <label>Descripción</label>
                                <textarea name="description" class="form-control"
                                          rows="4">{{$collection->description}}</textarea>
                            </div>
                            <button type="submit" class="btn btn-primary">Actualizar colección</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('dashboard.partials.footer')
@endsection

@section('script')
    <script>
        const app = new Vue({
            el: '#form-create-poll',
            data() {
                return {
                    responses: [],
                    date_from: null,
                    date_to: null,
                }
            },
            mounted() {
                $(".select2").select2();
                let selectedCategories = document.querySelector('#categories').value;
                $(".select2").val(selectedCategories.split(','));
                $(".select2").trigger('change');

                //$(".select2").append(new Option('Hola', 100, true, true));
            },
            methods: {},
        });
    </script>
@endsection
