@extends('layouts.dashboard')

@section('content')
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                {{--<h3 class="text-themecolor mb-0 mt-0">Recursos</h3>--}}
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Colecciones</a></li>
                    <li class="breadcrumb-item active">Crear</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body">
                        @include('dashboard.partials.flash-message')
                        <h4 class="card-title">Crear colección</h4>
                        {{--<h6 class="card-subtitle"> All with bootstrap element classies </h6>--}}
                        <form id="form-create-poll" class="mt-4" action="{{route('collections.store')}}" method="POST">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label>Titulo</label>
                                <input type="text" class="form-control" name="label" aria-describedby="emailHelp"/>
                            </div>
                            <div class="form-group">
                                <label>Categorías</label>
                                <select class="select2 mb-2 select2-multiple" style="width: 100%" multiple="multiple"
                                        data-placeholder="Elige al menos una categoría" name="categories[]">
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{$category->label}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Descripción</label>
                                <textarea name="description" class="form-control" rows="4"></textarea>
                            </div>
                            <button type="submit" class="btn btn-primary">Crear colección</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('dashboard.partials.footer')
@endsection

@section('script')
    <script>
        const app = new Vue({
            el: '#form-create-poll',
            data() {
                return {
                    responses: [],
                    date_from: null,
                    date_to: null,
                }
            },
            mounted() {
                let today = new Date().toISOString().slice(0, 10);
                this.date_from = today;
                this.date_to = today;
                $(".select2").select2();
            },
            methods: {
                addResponse() {
                    this.responses.push("");
                },
                deleteResponse(key) {
                    this.responses.splice(key, 1);
                },
            },

        });
    </script>
@endsection
