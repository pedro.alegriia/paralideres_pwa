<aside class="left-sidebar">
    <div class="scroll-sidebar">
        <div class="user-profile">
            <div class="profile-img">
              
            </div>
            <div class="profile-text">
                <a href="#">Username</a>
            {{-- auth()->user()->name --}}
            </div>
        </div>
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li>
                    <a class="has-arrow" href="#" aria-expanded="false">
                        <i class="mdi mdi-cube-outline"></i>
                        <span class="hide-menu">Recursos</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{route('recursos.index')}}">Listado</a></li>
                    </ul>
                </li>
                <li>
                    <a class="has-arrow" href="#" aria-expanded="false">
                        <i class="mdi mdi-buffer"></i>
                        <span class="hide-menu">Colecciones</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{route('collections.index')}}">Listado</a></li>
                        {{--<li><a href="{{route('collections.create')}}">Nueva</a></li>--}}
                    </ul>
                </li>

      
                <li>
                    <a class="has-arrow" href="#" aria-expanded="false">
                        <i class="mdi mdi-account-multiple"></i>
                        <span class="hide-menu">Usuarios</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{route('usuarios.index')}}">Listado</a></li>
                    </ul>
                </li>
                <li>
                    <a class="has-arrow" href="#" aria-expanded="false">
                        <i class="mdi mdi-buffer"></i>
                        <span class="hide-menu">Categorías</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{route('categories.list')}}">Listado</a></li>
                    </ul>
                </li> 
                <li>
                    <a class="has-arrow" href="#" aria-expanded="false">
                        <i class="mdi mdi-buffer"></i>
                        <span class="hide-menu">Carrousel</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{route('gallery.index')}}">Listado</a></li>
                        <li><a href="{{route('gallery.order')}}">Ordenar</a></li>
                        <li><a href="{{route('gallery.store')}}">Nuevo</a></li>
                       
                    </ul>
                </li> 
               
                <li>
                    <a class="has-arrow" href="#" aria-expanded="false">
                        <i class="mdi mdi-poll"></i>
                        <span class="hide-menu">Encuestas</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{route('encuestas.index')}}">Listado</a></li>
                        <li><a href="{{route('encuestas.create')}}">Nueva</a></li>
                    </ul>
                </li>
                <li>
                    <a class="has-arrow" href="#" aria-expanded="false">
                        <i class="mdi mdi-file-image"></i>
                        <span class="hide-menu">Anuncios</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{route('ads.index')}}">Listado</a></li>
                        <li><a href="{{route('ads.create')}}">Nuevo</a></li>
                    </ul>
                </li>
                <li>
                    <a class="" href="/">
                        <i class="mdi mdi-poll"></i>
                        <span class="hide-menu">Ir al portal</span>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
    <!-- Bottom points-->
    <div class="sidebar-footer">
        <!-- item-->
        <a href="" class="link" data-toggle="tooltip" title="Logout"><i class="mdi mdi-power"></i></a>
    </div>
    <!-- End Bottom points-->
</aside>
<script>

</script>
