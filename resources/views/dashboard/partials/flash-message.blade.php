@if ($message = Session::get('success'))
    <div id="flash-message" data-type="success" data-message="{{$message}}" class="alert alert-success" style="display: none">
        <div class="container-fluid">
            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
            {{$message}}
        </div>
    </div>
@endif

@if ($message = Session::get('error'))
    <div id="flash-message" data-type="error" data-message="{{$message}}" class="alert alert-danger" style="display: none">
        <div class="container-fluid">
           <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
            {{$message}}
        </div>
    </div>
@endif

@if ($message = Session::get('warning'))
    <div id="flash-message" data-type="warning" data-message="{{$message}}" class="alert alert-warning" style="display: none">
        <div class="container-fluid">
           <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
            {{$message}}
        </div>
    </div>
@endif

@if ($message = Session::get('info'))
    <div id="flash-message" data-type="info" data-message="{{$message}}" class="alert alert-info" style="display: none">
        <div class="container-fluid">
           <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
            {{$message}}
        </div>
    </div>
@endif
