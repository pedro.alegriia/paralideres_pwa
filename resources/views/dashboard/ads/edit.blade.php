@extends('layouts.dashboard')

@section('content')
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                {{--<h3 class="text-themecolor mb-0 mt-0">Recursos</h3>--}}
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Publicidad</a></li>
                    <li class="breadcrumb-item active">Edición</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Editar publicidad</h4>
                        {{--<h6 class="card-subtitle"> All with bootstrap element classies </h6>--}}
                        <form id="form-create-poll" class="mt-4" action="{{route('ads.update', $ad->id)}}" method="POST"
                              enctype="multipart/form-data">
                            {{csrf_field()}}
                            {{method_field('PUT')}}
                            <div class="form-group">
                                <label>Nombre</label>
                                <input type="text" class="form-control" name="name" aria-describedby="emailHelp"
                                       value="{{$ad->name}}"/>
                                @if($errors->has('name'))
                                    <span>{{$errors->first('name')}}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Descripción</label>
                                <textarea name="description" class="form-control" id="" cols="30" rows="4">
                                    {{$ad->description}}
                                </textarea>
                                @if($errors->has('description'))
                                    <span>{{$errors->first('description')}}</span>
                                @endif
                            </div>
                            <div class="row form-group">
                                <div class="form-group col-6">
                                    <label>Fecha de inicio:</label>
                                    <input type="date" name="starts_at" class="form-control" value="{{$ad->starts_at}}">
                                    @if($errors->has('starts_at'))
                                        <span>{{$errors->first('starts_at')}}</span>
                                    @endif
                                </div>
                                <div class="form-group col-6">
                                    <label>Fecha de terminación:</label>
                                    <input type="date" name="ends_at" class="form-control" value="{{$ad->ends_at}}">
                                    @if($errors->has('ends_at'))
                                        <span>{{$errors->first('ends_at')}}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Enlace</label>
                                <input type="text" name="link" class="form-control" value="{{$ad->link}}">
                                @if($errors->has('link'))
                                    <span>{{$errors->first('link')}}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Banner</label>

                                <div class="row">
                                    <div class="col-sm-6 offset-sm-3">
                                       {{-- <img src="{{$ad->banner}}" width="100%" class="mx-auto">--}}
                                    </div>
                                </div>

                                <input type="file" name="banner" class="form-control">
                                @if($errors->has('banner'))
                                    <span>{{$errors->first('banner')}}</span>
                                @endif
                            </div>
                            <button type="submit" class="btn btn-primary">Actualizar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Row -->

    </div>
    @include('dashboard.partials.footer')
@endsection
