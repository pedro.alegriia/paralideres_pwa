@extends('layouts.dashboard')

@section('content')
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                {{--<h3 class="text-themecolor mb-0 mt-0">Recursos</h3>--}}
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Publicidad</a></li>
                    <li class="breadcrumb-item active">Crear</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body">
                        @include('dashboard.partials.flash-message')
                        <h4 class="card-title">Crear publicidad</h4>
                        {{--<h6 class="card-subtitle"> All with bootstrap element classies </h6>--}}
                        <form id="form-create-poll" class="mt-4" action="{{route('ads.store')}}" method="POST"
                              enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label>Nombre</label>
                                <input type="text" class="form-control" name="name" aria-describedby="emailHelp"/>
                                @if($errors->has('name'))
                                    <span>{{$errors->first('name')}}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Descripción</label>
                                <textarea name="description" class="form-control" id="" cols="30" rows="4"></textarea>
                                @if($errors->has('description'))
                                    <span>{{$errors->first('description')}}</span>
                                @endif
                            </div>
                            <div class="row form-group">
                                <div class="form-group col-6">
                                    <label>Fecha de inicio:</label>
                                    <input type="date" name="starts_at" class="form-control">
                                    @if($errors->has('starts_at'))
                                        <span>{{$errors->first('starts_at')}}</span>
                                    @endif
                                </div>
                                <div class="form-group col-6">
                                    <label>Fecha de fin:</label>
                                    <input type="date" name="ends_at" class="form-control">
                                    @if($errors->has('ends_at'))
                                        <span>{{$errors->first('ends_at')}}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Enlace de redirección:</label>
                                <input type="text" name="link" class="form-control">
                                @if($errors->has('link'))
                                    <span>{{$errors->first('link')}}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Banner:</label>
                                <input type="file" name="banner" class="form-control">
                                @if($errors->has('banner'))
                                    <span>{{$errors->first('banner')}}</span>
                                @endif
                            </div>
                            <button type="submit" class="btn btn-primary">Publicar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('dashboard.partials.footer')
@endsection

@section('script')
    <script>
        const app = new Vue({
            el: '#form-create-poll',
            data() {
                return {
                    responses: [],
                    date_from: null,
                    date_to: null,
                }
            },
            mounted() {
                let today = new Date().toISOString().slice(0, 10);
                this.date_from = today;
                this.date_to = today;
            },
            methods: {
                addResponse() {
                    this.responses.push("");
                },
                deleteResponse(key) {
                    this.responses.splice(key, 1);
                },
            },

        });
    </script>
@endsection
