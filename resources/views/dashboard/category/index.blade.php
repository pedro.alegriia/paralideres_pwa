@extends('layouts.dashboard')

@section('content')
    <div id="collections-list" class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Categorías</a></li>
                    <li class="breadcrumb-item active">Listado</li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body">
                        @include('dashboard.partials.flash-message')
                        <h4 class="card-title">Listado de categorías</h4>
                        <table id="example" class="table table-striped table-bordered" style="width: 100%;">
                            <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Slug</th>
                                <th>Status</th>
                                {{-- <th>Diseños</th> --}}
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody id="agencies">
                                          @foreach($categories as $category)
                                          <tr>
                                               <td>{{$category->label}}</td>
                                               <td>{{$category->slug}}</td>
                                               
                                               <td>{{$category->status}}</td>
                                               
                                               <td style="text-align: center;">
                                                <div class="btn-group-vertical" role="group" aria-label="Second group">
                                                  <i style="cursor:pointer; font-size:30px; color:#202766" class="fa fa-ellipsis-v" id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" aria-hidden="true"></i>
                                                
                                                  <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                                    <a class="dropdown-item" href="{{ route('categories.show', $category->id)}}" >Ver</a>
                                                    <a class="dropdown-item" href="{{ route('categories.edit', $category->id)}}" >Editar</a>
                                                    <a class="dropdown-item" href="#">Eliminar</a>
                                                  </div>
                                                </div>
                                               
                                             
                                               </td>
                                           </tr>
                                          @endforeach
                                        </tbody>
                        </table>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('dashboard.partials.footer')
@endsection
@section('script')
    <script>
 $("#exampleModal").modal("show"); $("#exampleModal").css("z-index", "1042");
</script>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" defer/>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js" defer ></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js" defer></script>
<script type="text/javascript">
  $(document).ready( function () {
    $('#example').DataTable();
} );
</script>
@endsection
