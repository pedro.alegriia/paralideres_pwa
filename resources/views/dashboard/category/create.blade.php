@extends('layouts.dashboard')

@section('content')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.2/basic.css" integrity="sha512-Ucip2staDcls3OuwEeh5s9rRVYBsCA4HRr18+qd0Iz3nYpnfUeCIMh/82aHKeYgdaXGebmi9vcREw7YePXsutQ==" crossorigin="anonymous" />
    <div class="container-fluid">

        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Galería</a></li>
                    <li class="breadcrumb-item active">Crear</li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body">
                        @include('dashboard.partials.flash-message')
                        <h4 class="card-title">Crear imagén</h4>
                        
                        <form id="form-create-poll" class="mt-4" action="{{route('gallery.store')}}" enctype="multipart/form-data" method="POST">
                            {{csrf_field()}}
                            
                            <div class="form-group">
                                <label>Tipo</label>
                                <select name="type" class="form-control">
                                    <option value="visitant">Visitante</option>
                                    <option value="user">Usuario</option>
                                </select>
                                @if($errors->has('type'))
                                    <span>{{$errors->first('type')}}</span>
                                @endif
                            </div>


                            <div class="form-group">
                                <label>Imagen:</label>
                                <input type="file" name="image" class="form-control">
                                <p class="help-block">La imagén debe tener igual o mayor a 1368 x 516. </br> 
                                    No puede pesar más de 3 mb el archivo.
                                </p>
                                @if($errors->has('image'))
                                    <span>{{$errors->first('image')}}</span>
                                @endif
                            </div>
                            <!--leyenda con specificaciones-->

                            <div class="form-group">
                                <label>Estatus</label>
                                <select name="status" class="form-control">
                                    <option value="1">Activo</option>
                                    <option value="0">Inactivo</option>
                                </select>
                                @if($errors->has('status'))
                                    <span>{{$errors->first('status')}}</span>
                                @endif
                            </div>

                            <button type="submit" class="btn btn-primary">Guardar</button>
                            
                        </form>

                        <form action="{{ route("category.store") }}" class="mt-4"  method="POST" enctype="multipart/form-data">
                            {{csrf_field()}}
                            
                            <div class="form-group">
                                <label>Label:</label>
                                <input type="file" name="image" class="form-control" readonly>
                                <p class="help-block">La imagén debe tener igual o mayor a 1368 x 516. </br> 
                                    No puede pesar más de 3 mb el archivo.
                                </p>
                                @if($errors->has('image'))
                                    <span>{{$errors->first('image')}}</span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label>Slug:</label>
                                <input type="text" name="slug" class="form-control" readonly>
                                @if($errors->has('slug'))
                                    <span>{{$errors->first('slug')}}</span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="document">Diseños</label>
                                <div class="needsclick dropzone" id="document-dropzone">
                                </div>
                            </div>

                            <div class="row">
                                <input class="btn btn-primary" type="submit">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('dashboard.partials.footer')
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.2/min/dropzone.min.js" integrity="sha512-9WciDs0XP20sojTJ9E7mChDXy6pcO0qHpwbEJID1YVavz2H6QBz5eLoDD8lseZOb2yGT8xDNIV7HIe1ZbuiDWg==" crossorigin="anonymous"></script>
    <script>
     <script>
        Dropzone.options.myDropzone = {
            autoProcessQueue: false,
            uploadMultiple: true,
            maxFilezise: 10,
            maxFiles: 2,
            
            init: function() {
                var submitBtn = document.querySelector("#submit");
                myDropzone = this;
                
                submitBtn.addEventListener("click", function(e){
                    e.preventDefault();
                    e.stopPropagation();
                    myDropzone.processQueue();
                });
                this.on("addedfile", function(file) {
                    alert("file uploaded");
                });
                
                this.on("complete", function(file) {
                    myDropzone.removeFile(file);
                });

                this.on("success", 
                    myDropzone.processQueue.bind(myDropzone)
                );
            }
        };
    </script>
@endsection
