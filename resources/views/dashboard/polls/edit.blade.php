@extends('layouts.dashboard')

@section('content')
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                {{--<h3 class="text-themecolor mb-0 mt-0">Recursos</h3>--}}
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Encuestas</a></li>
                    <li class="breadcrumb-item active">Crear</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body">
                        @include('dashboard.partials.flash-message')
                        <h4 class="card-title">Editar encuesta</h4>
                        {{--<h6 class="card-subtitle"> All with bootstrap element classies </h6>--}}
                        <form id="form-create-poll"  action="{{route('encuestas.update', $poll->id)}}" enctype="multipart/form-data" method="POST">
                            {{csrf_field()}}
                            {{method_field('PUT')}}
                            <input type="hidden" name="poll" value="{{json_encode($poll)}}">
                            <div class="form-group">
                                <label>Pregunta</label>
                                <input type="text" class="form-control" name="question" value="{{$poll->question}}" aria-describedby="emailHelp"
                                       v-model="poll.question"/>
                            </div>
                            <div class="row form-group">
                                <div class="form-group col-6">
                                    <label>Fecha de inicio:</label>
                                    <input type="hidden">
                                    <input type="date" name="date_from" class="form-control" value="{{$poll->date_from}}" v-model="poll.date_from">
                                </div>
                                <div class="form-group col-6">
                                    <label>Fecha de fin:</label>
                                    <input type="date" name="date_to" class="form-control" value="{{$poll->date_to}}" v-model="poll.date_to">
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Respuestas</label>
                                
                                @foreach ($poll->options as $option)
                                    <div  class="d-flex">
                                    <input type="text" class="form-control my-1 mr-1" value="{{$option['option']}}"  name="option" style="display: inline-block"
                                           v-model="poll.options[key]['option']" :disabled="poll.options[key]['disabled']"/>
                                    <button class="btn btn-danger btn-sm my-1 ml-1"
                                            @click.prevent="deleteResponse(key)">Eliminar
                                    </button>
                                </div>
                                @endforeach
                                
                                
                                <button class="btn btn-success btn-sm my-3 d-block" @click.prevent="addResponse">Agregar
                                    respuesta
                                </button>
                            </div>
                            <button type="submit" class="btn btn-primary" @click.prevent="updatePoll">Actualizar
                                encuesta
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('dashboard.partials.footer')
@endsection

@section('script')
       <script>
 $("#exampleModal").modal("show"); $("#exampleModal").css("z-index", "1042");
</script>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" defer/>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js" defer ></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js" defer></script>
<script type="text/javascript">
  $(document).ready( function () {
    $('#example').DataTable();
} );
</script>
@endsection
