@extends('layouts.dashboard')

@section('content')
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                {{--<h3 class="text-themecolor mb-0 mt-0">Recursos</h3>--}}
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Encuestas</a></li>
                    <li class="breadcrumb-item active">Crear</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body">
                        @include('dashboard.partials.flash-message')
                        <h4 class="card-title">Crear encuesta</h4>
                        {{--<h6 class="card-subtitle"> All with bootstrap element classies </h6>--}}
                        <form id="form-create-poll" class="mt-4" action="{{route('encuestas.store')}}" method="POST">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label>Pregunta</label>
                                <input type="text" class="form-control" name="question" aria-describedby="emailHelp"/>
                            </div>
                            <div class="row form-group">
                                <div class="form-group col-6">
                                    <label>Fecha de inicio:</label>
                                    <input type="date" name="date_from" class="form-control" v-model="date_from">
                                </div>
                                <div class="form-group col-6">
                                    <label>Fecha de fin:</label>
                                    <input type="date" name="date_to" class="form-control" v-model="date_to">
                                </div>
                            </div>
                            <input type="hidden" name="former_id" class="form-control" value="0">
                            <div class="form-group">
                                <label>Respuestas</label>
                                <div v-for="(response, key) in responses" class="d-flex">
                                    <input type="text" class="form-control my-1 mr-1" name="responses[]"
                                           style="display: inline-block" v-model="responses[key]"/>
                                    <button class="btn btn-danger btn-sm my-1 ml-1"
                                            @click.prevent="deleteResponse(key)">Eliminar
                                    </button>
                                </div>
                                <button class="btn btn-success btn-sm my-3 d-block" @click.prevent="addResponse">Agregar
                                    respuesta
                                </button>
                            </div>
                            <button type="submit" class="btn btn-primary">Crear encuesta</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('dashboard.partials.footer')
@endsection

@section('script')
    <script>
        const app = new Vue({
            el: '#form-create-poll',
            data() {
                return {
                    responses: [],
                    date_from: null,
                    date_to: null,
                }
            },
            mounted() {
                let today = new Date().toISOString().slice(0, 10);
                this.date_from = today;
                this.date_to = today;
            },
            methods: {
                addResponse() {
                    this.responses.push("");
                },
                deleteResponse(key) {
                    this.responses.splice(key, 1);
                },
            },

        });
    </script>
@endsection
