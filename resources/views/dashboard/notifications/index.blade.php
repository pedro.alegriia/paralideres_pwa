@extends('layouts.dashboard')

@section('content')
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                {{--<h3 class="text-themecolor mb-0 mt-0">Recursos</h3>--}}
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Ads</a></li>
                    <li class="breadcrumb-item active">Listado</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body">
                        @include('dashboard.partials.flash-message')
                        <h4 class="card-title">Notificaciones</h4>
                        <table class="table table-striped table-hover table-responsive-md">
                            <thead>
                            <tr>
                                <th>Notificación</th>
                                <th>Fecha</th>
                                <th>Status</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($notifications as $notification)
                                <tr>
                                    <td>
                                        <a href="{{$notification->data['link']}}">{{$notification->data['title']}}</a>
                                        <br>
                                        <span>{{$notification->data['message']}}</span>
                                    </td>
                                    <td>{{$notification->created_at->format('d/M/ H:i')}}</td>
                                    <td>{{$notification->read_at ? 'Leída' : 'No leída'}}</td>
                                    <td>
                                        @if(!$notification->read_at)
                                            <a class="btn btn-success btn-sm"
                                               href="{{route('notification.read', $notification->id)}}">
                                                Marcar leída <i class="mdi mdi-check-circle"></i>
                                            </a>
                                        @endif
                                        {{--<form action="{{route('ads.destroy', $notification->id)}}" method="POST"
                                              style="display: inline-block">
                                            {{csrf_field()}}
                                            {{method_field('delete')}}
                                            <button class="btn btn-danger btn-sm"><i class="mdi mdi-delete"></i>
                                            </button>
                                        </form>--}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{$notifications->links()}}
                        <div class="clearfix"></div>
                        <hr>
                        <a class="btn btn-success btn-sm" href="{{route('notification.all.read')}}">
                            Marcar todas como leídas <i class="mdi mdi-check-circle"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Row -->

    </div>
    @include('dashboard.partials.footer')
@endsection
