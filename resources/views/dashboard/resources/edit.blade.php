@extends('layouts.dashboard')

@section('content')
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                {{--<h3 class="text-themecolor mb-0 mt-0">Recursos</h3>--}}
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Recursos</a></li>
                    <li class="breadcrumb-item active">Edición</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Editar recurso</h4>
                        {{--<h6 class="card-subtitle"> All with bootstrap element classies </h6>--}}
                        <form class="mt-4">
                            <div class="form-group">
                                <label>Titulo</label>
                                <input type="text" class="form-control" aria-describedby="emailHelp"
                                       value="{{$resource->title}}"/>
                            </div>
                            <div class="form-group">
                                <label>Categoría</label>
                                <select name="category_id" class="form-control">
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{$category->label}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Resumen:</label>
                                <textarea name="review" rows="3" class="form-control">{!! $resource->review !!}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Contenido:</label>
                                <textarea name="content" rows="10" class="form-control">{!! $resource->content !!}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Documento</label>
                                <br>
                                <input type="file" name="recurso_file" class="form-group">
                            </div>
                            <button type="submit" class="btn btn-primary">Actualizar recurso</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Row -->

    </div>
    @include('dashboard.partials.footer')
@endsection