@extends('layouts.dashboard')

@section('content')
    <div id="resources-list" class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                {{--<h3 class="text-themecolor mb-0 mt-0">Recursos</h3>--}}
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Recursos</a></li>
                    <li class="breadcrumb-item active">Listado</li>
                </ol>
            </div>
        </div>
      
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body">
                        @include('dashboard.partials.flash-message')
                        <h4 class="card-title">Listado de recursos</h4>
                        <label for="withTrashed">
                            <input id="withTrashed" type="checkbox">
                            Listar recursos eliminados
                        </label>
                        <table id="example" class="table table-striped table-bordered" style="width: 100%;">
                            <thead>
                            <tr>
                                <th>Titulo</th>
                                <th>Fecha</th>
                                <th>Autor</th>
                                <th>Categorías</th>
                                <th>Status</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            
                            @foreach($resources as $resource)
                                <tr>
                                    <td>{{$resource->title}}</td>
                                    <td>{{$resource->created_at->format('d/m/Y')}}</td>
                                    <td>
                                       {{$resource->user->username}} 
                                    </td>
                                    <td>
                       

                                    @if($resource->categories()->pluck('label')->implode(', '))
                                        {{$resource->categories()->pluck('label')->implode(', ')}}

                                    @endif
                                   </td>
                                    <td>
                                        @if($resource->deleted_at)
                                            <span class='badge badge-danger'>
                                                <i class="mdi mdi-delete"></i> Eliminado
                                            </span>
                                        @else
                                            <span class='badge badge-{{$resource->published ? 'success' : 'danger'}}'>
                                                <i class="mdi mdi-{{$resource->published ? 'check-circle' : 'close-octagon'}}"></i>
                                                {{$resource->published ? 'Publicado' : 'No publicado'}}
                                            </span>
                                        @endif
                                    </td>
                                    <td>
                                        <a class="btn btn-info btn-sm"
                                           href="/app#/recursos/{{$resource->id}}">
                                            <i class="mdi mdi-eye"></i> Ver
                                        </a>
                                        <a class="btn btn-warning btn-sm" href="/app#/publicar-recurso/{{$resource->id}}">
                                            <i class="mdi mdi-pencil"></i> Editar
                                        </a>
                                    
                                        <a class="btn btn-primary btn-sm"
                                           href="{{route('recursos.published', $resource->id)}}">
                                            <i class="mdi mdi-lock{{$resource->published ? '' : '-open'}}"></i>
                                            {{$resource->published ? 'Ocultar' : 'Publicar'}}
                                        </a>
                                        @if(!$resource->deleted_at)
                                            <form action="{{route('recursos.destroy', $resource->id)}}" method="POST"
                                                  style="display: inline-block">
                                                {{csrf_field()}}
                                                {{method_field('delete')}}
                                                <button class="btn btn-danger btn-sm"  onclick="return confirm('¿Quieres eliminar este recurso?');">
                                                    <i class="mdi mdi-delete"></i>
                                                    Eliminar
                                                </button>
                                            </form>
                                        @else
                                            <form action="{{route('recursos.recover', $resource->id)}}" method="POST"
                                                  style="display: inline-block">
                                                {{csrf_field()}}
                                                <button class="btn btn-success btn-sm"><i class="mdi mdi-check"></i>
                                                    Recuperar
                                                </button>
                                            </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                       {{$resources->links()}}
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    @include('dashboard.partials.footer')
@endsection
@section('script')

<script>
 $("#exampleModal").modal("show"); $("#exampleModal").css("z-index", "1042");
</script>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" defer/>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js" defer ></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js" defer></script>
<script type="text/javascript">
  $(document).ready( function () {
    $('#example').DataTable();
} );
</script>

@endsection
