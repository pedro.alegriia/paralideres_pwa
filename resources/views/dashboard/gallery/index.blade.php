@extends('layouts.dashboard')

@section('content')
    <div id="collections-list" class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Galéria</a></li>
                    <li class="breadcrumb-item active">Listado</li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body">
                        @include('dashboard.partials.flash-message')
                        <h4 class="card-title">Listado de imágenes</h4>
                        <table id="example" class="table table-striped table-bordered" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th>Orden</th>
                                    <th>Imagen</th>
                                    <th>Tipo</th>
                                    <th>Activo</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($gallery as $gallery)
                                    <tr>
                                        <td>{{$gallery->order}}</td>
                                        <td>
                                            <img src="{{asset('storage/images').'/' .$gallery->image}}" alt="" style="width: 60px;">
                                        </td>
                                        <td>{{$gallery->type}}</td>
                                        <td>
                                              @if ($gallery->status != '1')
                                                       <label <?php 
                                                if($gallery->status == '0'){echo('Inactivo');}
                                                 
                                                ?>>{{$gallery->status}}</label>
                                               @else
                                                Activo
                                              @endif
                                        </td>
                                        <td>
                                            <a class="btn btn-primary btn-sm" href="{{route('usuarios.edit', $gallery->id)}}">
                                                <i class="mdi mdi-table-edit"></i>
                                            </a>
                                            <form action="{{route('usuarios.destroy', $gallery->id)}}" method="POST"
                                                  style="display: inline-block">
                                                {{csrf_field()}}
                                                {{method_field('delete')}}
                                                <button class="btn btn-danger btn-sm"><i class="mdi mdi-delete"></i>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    @include('dashboard.partials.footer')
@endsection

@section('script')
   <script>
 $("#exampleModal").modal("show"); $("#exampleModal").css("z-index", "1042");
</script>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" defer/>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js" defer ></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js" defer></script>
<script type="text/javascript">
  $(document).ready( function () {
    $('#example').DataTable();
} );
</script>
@endsection
