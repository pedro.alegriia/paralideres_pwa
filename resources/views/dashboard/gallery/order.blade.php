@extends('layouts.dashboard')

@section('content')
<style>
  .SG{
  margin: 0;
  padding: 0;
  text-align: center;
}
.SG .sgLi{
  min-width: 24%;
  margin: 2% .35%;
  display: inline-flex;
  box-shadow: 0 2px 4px rgba(0,0,0, .2);
}

.box{
    display: block;
    padding: .5rem;
}



</style>
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Galería</a></li>
                    <li class="breadcrumb-item active">Ordenar</li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body">
                        @include('dashboard.partials.flash-message')
                        <h4 class="card-title">Ordenar imagenes de usuario</h4>
                        <button type="submit" id="submit" class="btn btn-primary float-right">Guardar</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Imagenes de usuario</h4>

                     <ul class="SG" id="image-list">
                     {{--  @foreach($query_user as $gallery)
                        <li class="sgLi" id="{{$gallery->id}}">
                            <div class="box">
                                <img src="{{$gallery->image}}"  style="width:180px; height:180px;">
                                <p>{{$gallery->id}}</p>
                            </div>
                        </li>
                        @endforeach
                        --}}
                    </ul>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Imagenes de Visitante</h4>

                    <ul class="SG" id="image-list-visitant">
                        {{--
                        @foreach($query_visitant as $gallery)
                        <li class="sgLi" id="{{$gallery->id}}">
                            <div class="box">
                            <img src="{{$gallery->image}}"  style="width:180px; height:180px;">
                            <p>{{$gallery->id}}</p>
                            </div>
                        </li>
                        @endforeach
                         --}}
                    </ul>

                    </div>
                </div>
            </div>
        </div>


    </div>
    @include('dashboard.partials.footer')
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"  crossorigin="anonymous"></script>
    <script>
    $(document).ready(function () {
        var dropIndex;
        const user_list = $("#image-list");
        const visitant_list = $("#image-list-visitant");
        const $submit =$('#submit');

        user_list.sortable({
            update: function(event, ui) {
            dropIndex = ui.item.index();
            }
        });

        visitant_list.sortable({
            update: function(event, ui) {
            dropIndex = ui.item.index();
            }
        });

        $submit.click(function (e) {
            console.log(dropIndex);
            var imageIdsArray = [];
            var vimageIdsArrays = [];
            $('#image-list li').each(function (index) {
                    var id = $(this).attr('id');
                    imageIdsArray.push(id);
            });

            $('#image-list-visitant li').each(function (index) {
                if(index <= dropIndex) {
                    var id = $(this).attr('id');
                    vimageIdsArrays.push(id);
                }
            });
            console.log('parametros', imageIdsArray);

            $.ajax({
                url: '/api/v1/gallery/update',
                type: 'post',
                data: {
                    gallery_user: imageIdsArray,
                    gallery_visitant:vimageIdsArrays
                },
                success: function (response) {
                    if(response){
                        window.location.href = `/admin/gallery`;
                    }
                }
            });
            e.preventDefault();
        });
    });
    </script>
@endsection
