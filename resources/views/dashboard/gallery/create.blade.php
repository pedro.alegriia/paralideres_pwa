@extends('layouts.dashboard')

@section('content')
    <div class="container-fluid">

        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Galería</a></li>
                    <li class="breadcrumb-item active">Crear</li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body">
                        @include('dashboard.partials.flash-message')
                        <h4 class="card-title">Crear imagén</h4>

                        <form id="form-create-poll" class="mt-4" action="{{route('gallery.store')}}" enctype="multipart/form-data" method="POST">
                            {{csrf_field()}}

                            <div class="form-group">
                                <label>Tipo</label>
                                <select name="type" class="form-control">
                                    <option value="visitant">Visitante</option>
                                    <option value="user">Usuario</option>
                                </select>
                                @if($errors->has('type'))
                                    <span>{{$errors->first('type')}}</span>
                                @endif
                            </div>


                            <div class="form-group">
                                <label>Imagen:</label>
                                <input type="file" name="image" class="form-control">
                                <p class="help-block">La imagén debe tener igual o mayor a 1368 x 516. </br>
                                    No puede pesar más de 3 mb el archivo.
                                </p>
                                @if($errors->has('image'))
                                    <span>{{$errors->first('image')}}</span>
                                @endif
                            </div>
                            <!--leyenda con specificaciones-->

                            <div class="form-group">
                                <label>Estatus</label>
                                <select name="status" class="form-control">
                                    <option value="1">Activo</option>
                                    <option value="0">Inactivo</option>
                                </select>
                                @if($errors->has('status'))
                                    <span>{{$errors->first('status')}}</span>
                                @endif
                            </div>
                            <input type="hidden" name="order" value="1" id="">

                            <button type="submit" class="btn btn-primary">Guardar</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('dashboard.partials.footer')
@endsection

@section('script')
    <script>
       /*  const app = new Vue({
            el: '#form-create-poll',
            data() {
                return {
                    responses: [],
                    date_from: null,
                    date_to: null,
                }
            },
            mounted() {
                let today = new Date().toISOString().slice(0, 10);
                this.date_from = today;
                this.date_to = today;
                $(".select2").select2();
            },
            methods: {
                addResponse() {
                    this.responses.push("");
                },
                deleteResponse(key) {
                    this.responses.splice(key, 1);
                },
            },

        }); */
    </script>
@endsection
