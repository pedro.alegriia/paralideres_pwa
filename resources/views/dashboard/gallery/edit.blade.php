@extends('layouts.dashboard')

@section('content')
    <div class="container-fluid">

        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Galería</a></li>
                    <li class="breadcrumb-item active">Edición</li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body">
                        @include('dashboard.partials.flash-message')
                        <h4 class="card-title">Editar imagén</h4>
                        
                        <form id="form-create-poll" class="mt-4" action="{{route('gallery.update', $image->id)}}" enctype="multipart/form-data" method="POST">
                            {{csrf_field()}}
                            {{method_field('PUT')}}
                            
                            <div class="form-group">
                                <label>Tipo</label>
                                <select name="type" class="form-control">
                                    <option value="visitant" {{ $image->type == 'visitant' ? 'selected' : ''}}>Visitante</option>
                                    <option value="user" {{ $image->type == 'user' ? 'selected' : ''}}>Usuario</option>
                                </select>
                                @if($errors->has('type'))
                                    <span>{{$errors->first('type')}}</span>
                                @endif
                            </div>

                            <div class="row">
                                <div class="col-sm-6 offset-sm-3">
                                    <img src="{{$image->image}}" width="100%" class="mx-auto">
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Imagen:</label>
                                <input type="file" name="image" class="form-control">
                                <p class="help-block">La imagén debe tener igual o mayor a 1368 x 516. </br> 
                                    No puede pesar más de 3 mb el archivo.
                                </p>
                                @if($errors->has('image'))
                                    <span>{{$errors->first('image')}}</span>
                                @endif
                            </div>
                            <!--leyenda con specificaciones-->

                            <div class="form-group">
                                <label>Estatus</label>
                                <select name="status" class="form-control">
                                    <option value="1" {{ $image->status ? 'selected' : ''}}>Activo</option>
                                    <option value="0" {{ !$image->status ? 'selected' : ''}}>Inactivo</option>
                                </select>
                                @if($errors->has('status'))
                                    <span>{{$errors->first('status')}}</span>
                                @endif
                            </div>

                            <button type="submit" class="btn btn-primary">Actualizar</button>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('dashboard.partials.footer')
@endsection

