@extends('layouts.dashboard')

@section('content')
{{--dd($permissions)--}}
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                {{--<h3 class="text-themecolor mb-0 mt-0">Recursos</h3>--}}
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Usuarios</a></li>
                    <li class="breadcrumb-item active">Edición</li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col">
                @include('dashboard.partials.flash-message')
            </div>
        </div>

        @if($user->tempAvatar)
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Solicitud Cambio de Avatar</h4>
                            <div class="row">
                                <div class="col-sm-4 offset-sm-4 text-center">
                                    <img class="border" src="{{$user->tempAvatar->getFirstMediaUrl('temp_avatars')}}"
                                         width="100%">
                                </div>
                                <div class="col-sm-4 offset-sm-4 text-center mt-3">
                                    <form action="{{url('admin/usuarios',$user->id)}}" method="POST">
                                        {{csrf_field()}}
                                        <input type="hidden" name="user_id" value="{{$user->id}}">
                                        <div class="form-group">
                                            <label for="action-selector">Acción:</label>
                                            <select name="status" id="action-selector" class="form-control">
                                                <option value="">Selecciona una opción</option>
                                                <option value="accept">Aceptar</option>
                                                <option value="reject">Rechazar</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="comment-input">Comentario</label>
                                            <textarea name="comment" id="comment-input" rows="2" class="form-control"
                                                      placeholder="Ej. Rechazada por inapropiada"></textarea>
                                        </div>
                                        <div class="form-group text-center">
                                            <button class="btn btn-success">Guardar cambios</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Editar usuario</h4>
                        {{--<h6 class="card-subtitle"> All with bootstrap element classies </h6>--}}
                        <form class="mt-4" action="{{route('usuarios.update', $user->id)}}" method="POST">
                            {{csrf_field()}}
                            {{method_field('PUT')}}
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" class="form-control" aria-describedby="emailHelp"
                                       value="{{$user->email}}" disabled/>
                                <hr>
                            </div>



<div class="">
  <div class="row">
    <div class="col-sm">
    <h4>Rol</h4>
                         
                                  
                                        <label>
                                        
                                        @if (isset($rolesByUser->role_id) == 1)
                                            ADMIN

                                        @elseif (isset($rolesByUser->role_id) == 2)
                                                EDITOR
                                                
                                        @elseif (isset($rolesByUser->role_id) ==3)
                                                COLABORADOR
                                                
                                        @elseif (isset($rolesByUser->role_id) ==4)
                                                MODERADOR
                                                
                                        @elseif (isset($rolesByUser->role_id) ==5)
                                                TEAM LEAD
                                                
                                        @elseif (isset($rolesByUser->role_id) == 6)
                                                TEAM MEMBER
                                                
                                        @elseif (isset($rolesByUser->role_id) ==7)
                                                CUSTOMER

                                        @else
                                         Sin rol
                        
                                        @endif
                                       
                                        
                                         
                                             
                                        
                                        </label>
        @foreach ( $roles as $role )
                                    <div class="checbox">
                                        <label>
                                            <input name="roles" type="checkbox" class="only-one" value="{{ $role->id }}"
                                             {{$user->roles->contains($role->id) ? 'checked':'' }}
                                                {{--$role->id = $user->id ? 'checked':'' --}}>
                                            {{ $role->name }} <br>
                                        
                                        </label>
                                    
                                    </div>
                                @endforeach
                                 <input type="hidden" name="model_id" value="{{$user->id}}">

    </div>
    <div class="col-sm">
      Permisos
        @foreach($permissionByUser as $permissionByUser)          
            <label>
             @if ($permissionByUser->permission_id == 1)
                                            Crear Recurso,

                                        @elseif (isset($permissionByUser->permission_id) == 2)
                                                Editar recurso,
                                                
                                        @elseif (isset($permissionByUser->permission_id) == 3)
                                                Editar recurso propio,
                                         @elseif (isset($permissionByUser->permission_id) == 4)
                                                Prohibir recurso,
                                                @elseif (isset($permissionByUser->permission_id) == 5)
                                               Eliminar recurso,

                                                @elseif (isset($permissionByUser->permission_id) == 6)
                                                Eliminar recurso propio,

                                                @elseif (isset($permissionByUser->permission_id) == 7)
                                                Crear encuesta,

                                                @elseif (isset($permissionByUser->permission_id) == 8)
                                                Editar encuesta,

                                                @elseif (isset($permissionByUser->permission_id) == 9)
                                                Eliminar encuesta,

                                                @elseif (isset($permissionByUser->permission_id) == 10)
                                                Votar encuesta,

                                                @elseif (isset($permissionByUser->permission_id) == 11)
                                                Crear categoria,

                                                @elseif (isset($permissionByUser->permission_id) == 12)
                                                Editar categoria,

                                                @elseif (isset($permissionByUser->permission_id) == 13)
                                                Eliminar categoria,

                                                @elseif (isset($permissionByUser->permission_id) == 14)
                                                Crear etiqueta,

                                                @elseif (isset($permissionByUser->permission_id) == 15)
                                                Editar etiqueta,
                                                @elseif (isset($permissionByUser->permission_id) == 16)
                                                Eliminar etiqueta,
                                                @elseif (isset($permissionByUser->permission_id) == 17)
                                                Crear role,
                                                @elseif (isset($permissionByUser->permission_id) == 18)
                                                Editar role,
                                                @elseif (isset($permissionByUser->permission_id) == 19)
                                                Eliminar Role,
                                                @elseif (isset($permissionByUser->permission_id) == 20)
                                                Crear permiso,
                                                @elseif (isset($permissionByUser->permission_id) == 21)
                                                Editar permiso,
                                                @elseif (isset($permissionByUser->permission_id) == 22)
                                                Eliminar permiso
                                               
                                                @else
                                         Sin permisos
                        
                                        @endif
                                       
                 
                                            
            </label>
    @endforeach
      
    </div>
    
  </div>
</div>


                          
                              
                                    
                           
                             

                        
                                



                                   

                              

                                <hr>
                          
                            <div class="form-group">
                                <div class="row">
                                    @foreach($permission_groups as $category => $group)
                                        <div class="col-md-4 mt-2">
                                            <h4>{{ucwords($category)}}</h4>
                                            @foreach($group as $permission)
                                                <label for="id-{{$permission->name}}" class="d-block">
                                                    <input id="id-{{$permission->name}}" type="checkbox" value="{{$permission->id}}"
                                                           name="permissions['{{$permission->name}}'][]"
                                                            {{--{{$user->hasPermissionTo($permission->name) ? 'checked' : ''}}--}}
                                                    >
                                                    {{str_replace("_", " ", ucwords($permission->name))}}
                                                </label>
                                            @endforeach
                                        </div>
                                    @endforeach
                                </div>
                                <hr>
                            </div>
                            <div class="form-group">
                                <label>Estatus</label>
                                <select name="status" class="form-control">
                                    <option value="1" {{$user->is_active ? 'selected' : ''}}>Activo</option>
                                    <option value="0" {{!$user->is_active ? 'selected' : ''}}>Inactivo</option>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-primary">Actualizar usuario</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Row -->

    </div>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

    <script type="text/javascript">
        $('input:checkbox[name=roles]').on('change', function (e) {
           if ($('input:checkbox[name=roles]:checked').length > 1) {
               $(this).prop('checked', false);
               alert("Solo puedes seleccionar un rol");
           }
       });
       </script>
<script type="text/javascript">
       

       function selectOnlyThis(id){
  var myCheckbox = document.getElementsByName("roles");
  Array.prototype.forEach.call(myCheckbox,function(el){
    el.checked = false;
  });
  id.checked = true;
} 
</script>
    @include('dashboard.partials.footer')



@endsection
