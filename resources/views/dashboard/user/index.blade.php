@extends('layouts.dashboard')

@section('content')

    <div id="users-list" class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                {{--<h3 class="text-themecolor mb-0 mt-0">Recursos</h3>--}}
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Usuarios</a></li>
                    <li class="breadcrumb-item active">Listado</li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body">
                        @include('dashboard.partials.flash-message')
                        <h4 class="card-title">Listado de usuarios</h4>
                        <table id="example" class="table table-striped table-bordered" style="width: 100%;">
                            <thead>
                            <tr>
                                <th>Email</th>
                                <th>Usuario</th>
                                <th>Role</th>
                                <th>Status</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                               <tbody>

                            @foreach($users as  $index => $user)
                                <tr>
                                    <td>{{$user->email}}</td>
                                    <td>{{$user->username}}</td>

                                        <td>
                                        {{$user->role}}
                                      

                                           




                                        </td>


                                    <td>
                                        <span class='badge badge-{{$user->is_active ? 'success' : 'danger'}}'>{{$user->is_active ? 'Activo' : 'Inactivo'}}</span>
                                    </td>
                                    <td>
                                        <a class="btn btn-primary btn-sm" href="{{route('usuarios.edit', $user->id)}}">
                                            <i class="mdi mdi-table-edit"></i>
                                        </a>
                                        <form action="{{route('usuarios.destroy', $user->id)}}" method="POST"
                                              style="display: inline-block">
                                            {{csrf_field()}}
                                            {{method_field('delete')}}
                                            <button class="btn btn-danger btn-sm"><i class="mdi mdi-delete"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('dashboard.partials.footer')
@endsection
@section('script')
    <script>
 $("#exampleModal").modal("show"); $("#exampleModal").css("z-index", "1042");
</script>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" defer/>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js" defer ></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js" defer></script>
<script type="text/javascript">
  $(document).ready( function () {
    $('#example').DataTable();
} );
</script>
@endsection
