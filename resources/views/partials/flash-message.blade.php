@if ($message = Session::get('success'))
    <div class="alert alert-success">
        <div class="container-fluid">
            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
            {{$message}}
        </div>
    </div>
@endif

@if ($message = Session::get('error'))
    <div class="alert alert-danger">
        <div class="container-fluid">
           <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
            {{$message}}
        </div>
    </div>
@endif

@if ($message = Session::get('warning'))
    <div class="alert alert-warning">
        <div class="container-fluid">
           <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
            {{$message}}
        </div>
    </div>
@endif

@if ($message = Session::get('info'))
    <div class="alert alert-info">
        <div class="container-fluid">
           <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
            {{$message}}
        </div>
    </div>
@endif
