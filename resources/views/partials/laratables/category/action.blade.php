<div class="d-flex flex-column">
    <div class="d-flex flex-row">
        <a class="btn btn-info btn-sm mx-1" target="_blank"
           href="/app/collections/{{$collection->slug}}">
            <i class="mdi mdi-eye"></i> Ver
        </a>
        <a class="btn btn-warning btn-sm mx-1"
           href="/admin/collections/{{$collection->slug}}/edit">
            <i class="mdi mdi-pencil"></i> Editar
        </a>
    </div>
</div>
