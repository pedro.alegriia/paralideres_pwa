<span class='badge badge-{{$gallery->status ? 'success' : 'danger'}}'>
    <i class="mdi mdi-{{$gallery->status ? 'check-circle' : 'close-octagon'}}"></i>
    {{$gallery->status ? 'Activo' : 'Inactivo'}}
</span>
