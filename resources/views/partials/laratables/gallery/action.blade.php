<div class="d-flex flex-column">
    <div class="d-flex flex-row">
        {{-- <a class="btn btn-info btn-sm mx-1" target="_blank" href="/admin/gallery/image/{{$gallery->id}}">
            <i class="mdi mdi-eye"></i> Ver
        </a> --}}
        <a class="btn btn-success btn-sm mx-1" href="/admin/gallery/edit/{{$gallery->id}}">
            <i class="mdi mdi-pencil"></i> Editar
        </a>
        <form action="{{route('gallery.change', $gallery->id)}}" method="POST"
            style="display: inline-block">
          {{csrf_field()}}
          <button class="btn btn-warning btn-sm">
              <i class="mdi mdi-delete"></i>
              Cambiar status
          </button>
      </form>
      <form action="{{route('gallery.destroy', $gallery->id)}}" method="POST"
        style="display: inline-block">
      {{csrf_field()}}
      <button class="btn btn-danger btn-sm">
          <i class="mdi mdi-delete"></i>
            Eliminar
      </button>
  </form>
    </div>
</div>
