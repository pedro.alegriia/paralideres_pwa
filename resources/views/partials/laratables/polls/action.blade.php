@if($poll->active)
    <a class="btn btn-primary btn-sm"
       href="{{route('encuestas.status', $poll->id)}}">
        <i class="mdi mdi-lock"></i> Abrir
    </a>
@else
    <a class="btn btn-primary btn-sm"
       href="{{route('encuestas.status', $poll->id)}}">
        <i class="mdi mdi-lock-open"></i> Cerrar
    </a>
@endif
<a class="btn btn-warning btn-sm" href="{{route('encuestas.edit', $poll->id)}}">
    <i class="mdi mdi-table-edit"></i> Editar
</a>
<form action="{{route('encuestas.destroy', $poll->id)}}" method="POST"
      style="display: inline-block">
    {{csrf_field()}}
    {{method_field('delete')}}
    <button class="btn btn-danger btn-sm"><i class="mdi mdi-delete"></i> Eliminar
    </button>
</form>
