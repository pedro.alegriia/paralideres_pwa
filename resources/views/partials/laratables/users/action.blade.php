<div class="d-flex flex-column">
    <div class="d-flex flex-row">
        <a class="btn btn-primary btn-sm m-1" href="{{route('usuarios.edit', $user->id)}}">
            <i class="mdi mdi-table-edit"></i> Editar
        </a>
        <form action="{{route('usuarios.destroy', $user->id)}}" method="POST"
              style="display: inline-block">
            {{csrf_field()}}
            {{method_field('delete')}}
            <button class="btn btn-danger btn-sm m-1"><i class="mdi mdi-delete"></i> Eliminar
            </button>
        </form>
    </div>
</div>
