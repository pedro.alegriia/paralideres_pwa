<span class='badge badge-{{$resource->published ? 'success' : 'danger'}}'>
        <i class="mdi mdi-{{$resource->published ? 'check-circle' : 'close-octagon'}}"></i>
        {{$resource->published ? 'Publicado' : 'No publicado'}}
</span>
