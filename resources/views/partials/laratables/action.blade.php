<div class="d-flex flex-column">
    <div class="d-flex flex-row">
        <a href="/app/resources/{{$resource->slug}}" class="btn btn-sm btn-success m-1"><i class="mdi mdi-eye"></i> Ver</a>
        <a href="/app/editar/{{$resource->slug}}" class="btn btn-sm btn-warning m-1"><i class="mdi mdi-pencil"></i>
            Editar</a>
    </div>
    <div class="d-flex flex-row">
        <a href="{{route('recursos.published', $resource->id)}}" class="btn btn-sm btn-primary m-1">
            <i class="mdi mdi-lock{{$resource->published ? '' : '-open'}}"></i>
            {{$resource->published ? 'Ocultar' : 'Publicar'}}</a>
        {{--<a href="/recurso/{{$resource->id}}" class="btn btn-sm btn-danger m-1"><i class="mdi mdi-delete"></i>
            Eliminar</a>--}}
        @if(!$resource->deleted_at)
            <form action="{{route('recursos.destroy', $resource->id)}}" method="POST"
                  style="display: inline-block">
                {{csrf_field()}}
                {{method_field('delete')}}
                <button class="btn btn-danger btn-sm">
                    <i class="mdi mdi-delete"></i>
                    Eliminar
                </button>
            </form>
        @else
            <form action="{{route('recursos.recover', $resource->id)}}" method="POST"
                  style="display: inline-block">
                {{csrf_field()}}
                <button class="btn btn-success btn-sm"><i class="mdi mdi-check"></i>
                    Recuperar
                </button>
            </form>
        @endif
    </div>
</div>
