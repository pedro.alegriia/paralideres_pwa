<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width"/>
    <style type="text/css">
        * {
            margin: 0;
            padding: 0;
            font-size: 100%;
            font-family: 'Avenir Next', "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
            line-height: 1.65;
        }

        img {
            max-width: 100%;
            margin: 0 auto;
            display: block;
        }

        body, .body-wrap {
            width: 100% !important;
            height: 100%;
            background: #F1F1F1;
        }

        a {
            color: #EAC14D;
            text-decoration: none;
        }

        a:hover {
            text-decoration: underline;
        }

        .text-center {
            text-align: center;
        }

        .text-right {
            text-align: right;
        }

        .text-left {
            text-align: left;
        }

        .button {
            display: inline-block;
            color: white;
            background: #2E353E;
            border: solid #2E353E;
            border-width: 10px 20px 8px;
            font-weight: bold;
            border-radius: 4px;
        }

        .button:hover {
            text-decoration: none;
        }

        h1, h2, h3, h4, h5, h6 {
            margin-bottom: 20px;
            line-height: 1.25;
        }

        h1 {
            font-size: 32px;
        }

        h2 {
            font-size: 28px;
        }

        h3 {
            font-size: 24px;
        }

        h4 {
            font-size: 20px;
        }

        h5 {
            font-size: 16px;
        }

        p, ul, ol {
            font-size: 16px;
            font-weight: normal;
            margin-bottom: 20px;
        }

        .container {
            display: block !important;
            clear: both !important;
            margin: 0 auto !important;
            max-width: 580px !important;
        }

        .container table {
            width: 100% !important;
            border-collapse: collapse;
        }

        .container .masthead {
            padding: 80px 0;
            background: #2E353E;
            color: white;
        }

        .container .masthead h1 {
            margin: 0 auto !important;
            max-width: 90%;
            text-transform: uppercase;
        }

        .container .content {
            background: white;
            padding: 30px 35px;
        }

        .container .content.footer {
            background: none;
        }

        .container .content.footer p {
            margin-bottom: 0;
            color: #A4A6A9;
            text-align: center;
            font-size: 14px;
        }

        .container .content.footer a {
            color: #A4A6A9;
            text-decoration: none;
            font-weight: bold;
        }

        .container .content.footer a:hover {
            text-decoration: underline;
        }

    </style>
</head>
<body>
<table class="body-wrap">
    <tr>
        <td class="container">
            <!-- Message start -->
            <table>
                <tr>
                    <td align="center" class="masthead">
                        <a href="http://paralideres.org/" target="_blank"><img
                                    src="https://para-lideres-bucket.s3.amazonaws.com/newsletter/logo+para+lideres.png"
                                    alt="Versión Beta Paralideres.org" width="100%" style="background:#2e353e"></a>
                    </td>
                </tr>
                <tr>
                    <td class="content">
                        <h2>Nos da mucho gusto anunciar que pronto tendrás acceso al nuevo ParaLideres.org,</h2>
                        <p><a href="http://paralideres.org/" target="_blank">La pagina</a>, <a
                                    href="https://blog.paralideres.org/" target="_blank">el blog</a> y <a
                                    href="https://www.youtube.com/user/ParaLideres/" target="_blank">el canal de
                                YouTube</a> será aun más útil. Tendrás acceso a todos lo contenidos, en cualquier lugar,
                            en cualquier momento, y desde cualquier dispositivo. Será más fácil compartir y colaborar,
                            incluso con tus propios vídeos.</p>

                        <p>Con el propósito de que pronto lo puedas disfrutar, este fin de semana el sitio no estará
                            disponible. <strong>Si necesitas algún recurso o documento, por favor, bájalo antes de las
                                10:00 GMT-7 del domingo 15 de marzo 2020.</strong></p>
                        <p>La nueva versión estará disponible <strong>desde el lunes 16 de este mes de marzo del 2020, a
                                las 10:00 GMT-7</strong></p>
                        <p>Ahora la oportunidad de servir… Necesitamos que accedas a la página actual <a
                                    href="http://paralideres.org/" target="_blank">ParaLideres.org</a> y participes de
                            la encuesta de una sola pregunta, que se encuentra allí. Nos interesa conocer más de ti,
                            además de tus necesidades, para así, poder proveerte los mejores recursos.</p>
                        <a href="http://paralideres.org/" target="_blank"><img
                                    src="https://para-lideres-bucket.s3.amazonaws.com/newsletter/header+para+lideres.jpg"
                                    alt="Header Paralideres.org"
                                    width="100%"></a>
                        <table>
                            <tr>
                                <td align="center">
                                    <p>
                                        <a href="http://paralideres.org/" target="_blank" class="button">Descargar
                                            recursos</a>
                                    </p>
                                </td>
                            </tr>
                        </table>
                        <p>¡Muchas Gracias!
                            Y gracias por tu interés en la juventud,
                            <a href="http://paralideres.org/" target="_blank">ParaLideres.org</a>.</p>
                        <p><em>– El equipo de ParaLideres.org</em></p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="container">
            <!-- Message start -->
            <table>
                <tr>
                    <td class="content footer" align="center">
                        <p>Enviado por: <a href="http://paralideres.org/" target="_blank">ParaLideres.org © 2020</a></p>
                        <p><a href="mailto:soporte@paralideres.org">Reportar una falla</a> | <a
                                    href="mailto:soporte@paralideres.org">Cancelar la suscripción</a></p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
