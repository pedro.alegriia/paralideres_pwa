<footer class="footer_area form_hide_m">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="footer_top_link text-center">
                    <a href="/app/resources">RECURSOS</a>
                    <a href="/app/collections">COLECCIONES</a>
                </div>
            </div>
        </div>
    </div>
    <p class="footer-link">ParaLideres.org © {{date('Y')}} | <a href="/app/terms">Términos</a> | <a href="/app/notice">Privacidad</a>
        | <a
                href="#">Todos los derechos reservados</a></p>
</footer>

<footer class="footer_area footer_area_m form_hide_d">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="footer_top_link text-center">
                    <a href="/app/resources">RECURSOS</a>
                    <a href="/app/collections">COLECCIONES</a>
                </div>
            </div>

            {{--@foreach($categoriesCollections as $catColl)
                <div class="col-xs-6">
                    <div class="footer_content_m">
                        <ul>
                            @foreach($catColl->collections as $collection)
                                <li><a href="{{url($collection->slug)}}">{{$collection->label}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endforeach--}}
        </div>
    </div>
    <p class="footer-link">ParaLideres.org © {{date('Y')}} | <a href="/app/terms">Términos</a> | <a href="/app/notice">Privacidad</a>
        | <a
                href="#">Todos los derechos reservados</a></p>
</footer>
