<section class="social_share_area">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 text-center">
                <div class="social_inner text-center">
                    <h3>
                        <span>
                            <img src="{{asset('images/social-3.png')}}" alt="">
                        </span> Facebook
                        <a href="https://www.facebook.com/paralideres" target="_blank">Siguenos!</a>
                    </h3>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="social_inner social_inner_2 text-center">
                    <h3>
                        <span>
                            <img src="{{asset('images/social-2.png')}}" alt="">
                        </span> Twitter
                        <a href="https://www.twitter.com/paralideres" target="_blank">Siguenos!</a>
                    </h3>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="social_inner social_inner_3 text-center">
                    <h3>
                        <span>
                            <img src="{{asset('images/social-1.png')}}" alt="">
                        </span>
                        YouTube
                        <a href="https://www.youtube.com/paralideres" target="_blank">Siguenos!</a>
                    </h3>
                </div>
            </div>
        </div>
    </div>
</section>
