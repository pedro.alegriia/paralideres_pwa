<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <!-- <link rel="preload" href="{{asset('css/stable.css')}}" as="style"> -->
    <!-- <link rel="preload" href="{{asset('fonts/FiraSans-Bold.woff')}}" as="style">
    <link rel="preload" href="{{asset('fonts/FiraSans-Book.woff')}}" as="style">
    <link rel="preload" href="{{asset('fonts/FiraSans-Regular.woff')}}" as="style"> -->
    <!-- {{-- <link rel="preconnect" href="https://stackpath.bootstrapcdn.com"> --}} -->
    <link rel="preconnect" href="https://www.googletagmanager.com">
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> -->
    <meta property="og:type" content="article"/>
    <meta property="og:url" content="https://paralideres.org"/>
    <meta property="og:site_name" content="https://paralideres.org"/>
    <meta property="og:title" content="ParaLíderes"/>
    <meta property="og:image" content="{{env('APP_URL')}}/images/para-lideres-logo-navbar.png"/>
    <meta property="og:determiner" content="auto"/> 
    <meta property="og:description" content="Bienvenido a la nueva versión beta de ParaLíderes"/>

    <meta http-equiv="Content-Type" content="text/html; charset=gb18030">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="user" content="{{Auth::user()}}">
    <link rel="manifest" href="manifest.json" />

    {{--
    @if(Auth::check())
        <meta name="admin_role" content="{{Auth::user()->hasRole('admin')}}">
        <meta name="roles" content="{{Auth::user()->getRoleNames()->implode(',')}}">
    @else
        <meta name="admin_role" content="">
        <meta name="roles" content="">
    @endif
    --}}
    <title>ParaLideres</title>
    {{-- <title>{{ config('app.name', 'ParaLideres') }}</title> --}}
    <meta name="description" content="ParaLideres">
    <link rel="icon" type="image/ico" sizes="32x32" href="{{asset('favicon.ico')}}">

    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="//fonts.googleapis.com/icon?family=Material+Icons">
    <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">
    <!-- <link href="{{asset('stable.css')}}" rel="stylesheet"> -->
    {{-- <link href="{{asset('front-src/css/custom.css')}}" rel="stylesheet"> --}}

    @yield('styles')

    <script>
        window.asset = '{{env("APP_URL")}}/';
        window.base_url = '{{env("APP_URL")}}/';
        window.img_path = '{{env("APP_URL")}}/';
        window.api_url = 'api/v1/';
    </script>
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NBQJD6F"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->

<div id="app">
    @yield('content')
</div>

{{--@include('layouts.common.footer')--}}

<!--Scripts--->
@yield('scripts')

<script src="{{ asset('js/app.js') }}" defer></script>
<script>
    if ('serviceWorker' in navigator) {
        window.addEventListener('load', () => {
            navigator.serviceWorker.register('/sw.js');
        });
    }
</script>

<noscript>
    <img height="1" width="1" src="https://www.facebook.com/tr?id=227034675295787&ev=PageView&noscript=1"/>
</noscript>

<!-- Facebook Pixel Code -->
<script>
    !function (f, b, e, v, n, t, s) {
        if (f.fbq) return;
        n = f.fbq = function () {
            n.callMethod ?
                n.callMethod.apply(n, arguments) : n.queue.push(arguments)
        };
        if (!f._fbq) f._fbq = n;
        n.push = n;
        n.loaded = !0;
        n.version = '2.0';
        n.queue = [];
        t = b.createElement(e);
        t.async = !0;
        t.src = v;
        s = b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t, s)
    }(window, document, 'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '227034675295787');
    fbq('track', 'PageView');
</script>
<!-- End Facebook Pixel Code -->
</body>
</html>