/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

// require('./bootstrap');

// window.Vue = require('vue');

// //call componets
// //Vue.component('example-component', require('./components/ExampleComponent.vue').default);
// //Vue.component('twonav-component', require('./components/Index.vue').default);
// //Vue.component('banner-component', require('./components/BannerComponent.vue').default);

// const app = new Vue({
//     el: '#app',
// });
import Vue from "vue";
import 'es6-promise/auto'
import axios from 'axios'
import VueAuth from '@websanova/vue-auth'
// import auth                  from '@websanova/vue-auth/dist/v2/vue-auth.common.js';
import VueAxios from 'vue-axios'
import VueRouter from 'vue-router'

import App from "./App.vue";
import router from "./router";
import auth from './auth'

import MaterialKit from "./plugins/material-kit";
import Filters from "./plugins/filters";
import Notifications from 'vue-notification'
import Vuelidate from 'vuelidate'
import StarRating from 'vue-star-rating'
import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'
import Loading from 'vue-loading-overlay';
import 'vue-loading-overlay/dist/vue-loading.css';

// Set Vue globally
window.Vue = Vue

// Set Vue router
Vue.router = router
Vue.use(VueRouter)

// Set Vue authentication
Vue.use(VueAxios, axios)
    // axios.defaults.baseURL = `${process.env.MIX_APP_URL}`
Vue.use(VueAuth, auth)

Vue.config.productionTip = false;
Vue.use(VueMaterial);
Vue.use(Filters);
// Vue.use(VueMaterial.default)
Vue.use(MaterialKit);
Vue.use(Notifications)
Vue.use(Vuelidate)
Vue.component('star-rating', StarRating);
Vue.component('loading', Loading);

const NavbarStore = {
    showNavbar: false
};

Vue.mixin({
    data() {
        return {
            NavbarStore
        };
    }
});

new Vue({
    router,
    render: h => h(App),
    data() {
        return {
            countries: [
                { id: 1, name: "México" },
                { id: 2, name: "Colombia" },
                { id: 3, name: "Estados Unidos" },
                { id: 4, name: "Brasil" },
                { id: 5, name: "Argentina" },
                { id: 6, name: "Cuba" },
                { id: 7, name: "Canadá" },
                { id: 8, name: "Puerto Rico" },
                { id: 9, name: "Chile" },
                { id: 10, name: "Costa Rica" },
            ]
        }
    },
    methods: {
        getCountryName(id) {
            if (id) {
                let country = this.countries.find(element => element.id == id);
                if (country) return country.name
            }
        },
        SuccessMgs(text) {
            this.$notify({
                group: 'main',
                type: 'succes',
                title: 'Completado',
                text: text
            });
        },
        WarnMgs(text) {
            this.$notify({
                group: 'main',
                type: 'warn',
                title: 'Alerta',
                text: text
            });
        },
        ErrorMgs(text) {
            this.$notify({
                group: 'main',
                type: 'error',
                title: 'Error',
                text: text
            });
        }
    }
}).$mount("#app");