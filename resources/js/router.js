import Vue from "vue";
import Router from "vue-router";
import Index from "./views/Index.vue";
import Home from "./views/Home.vue";
import PublishResource from "./views/resource/publish";
import Resource from "./views/resource";
import ResourceId from "./views/resource/detail";
import InResource from "./views/resource/in";
import PublishCollection from "./views/collection/publish";
import Collection from "./views/collection";
import InCollection from "./views/collection/in";
import CollectionDetail from "./views/collection/detail";
import Account from "./views/account";
import author from "./views/author"
import RegisterWin from "./views/registerWin";
import Terms from "./views/Terms";
import Landing from "./views/Landing.vue";
import Login from "./views/Login.vue";
import Profile from "./views/Profile.vue";
import MainNavbar from "./layout/MainNavbar.vue";
import Navbar from "./layout/Navbar.vue";
import MainFooter from "./layout/MainFooter.vue";

Vue.use(Router);

export default new Router({
    routes: [{
            path: "/",
            name: "index",
            components: { default: Home, header: Navbar, footer: MainFooter },
            props: {
                header: { colorOnScroll: 400 },
                footer: { backgroundColor: "black" }
            }
        },
        {
            path: "/recursos", //Publico
            name: "resources",
            components: { default: Resource, header: Navbar, footer: MainFooter },
        },
        {
            path: "/recursos/:id?", //Publico
            name: "resourcedetail",
            components: { default: ResourceId, header: Navbar, footer: MainFooter },
        },
        {
            path: "/in-recursos", //Privado
            name: "in-resources",
            components: { default: InResource, header: Navbar, footer: MainFooter },
            meta: {
                auth: true
            }
        },
        {
            path: "/publicar-recurso/:id?",
            name: "resource",
            components: { default: PublishResource, header: Navbar, footer: MainFooter },
            meta: {
                auth: true
            }
        },
        {
            path: "/colecciones",
            name: "collections",
            components: { default: Collection, header: Navbar, footer: MainFooter },
        },
        {
            path: "/in-colecciones",
            name: "in-collections",
            components: { default: InCollection, header: Navbar, footer: MainFooter },
            meta: {
                auth: true
            }
        },
        {
            path: "/coleccion-detalle/:id?",
            name: "collection-detail",
            components: { default: CollectionDetail, header: Navbar, footer: MainFooter },
        },
        {
            path: "/publicar-coleccion/:id?",
            name: "collection",
            components: { default: PublishCollection, header: Navbar, footer: MainFooter },
            meta: {
                auth: true
            }
        },
        {
            path: "/cuenta",
            name: "account",
            components: { default: Account, header: Navbar, footer: MainFooter },
            meta: {
                auth: true
            }
        },
        {
            path: "/autor/:id?",
            name: "author",
            components: { default: author, header: Navbar, footer: MainFooter },
            // meta: {
            //     auth: true
            // }
        },
        {
            path: "/landing",
            name: "landing",
            components: { default: Landing, header: MainNavbar, footer: MainFooter },
            props: {
                header: { colorOnScroll: 400 },
                footer: { backgroundColor: "black" }
            }
        },
        {
            path: "/login",
            name: "login",
            components: { default: Login, header: Navbar, footer: MainFooter },
            props: {
                header: { colorOnScroll: 400 }
            }
        },
        {
            path: "/profile",
            name: "profile",
            components: { default: Profile, header: MainNavbar, footer: MainFooter },
            props: {
                header: { colorOnScroll: 400 },
                footer: { backgroundColor: "black" }
            },
            meta: {
                auth: true
            }
        },
        {
            path: "/registerWin",
            name: "registerWin",
            components: { default: RegisterWin, header: Navbar, footer: MainFooter },
        },
        {
            path: "/terms",
            name: "termsandconditions",
            components: { default: Terms, header: Navbar, footer: MainFooter },
            meta: {
                auth: false
            }
        }
    ],
    scrollBehavior: to => {
        if (to.hash) {
            return { selector: to.hash };
        } else {
            return { x: 0, y: 0 };
        }
    }
});