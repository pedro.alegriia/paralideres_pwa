import bearer from '@websanova/vue-auth/drivers/auth/bearer'
import axios from '@websanova/vue-auth/drivers/http/axios.1.x'
import router from '@websanova/vue-auth/drivers/router/vue-router.2.x'
// import bearer from '@websanova/vue-auth/src/drivers/auth/bearer'
// import axios from '@websanova/vue-auth/src/drivers/http/axios.1.x'
// import router from '@websanova/vue-auth/src/drivers/router/vue-router.2.x'


/**
 * Authentication configuration, some of the options can be override in method calls
 */
const config = {
    auth: bearer,
    http: axios,
    router: router,
    tokenDefaultName: 'laravel-jwt-auth',
    tokenStore: ['localStorage'],
    rolesVar: 'role', //User model field which contains the user role details.

    // API endpoints used in Vue Auth.
    registerData: {
        url: 'api/register',
        method: 'POST',
        // redirect: '/login'
    },
    loginData: {
        url: '/api/login',
        method: 'POST',
        // redirect: '/',
        fetchUser: true
    },
    logoutData: {
        url: 'api/logout',
        method: 'POST',
        redirect: '/',
        makeRequest: true
    },
    fetchData: {
        url: '/api/user',
        method: 'GET',
        enabled: true
    },
    refreshData: {
        url: '/api/refresh',
        method: 'GET',
        enabled: true,
        interval: 30
    }
}
export default config