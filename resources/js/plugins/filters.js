// import Vue from 'vue'

export default {
    install(Vue) {

        Vue.filter('capitalize', val => val.toUpperCase())

        /**
        * @param {String} text    The value string.
         * @param {Number} decimals The number of characters required.
         */
        Vue.filter('truncateText', function (text, size) {
            const clamp = '...';
            if (size) {
                if (text && text.length <= size) {
                    return text;
                } else {
                    return text.slice(0, size) + clamp
                }
            } else {
                return text;
            }
        });

    }
}