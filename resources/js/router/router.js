/*=========================================================================================
  File Name: router.js
  Description: Routes for vue-router. Lazy loading is enabled.
  Object Structure:
                    path => router path
                    name => router name
                    component(lazy loading) => component to load
                    meta : {
                      rule => which user can have access (ACL)
                      breadcrumb => Add breadcrumb to specific page
                      pageTitle => Display title besides breadcrumb
                    }
  ----------------------------------------------------------------------------------------
=============================*/
import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter);

const isAuthenticated = (to, from, next) => {
    if (store.getters.authenticatedUser) {
        /* if (moment().isAfter(store.getters.session_expires_at)) {
            store.dispatch('logout');
        } */
        next();
    }
    //notify cant without authenticated user
};


import Home from "../views/Index.vue";

const routes = [
    {
        path:'/app',
        component: Home,
        children: [
            {
                name:'/login',
                path:'Login',
                component:() => import('pages/login.vue'),
            },
            {
                name:'/register',
                path:'Register',
                component:() => import('pages/register.vue'),
            },
            {
                path: '/account',
                name: 'Account',
                component: () => import('pages/account.vue'),
            },
            {
                path: '/profile/:id',
                name: 'Profile',
                component: () => import('pages/profile.vue'),
            },
            {
                path: '/resources',
                name: 'resources',
                component: () => import('./pages/resources/index.vue'),
                meta: {
                    rule: 'admin', //acl
                    breadcrumb:[''],
                    no_scroll: true,
                },
                children:[
                    {
                        path: '/rated/:id',
                        name: 'resources-ranked',
                        component:() => import ('./components/resources/ranked.vue'),
                    },
                    {
                        path: '/resources/:slug',
                        name: 'resource',
                        component: () => import ('./components/index/resource.vue'),
                    },
                    {
                        path: '/create',
                        name: 'create-resource',
                        component: () => import ('./components/resources/form.vue'),
                        beforeEnter: isAuthenticated,
                    },
                    {
                        path: '/edit/:slug',
                        name: 'edit-resource',
                        component: () => import ('./components/resources/form.vue'),
                        beforeEnter: isAuthenticated,
                    },
                ]
            },
            {
                path: '/collection',
                name: 'collection',
                component: () => import('./pages/collection/index.vue'),
                meta: {
                    breadcrumb:[''],
                    no_scroll: true,
                },
                children:[
                    {
                        path: '/collection/:slug',
                        name: 'resource',
                        component: () => import ('./pages/collection/collection.vue'),
                    },
                    {
                        path: '/create',
                        name: 'create-collection',
                        component: () => import ('./pages/collection/form.vue'),
                        beforeEnter: isAuthenticated,
                    },
                    {
                        path: '/edit/:slug',
                        name: 'edit-resource',
                        component: () => import ('./pages/collection/form.vue'),
                        beforeEnter: isAuthenticated,
                    },
                ]
            },
            ...[
                '/terms',
                '/notice',
                '/accept-terms',
              ].map(component_name => ({
                path: component_name,
                name: component_name,
                component: () => import(`pages/static/${component_name}.vue`),
              })),        
        ]
    },
],

export default routes;