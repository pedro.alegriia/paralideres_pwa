$(document).ready(function () {
    let cookieAlert = document.querySelector('.cookie-consent-div');
    cookieAlert.addEventListener('click', function () {
        localStorage.setItem('cookieConsent', "true");
        cookieAlert.style.display = 'none';
    });

    if (typeof (Storage) !== "undefined") {
        let cookieConsent = localStorage.getItem('cookieConsent');

        if (cookieConsent !== "true") {
            cookieAlert.style.display = 'block';
        }
    }
});

/*
$(document).ready(function () {
    var service_head = $(".service_head").outerHeight();
    var author = $(".author").outerHeight();
    var comment = $(".comment").outerHeight();
    var minheight = service_head + author + comment;

    var height1 = $(".service_inner > p").map(function () {
            return $(this).height();
        }).get(),

        maxHeight1 = Math.max.apply(null, height1);

    var height2 = $(".service_inner > h4").map(function () {
            return $(this).height();
        }).get(),

        maxHeight2 = Math.max.apply(null, height2);

    maxHeight = maxHeight1 + maxHeight2;

    totalheight = minheight + maxHeight + "px";
    $(".service_inner").css("min-height", totalheight);
});*/
