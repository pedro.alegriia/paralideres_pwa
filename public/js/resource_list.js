/**
 * Created by Tarek on 10/20/2017.
 */
$(document).ready(function () {
    new Vue({
        el: '#app',
        data: {
            asset: window.asset,
            base_url: window.base_url,
            img_path: window.img_path,
            api_url: window.api_url,
            resources: {
                data: [],
            },
            //resources: []
        },
        mounted() {
            this.getResources();
        },
        filters: {
            truncate: function (string, value) {
                return string.substr(0, value) + '...';
            }
        },
        methods: {
            getResources() {
                this.$common.loadingShow(0);
                axios.get(`${this.api_url}resources?${search_text}${tag_slug}${cat_slug}${author}`)
                    .then((response) => {
                        console.log(response);
                        this.resources = response.data.data;
                        console.log(this.resources);
                        this.$common.loadingHide(0);
                        setTimeout(function () {
                            addthis.toolbox('.addthis_toolbox')
                        }, 2000);
                    });
            },
            getNextResources: function (next_page_url) {
                var THIS = this;
                THIS.$common.loadingShow(0);
                axios.get(next_page_url)
                    .then(function (response) {
                        THIS.resources = response.data.data;
                        THIS.$common.loadingHide(0);
                    });
                $('html, body').animate({
                    scrollTop: 0
                }, 500);
            },
            filterResource: function () {
                var THIS = this;
                var formData = $('#filterResource').serialize();
                THIS.$common.loadingShow(0);
                axios.get(THIS.api_url + 'resources?' + formData + author).then(function (response) {
                    THIS.resources = response.data.data;
                });
            },
            /*givenResourceLike: function (resource) {
                var THIS = this;
                THIS.$common.loadingShow(0);
                axios.post(THIS.api_url + 'resources/' + resource.id + '/like')
                    .then(function (response) {
                        if (response.data.status === 'like') {
                            if (resource.likes_count.length > 0) {
                                resource.likes_count[0].total = parseInt(resource.likes_count[0].total) + 1;
                            } else {
                                resource.likes_count = [{'resource_id': resource.id, 'total': 1}];
                            }
                            resource.like = [{'resource_id': resource.id, 'user_id': null}];
                        } else if (response.data.status === 'unlike') {
                            if (resource.likes_count.length > 0) {
                                resource.likes_count[0].total = parseInt(resource.likes_count[0].total) - 1;
                            } else {
                                resource.likes_count = [{'resource_id': resource.id, 'total': 0}];
                            }
                            resource.like = [];
                        }
                    });
            },*/
            givenResourceLike(resource) {
                if (!this.isLoggedIn()) {
                    return false;
                }

                axios.post(this.api_url + 'resources/' + resource.id + '/like')
                    .then(response => {
                        console.log(response);
                        if (response.data.status) {
                            this.$common.showMessage({title: response.data.message, status: 'success'});
                            this.getResources();
                        }
                    })
                    .catch(error => {
                        console.log("Error #384-2");
                    });
            },
            getImagePath(resource) {
                let path = this.img_path + '/images/love.jpg';

                if (resource.likes.length) {
                    path = this.img_path + '/images/love3.png';
                }

                return path;
            },
            getCategoryImagePath(resource) {
                let path = this.img_path + '/images/icon/cat-icon-' + resource.category.id + '.png';

                if ([9, 11, 12].indexOf(resource.category.id) > -1) {
                    path = this.img_path + '/images/icon/cat-icon-12.png';
                }

                return path;
            },
            getAuthorImagePath(resource) {
                let path = this.img_path + '/images/user.png';

                if (resource.user.image) {
                    path = this.asset + 'uploads/' + resource.user.image;
                }

                return path;
            },
            isLoggedIn() {
                return localStorage.getItem('access_token') !== null;
            },
        }
    });
});