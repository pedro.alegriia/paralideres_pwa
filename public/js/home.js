$(document).ready(function () {
    new Vue({
        el: '#app',
        data: {
            asset: window.asset,
            base_url: window.base_url,
            img_path: window.img_path,
            api_url: window.api_url,
            resources: {
                data: []
            },
            checkedAnswer: 0,
            poll: null,
            pollResult: true,
            pagination: 2,
            selectedPollResponse: null,
        },
        created: function () {
            this.getResources();
            this.getLastPoll();
        },
        filters: {
            truncate: function (string, value) {
                return string.substring(0, value) + '...';
            }
        },
        methods: {
            getResources(search, value) {
                console.log(search);
                this.$common.loadingShow(0);
                let action_url;
                if (value !== undefined) {
                    action_url = '/search?search=' + value;
                } else {
                    action_url = '/search?search=';
                }

                axios.get(this.api_url + 'resources' + action_url)
                    .then(response => {
                        console.log(response.data);
                        this.resources.data = response.data.resources;
                        this.$common.loadingHide(0);
                    });
            },
            getNextResources: function () {
                var THIS = this;
                THIS.$common.loadingShow(0);
                var action_url = '/search?pageNo=' + THIS.pagination;
                axios.get(this.api_url + 'resources' + action_url)
                    .then(function (response) {
                        THIS.pagination = THIS.pagination + 1;
                        THIS.resources.data = response.data;
                        THIS.$common.loadingHide(0);
                    });
                /*alert(next_page_url);
                var THIS = this;
                THIS.$common.loadingShow(0);
                axios.get(next_page_url).then(function (response) {
                    THIS.resources = response.data.data;
                    THIS.$common.loadingHide(0);
                });*/

                $('html, body').animate({scrollTop: 200}, 500);
            },
            getCategoryResources: function (slug) {
                var THIS = this;
                this.$common.loadingShow(0);
                axios.get(THIS.api_url + 'categories/' + slug + '/resources').then(function (response) {
                    THIS.resources = response.data.data;
                    THIS.$common.loadingHide(0);
                });

            },
            givenResourceLike(resource) {
                if (!this.isLoggedIn()) {
                    return false;
                }

                axios.post(this.api_url + 'resources/' + resource.id + '/like')
                    .then(response => {
                        console.log(response);
                        if (response.data.status) {
                            this.$common.showMessage({title: response.data.message, status: 'success'});
                            this.getResources();
                        }
                        /*if (response.data.status === 'like') {
                            if (resource.likes.length) {
                                resource.likes_count[0].total = parseInt(resource.likes_count[0].total) + 1;
                            } else {
                                resource.likes_count = [{'resource_id': resource.id, 'total': 1}];
                            }
                            resource.like = [{'resource_id': resource.id, 'user_id': null}];
                        }
                        else if (response.data.status === 'unlike') {
                            if (resource.likes_count.length > 0) {
                                resource.likes_count[0].total = parseInt(resource.likes_count[0].total) - 1;
                            } else {
                                resource.likes_count = [{'resource_id': resource.id, 'total': 0}];
                            }
                            resource.like = [];
                        }*/
                    })
                    .catch(error => {
                        console.log("Error #384-2");
                    });
            },
            getLastPoll() {
                axios.get(this.api_url + 'polls/last')
                    .then(response => {
                        this.poll = response.data.data;
                        this.selectedPollResponse = null;
                    });
            },
            votePoll() {
                if (!this.isLoggedIn()) {
                    this.$common.showMessage({title: 'Debes iniciar sesión antes de poder votar', status: 'error'});
                }

                this.$common.showLoading();
                axios.post(this.base_url + this.api_url + 'polls/' + this.poll.id + '/vote', {
                    'option': this.selectedPollResponse,
                }).then(response => {
                    console.log(response);
                    if (response.data.status) {
                        this.$common.hideLoading();
                        this.$common.showMessage({title: response.data.message, status: 'success'});
                        this.getLastPoll();
                    }
                }).catch(error => {
                    this.$common.hideLoading();
                    if (error.response.status === 500 && error.response.data.code === 500) {
                        this.$common.showMessage(error);
                    }
                });
            },
            getImagePath(resource) {
                let path = this.img_path + '/images/love.jpg';

                if (resource.likes.length) {
                    path = this.img_path + '/images/love3.png';
                }

                return path;
            },
            getCategoryImagePath(resource) {
                let path = this.img_path + '/images/icon/cat-icon-' + resource.category.id + '.png';

                if ([9, 11, 12].indexOf(resource.category.id) > -1) {
                    path = this.img_path + '/images/icon/cat-icon-12.png';
                }

                return path;
            },
            getAuthorImagePath(resource) {
                let path = this.img_path + '/images/user.png';

                if (resource.user.image) {
                    path = this.asset + 'uploads/' + resource.user.image;
                }

                return path;
            },
            isLoggedIn() {
                return localStorage.getItem('access_token') !== null;
            },
        }
    });
});


/*
$(document).ready(function(){
    new Vue({
        el: '#app',
        data:{
            asset: window.asset,
            base_url: window.base_url,
            api_url: window.api_url,
            resources: [],
            poll:[],
            pollResult:false
        },

        created(){
            this.getResources();
            this.getLastPoll();
        },

        filters: {
            truncate: function (string, value) {
                return string.substring(0, value) + '...';
            }
        },

        methods: {

            getResources(search=null, value=null){
                this.$common.loadingShow(0);
                if(search !=null){
                    var action_url = '/'+search+'?search='+value;
                }else{
                    var action_url = '';
                }
                axios.get(this.api_url+'resources'+action_url)
                    .then(response => {
                    this.resources = response.data.data;
                this.$common.loadingHide(0);
                // console.log(response);
                // console.log(this.resources);
            });
            },

            getNextResources(next_page_url){
                this.$common.loadingShow(0);
                axios.get(next_page_url)
                    .then(response => {
                    this.resources = response.data.data;
                this.$common.loadingHide(0);
                // console.log(response);
                // console.log(this.resources);
            });
                $('html, body').animate({
                    scrollTop: 0
                }, 500);
            },

            getCategoryResources(slug){
                this.$common.loadingShow(0);
                axios.get(this.api_url+'categories/'+slug+'/resources')
                    .then(response => {
                    this.resources = response.data.data;
                this.$common.loadingHide(0);
                // console.log(response);
                // console.log(this.resources);
            });
            },

            resourceDownload(resource){

            },

            givenResourceLike(resource){
                axios.post(this.api_url+'resources/'+resource.id+'/like')
                    .then(response => {
                    if(response.data.status == 'like'){
                    if(resource.likes_count.length > 0){
                        resource.likes_count[0].total +=1;
                    }else{
                        resource.likes_count = [{'resource_id': resource.id,'total':1}];
                    }
                    resource.like = [{'resource_id': resource.id,'user_id':null}];
                }else if(response.data.status == 'unlike'){
                    if(resource.likes_count.length > 0){
                        resource.likes_count[0].total -=1;
                    }else{
                        resource.likes_count = [{'resource_id': resource.id,'total':0}];
                    }
                    resource.like = [];
                }
            });
            },

            getLastPoll(){
                axios.get(this.api_url+'polls/last')
                    .then(response => {
                    this.poll = response.data.data;
                // console.log(response);
                // console.log(this.poll);
            });
            },

            votePoll(){
                let formID = document.querySelector('#votePoll');
                let formData = new FormData(formID);
                this.$common.loadingShow(0);
                axios.post(this.base_url+this.api_url+'polls/'+this.poll.id+'/vote', {
                    'option': formData.get('poll_option')
                })
                    .then(response => {
                    this.$common.loadingHide(0);
                this.pollResult = true;
                this.poll = response.data.data;
                this.$common.showMessage(response.data);
                console.log(this.pollResult, response.data.data);
            })
            .catch(error => {
                    this.$common.loadingHide(0);
                if (error.response.status == 500 && error.response.data.code == 500) {
                    this.$common.showMessage(error);
                }
            });
            },

        }

    });
});*/
