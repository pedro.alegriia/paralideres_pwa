/**
 * Created by Tarek on 10/21/2017.
 */
$(document).ready(function () {
    let app = null;
    if ($('#create_resource_popup').length) {
        app = new Vue({
            el: '#create_resource_popup',
            data: {
                errors1: [],
                errors2: [],
                errors3: [],
                resource: {
                    title: '',
                    review: '',
                    category_id: '',
                    tags_id: [],
                    catThumb: '',
                    step: 0,
                    content: '',
                    attach: '',
                },
                file: '',
                tags: [],
                categories: [],
                user: null,
            },
            created: function () {
                $("#old_tag").show();
                $("#new_tag").hide();
            },
            mounted() {
                this.getCategories();
                this.getTags();
                this.user = JSON.parse(document.head.querySelector('meta[name="user"]').content);
            },
            methods: {
                categorySelect: function () {
                    this.resource.catThumb = window.img_path + '/images/icon/cat-icon-' + this.resource.category_id + '.png'
                },
                newTag: function () {
                    $('#select2').prop('selectedIndex', -1);
                    $("#old_tag").hide();
                    $("#new_tag").show();
                },
                oldTag: function () {
                    $('#newTag').val("");
                    $("#old_tag").show();
                    $("#new_tag").hide();
                },
                option1: function () {
                    $(".step_2").hide();
                    $(".step_3").show();
                },
                option2: function () {
                    $(".step_2").hide();
                    $(".step_4").show();
                },
                closePopup: function () {
                    $('body').removeClass('bodyScroll');
                    $(".popup_content").removeClass("open_content");
                    $(".step_1 ,.step_2 ,.step_3, .step_4").hide();
                },
                back1: function () {
                    $(".step_1").show();
                    $(".step_2").hide();
                },
                back2: function () {
                    $(".step_2").show();
                    $(".step_3").hide();
                    $(".step_4").hide();
                },
                stepOne() {
                    this.resource.step = 1;
                    //let THIS = this;
                    //let title = $('#create_resource_form').find('input[name="title"]').val();
                    //let review = $('#create_resource_form').find('textarea[name="review"]').val();
                    //let category_id = $('#create_resource_form').find('select[name="category_id"]').val();
                    this.resource.tags_id = $('#create_resource_form').find('select[name="tags_ids[]"]').val();

                    /*if (!this.resource.title || !this.resource.review || !this.resource.category_id || !this.resource.tags_id) {
                        this.$common.showMessage({title: 'Debes llenar todos los campos', status: 'error'});
                        return false;
                    }*/

                    this.$common.loadingShow(0);
                    //let formData = $('#create_resource_form').serialize();
                    //formData += '&step=1';

                    axios.post('/webapi/resources', this.resource)
                        .then((response) => {
                            console.log(response);
                            this.$common.loadingHide(0);
                            this.errors1 = [];
                            // this.$common.showMessage(response.data);
                            // this.resources = response.data.data;
                            $(".step_1").hide();
                            $(".step_2").show();
                        })
                        .catch((error) => {
                            console.log(error);
                            console.log(error.response);
                            this.$common.loadingHide(0);
                            this.errors1 = [];
                            if (error.response.status === 500 && error.response.data.code === 500) {
                                let errorObject = error.response.data;
                                this.$common.showMessage(errorObject);
                            } else if (error.response.status === 422) {
                                this.errors1 = error.response.data.errors;
                            }
                        });
                },
                stepTwo() {
                    this.resource.step = 2;
                    this.$common.loadingShow(0);
                    let data = {...this.resource, user_id: this.user.id};

                    //axios.post(window.base_url + window.api_url + 'resources', data)
                    axios.post('/webapi/resources', data)
                        .then((response) => {
                            this.$common.loadingHide(0);
                            this.errors2 = [];
                            this.$common.showMessage(response.data);
                            // this.resources = response.data.data;
                            this.closePopup();
                        })
                        .catch((error) => {
                            this.$common.loadingHide(0);
                            this.errors2 = [];
                            if (error.response.status === 500 && error.response.data.code === 500) {
                                let errorObject = error.response.data;
                                // this.$common.showMessage(error);
                            } else if (error.response.status === 422) {
                                this.errors2 = error.response.data.errors;
                            }
                        });
                },
                stepThree() {
                    this.resource.step = 3;
                    this.$common.loadingShow(0);
                    let formData = new FormData();
                    for (key in this.resource) {
                        console.log(key, this.resource[key]);
                        formData.append(key, this.resource[key]);
                    }
                    formData.append('attach', this.file);
                    formData.append('user_id', this.user.id);
                    this.resource.attach = this.file;

                    axios.post('/webapi/resources', formData, {
                        headers: {
                            'Content-Type': 'multipart/form-data'
                        }
                    })
                        .then(response => {
                            console.log(response);
                            this.$common.loadingHide(0);
                            this.errors3 = [];
                            this.$common.showMessage(response.data);
                            // this.resources = response.data.data;
                            this.closePopup();
                        })
                        .catch(error => {
                            this.$common.loadingHide(0);
                            this.errors3 = [];
                            if (error.response.status === 500 && error.response.data.code === 500) {
                                let errorObject = error.response.data;
                                // this.$common.showMessage(error);
                            } else if (error.response.status === 422) {
                                this.errors3 = error.response.data.errors;
                            }
                        });
                },
                handleFileUpload() {
                    this.file = this.$refs.file.files[0];
                },
                getTags() {
                    axios.get("/tags")
                        .then(res => {
                            if (res.status) {
                                this.tags = res.data.tags;
                            }
                        });
                },
                getCategories() {
                    axios.get(window.asset + "api/v1/categories")
                        .then(res => {
                            if (res.status) {
                                this.categories = res.data.categories;
                            }
                        });
                }
            }
        });
    }

    $('body')
        .on('click', '.select2-results__message', function () {
            let text = $('.select2-search__field').val();
            $('.select2-search__field').val('');
            $('.select2-search__field').focus();
            $(this).closest('.select2-container').remove();
            $.ajax({
                url: "/tags/create",
                data: {tag: text},
                method: 'POST',
                success: function (res) {
                    res = JSON.parse(res);
                    if (res.status === 2000) {
                        var target = $('#select2');
                        var html = '<option value="' + res.tag.id + '" selected>' + res.tag.label + '</option>';
                        target.append(html);
                        target.select2();
                    }
                }
            });
        })
        .on('keydown', '.select2-search__field', function (e) {
            if (e.which === 188 || e.which === 13) {
                e.preventDefault()
            }
        })
        .on('keyup', '.select2-search__field', function (e) {
            let trigger = $(this);
            if (e.which === 188 || e.which === 13) {
                e.preventDefault();
                let text = trigger.val();
                trigger.val('');
                trigger.focus();
                text = text.replace(",", "");
                axios.post("/tags", {label: text})
                    .then(res => {
                        app.getTags();
                    });
                /*$.ajax({
                    url: window.asset + "api/v1/tags/create",
                    data: {tag: text},
                    method: 'POST',
                    success: function (res) {
                        res = JSON.parse(res);
                        if (res.status === 2000) {
                            var target = $('#select2');
                            var html = '<option value="' + res.tag.id + '" selected>' + res.tag.label + '</option>';
                            target.append(html);
                            target.select2();
                            trigger.focus();
                        }
                    }
                });*/
            }
        });
});
