let $flashMessage = $('#flash-message');
if ($flashMessage.length) {
    let type = $flashMessage.data('type');
    let message = $flashMessage.data('message');
    $.toast({
        heading: message,
        //text: null,
        position: 'top-right',
        loaderBg: '##d4edda',
        icon: type,
        hideAfter: 5000,
        stack: 6
    });
}