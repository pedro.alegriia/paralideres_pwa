<?php

use Illuminate\Http\Request;

/**
 * API
 */

 //User login 
 Route::post('/login', 'Api\V1\AuthenticateController@login');
Route::post('/register', 'Api\V1\AuthenticateController@register');
Route::post('/forgotPassword', 'Api\V1\AuthenticateController@forgetPassword');

// Refresh the JWT Token
Route::get('refresh', 'Api\V1\AuthenticateController@refresh');
Route::get('recovery_s3_names', 'Api\V1\CollectionController@recovery_s3_names');

Route::middleware('auth:api')->group(function () {
    // Get user info
    Route::get('user', 'Api\V1\AuthenticateController@user');
    Route::post('profile', 'Api\V1\UserController@updateProfile');
    Route::post('profile_image', 'Api\V1\UserController@uploadImageProfile');
    Route::post('resetPassword', 'Api\V1\UserController@resetPassword');

    Route::get('tagsList', 'Api\V1\ResourceTagController@getAll');
    // Logout user from application
    Route::post('logout', 'Api\V1\AuthenticateController@logout');

    Route::post('/resource', 'Api\V1\ResourceController@new_store');

    Route::get('/resource', 'Api\V1\ResourceController@all');

    Route::get('/file/{id}', 'Api\V1\ResourceController@file');
    
    Route::post('/collection', 'Api\V1\CollectionController@save');
    Route::get('/collection', 'Api\V1\CollectionController@all');
    Route::get('/collection/{id}', 'Api\V1\CollectionController@getResourceByCategories');
    Route::get('/collectionById/{id}','Api\V1\CollectionController@getById');
   
    Route::post('/sendVote', 'Api\V1\RatingController@sendVote');
    Route::post('/sendPoll', 'Api\V1\PollController@sendPoll');
    Route::get('/getStatisticsPoll', 'Api\V1\PollController@GetStatisticsPoll');
    Route::get('/getPoll', 'Api\V1\PollController@getActivePoll');
    Route::get('/profile', 'Api\V1\UserController@getProfile');

    Route::post('/subscribe', 'Api\V1\UserController@subscribe');
    
    
});

Route::get('/allCollection', 'Api\V1\CollectionController@publicGet');
Route::get('/allResource', 'Api\V1\ResourceController@publicGet');
Route::get('/resource/{id}', 'Api\V1\ResourceController@getById');

Route::get('/homeResources', 'Api\V1\ResourceController@publicHome');
Route::get('/authors', 'Api\V1\ResourceController@allAuthors');
Route::get('/author/{id}', 'Api\V1\UserController@AuthorById');
Route::get('/topTwelve', 'Api\V1\ResourceController@topTwelve');
Route::get('/getResourceByCollection/{id}', 'Api\V1\CollectionController@getResourceByCollection');
Route::get('/getResourceByCategories', 'Api\V1\ResourceController@getResourceByCategories');
Route::get('/resourceByAuthor/{id}', 'Api\V1\ResourceController@ResourcesByAuthor');
Route::get('/search', 'Api\V1\ResourceController@search');
Route::get('/searchAuthor','Api\V1\ResourceController@slugAuthors');
//User Actions
Route::group(['prefix' => 'account', 'namespace' => 'Api\V1'], function () {
    // Get logged user
    Route::get('/', 'UserController@currentUser');
});

//User Actions
Route::group(['prefix' => 'users', 'namespace' => 'Api\V1'], function () {

    Route::get('/', 'UserController@index');
    Route::post('/', 'UserController@store');

    // User Methods
    Route::group(['prefix' => '{id}'], function () {

        Route::get('/', 'UserController@show');
        Route::put('/', 'UserController@update');
        Route::delete('/', 'UserController@delete');

        // Get User Profile
        Route::get('profile', 'UserController@getProfile');

        // Update user password
        //Route::post('password/reset', 'Auth\PasswordController@postReset');

        
        Route::post('profile/image', 'UserController@updateImage');
        Route::delete('profile/image', 'UserController@deleteImage');
    });
});

// Resources
Route::group(['prefix' => 'resources', 'namespace' => 'Api\V1'], function () {

    /*Route::get('/', 'ResourceController@index');
    */
    Route::post('/remove', 'ResourceController@remove');
    Route::post('/update', 'ResourceController@updateRecurso');
    /*Route::get('/latest', 'ResourceController@getLatestResources');*/

    // Search Resource
    //Route::get('/search', 'ResourceController@search');

    Route::group(['prefix' => '{id}'], function () {

        Route::get('/', 'ResourceController@show');
        Route::put('/', 'ResourceController@update');
        Route::delete('/', 'ResourceController@delete');
        Route::post('attach', 'ResourceController@upload');
        Route::post('like', 'ResourceController@like');
        Route::put('addToCollection', 'ResourceController@addToCollection');
        Route::resource('tags', 'ResourceTagController');
    });
});

// Categories
Route::group(['prefix' => 'categories', 'namespace' => 'Api\V1'], function () {
    Route::get('/{slug}/resources', 'CategoryController@resources');
    Route::resource('/', 'CategoryController');
});

Route::resource('collections', 'Api\V1\CollectionController');

//Route::resource('tags', 'Api\V1\TagController');

/*Route::group(['prefix' => 'polls', 'namespace' => 'Api\V1'], function () {
    Route::post('/{id}/vote', 'PollController@vote');
    Route::get('/{id}/result', 'PollController@result');
    Route::get('/last', 'PollController@last');

    Route::resource('/', 'PollController');
});*/


//gallery
Route::group(['prefix' => 'gallery', 'namespace' => 'Api\V1'], function () {
   /*  Route::get('/list', 'GalleryController@GetOrders')->name('gallery.GetOrders'); */
    Route::post('/update', 'GalleryController@UpdateOrder');
});