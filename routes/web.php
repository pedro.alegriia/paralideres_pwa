<?php

Route::prefix('/')->namespace('Web')->group(function () {
    // show the page page

    Route::get('/helper', function () {
        return \App\Helpers\helperx();
    });
    Route::redirect('/', '/app');

    Route::get('/app/{vue_capture?}', function () {
        return view('app');
    })->where('vue_capture', '[\/\w\.-]*');

    /***return view activation account */
    Route::get('/account/activation/{token}', 'HomeController@activateUser');

    /***search resource */
    Route::get('/resources/search', 'ResourceController@search');
    Route::get('/resources', 'ResourceController@index');

    Route::get('/collections/search', 'CollectionController@search');
    Route::get('/collections', 'CollectionController@index');

    Route::get('/{slug}/resources', 'CategoryController@resources');
    Route::resource('/categories', 'CategoryController');

    // show resource create form
    //Route::get('/resources/create', 'ResourceController@create')->middleware('auth');

    Route::get('app#/recursos/{id}', 'ResourceController@show');

   
    Route::group(['prefix' => '/resources/{slug}'], function () {
        // show single resource
        Route::get('/', 'ResourceController@show');
        Route::get('/edit', 'ResourceController@edit');

        // Download Resource
        Route::get('/download', 'ResourceController@download');
    });

    Route::group(['prefix' => '/collections/{slug}'], function () {
        // show single resource
        Route::get('/', 'CollectionController@show');
    });
});
/***
    Route::prefix('/')->namespace('Auth')->group(function () {
        // show web login form
        Route::get('login', 'LoginController@showLoginForm')->name('login');

        // show web registration form
        Route::get('register', 'LoginController@showRegistrationForm');
        Route::get('register-ok', 'LoginController@showRegistrationSuccess');

        //Password Reset Routes...
        Route::get('password-reset', 'ForgotPasswordController@showLinkRequestForm');
        Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
        Route::get('password/reset', 'ResetPasswordController@showResetForm')->name('password.reset');
        Route::post('password/resetV2', 'ResetPasswordController@updatePassword')->name('password.update');
    });
*** */
Route::post('password/resetV2', 'Auth\ResetPasswordController@updatePassword')->name('password.update');
    Route::prefix('/')->namespace('Api\V1')->group(function () {

        // login from web by using api
        Route::post('login', 'AuthenticateController@authenticate');

        // logout from web by using api
        Route::post('logout', 'AuthenticateController@logout');
    });

    Route::group(['prefix' => 'users', 'namespace' => 'Api\V1'], function () {

        Route::get('/', 'UserController@index');
        Route::post('/', 'UserController@store');

        // User Methods
        Route::group(['prefix' => '{id}'], function () {

            Route::get('/', 'UserController@show');
            Route::put('/', 'UserController@update');
            Route::delete('/', 'UserController@delete');

            // Get User Profile
            Route::get('profile', 'UserController@getProfile');

            // Update user password
            //Route::post('password/reset', 'Auth\PasswordController@postReset');

            Route::put('profile', 'UserController@updateProfile');
            Route::post('profile/image', 'UserController@updateImage');
            Route::post('password/update', 'UserController@updatePassword');
            Route::delete('profile/image', 'UserController@deleteImage');
            Route::post('/avatar', 'UserController@setAvatar');
            Route::get('/settings', 'UserController@getSettings');
            Route::post('/settings', 'UserController@setSettings');
        });
    });

    Route::group(['prefix' => 'polls', 'namespace' => 'Api\V1'], function () {
        Route::post('/{id}/vote', 'PollController@vote');
        Route::get('/{id}/result', 'PollController@result');
        Route::get('/last', 'PollController@last');

        Route::resource('/', 'PollController');
    });




Route::get('/webapi/resources', 'Api\V1\ResourceController@index');
Route::get('/webapi/resources-by-query', 'Api\V1\ResourceController@getResourcesByQuery');
Route::post('/webapi/resources', 'Api\V1\ResourceController@store');
Route::get('/webapi/resources/latest', 'Api\V1\ResourceController@getLatestResources');
Route::get('/webapi/resources/latest/{id}', 'Api\V1\ResourceController@getLatestResourcesByAuthor');
Route::get('/webapi/resources/ranked/{id}', 'Api\V1\ResourceController@getRankedResourcesByAuthor');
Route::post('/webapi/resources/{id}/like', 'Api\V1\ResourceController@like');
Route::get('/webapi/resources/{id}', 'Api\V1\ResourceController@show');
Route::post('/webapi/resources/{id}', 'Api\V1\ResourceController@updateResource');
Route::post('/webapi/resources/{id}/file', 'Api\V1\ResourceController@updateFileResource');
Route::post('/webapi/resources/{id}/new-update', 'Api\V1\ResourceController@newUpdate');
Route::get('/webapi/resources/{id}/edit', 'Api\V1\ResourceController@edit');
Route::post('/webapi/resources/updateInfo/{id}', 'Api\V1\ResourceController@updateInfo');
Route::post('/webapi/resources/file/delete', 'Api\V1\ResourceController@fileDelete');
Route::get('/webapi/resources/{slug}/download-content', 'Api\V1\ResourceController@downloadContent');
Route::get('/webapi/resources/{slug}/download-attachment', 'Api\V1\ResourceController@downloadAttachment');
Route::post('/webapi/ratings', 'Api\V1\RatingController@setRating');

//Route::get('/webapi/collections/{collection_id}/resources', 'Api\V1\CollectionController@getResources');
Route::get('/webapi/collections/{collection_id}/resources', 'Api\V1\CollectionController@getResourcesByCollection');
Route::post('/webapi/collections/sync/resources', 'Api\V1\CollectionController@syncResources');
Route::get('/webapi/collections/{slug}', 'Api\V1\CollectionController@show');


Route::get('/webapi/tags', 'Api\V1\ResourceController@searchTags');
Route::get('/webapi/author/{id}', 'Api\V1\ResourceController@getAuthor');
Route::get('/webapi/authors', 'Api\V1\ResourceController@searchAuthors');

Route::resource('/tags', 'Api\V1\TagController');

Route::get('/ads', 'Api\V1\ResourceController@getAds');
Route::get('/linker/{uuid}', 'Api\V1\ResourceController@linker');
Route::get('/mailablex', function () {
    return new \App\Mail\UserActivationCode(App\Models\User::find(1));
});
Route::get('/mailable/{email}', function ($email) {
    Mail::raw('Test email', function ($message) use ($email) {
        $message->subject('Testing Mailables')
            ->to($email);
    });
});

Route::get('/launching/{email}', function ($email) {
    Mail::to($email)->send(new \App\Mail\LaunchingV2());
});

/* Route::get('/elasticsearch/{query}', 'ElasticSearchController@search');
Route::resource('/elastic', 'ElasticSearchController'); */


Route::post('/accept', 'Web\HomeController@acceptTerms');
Route::get('/badges', 'Web\HomeController@badges');

//login admin
Route::post('loginAdmin', 'LoginAdminController@loginAdmin');
Route::get('admin_login', 'LoginAdminController@loginAdminView');

Route::get('reset_password', 'ResetPasswordController@index');


Route::group(['prefix' => 'admin', 'namespace' => 'Web'], function() {
    //'middleware' => 'backend',
//oute::group(['prefix' => 'admin', 'namespace' => 'Web','middleware' => 'backend'], function() {
    // Rutas de los controladores dentro del Namespace "App\Http\Controllers\Admin"
    Route::view('/', 'dashboard.index')->name('admin');

    //ads
    Route::resource('/ads', 'AdminAdsController');

    Route::get('/password-reset/token={token}', 'AdminUserController@showEmail');
    //recursos
    Route::get('/recursos/datatables', 'AdminResourceController@getDatatable');
    Route::resource('/recursos', 'AdminResourceController');
    Route::post('/recursos/publishedResources/{id}', 'AdminResourceController@publishedResources');
    Route::post('/recursos/recover/{id}', 'AdminResourceController@recover')->name('recursos.recover');

    //publicamos recurso
    Route::get('/recursos/{id}/status', 'AdminResourceController@togglePublished')->name('recursos.published');

    //usuarios
    Route::get('/usuarios/datatables', 'AdminUserController@getDatatable');
    Route::post('/usuarios/avatar', 'AdminUserController@manageAvatar');
    Route::resource('/usuarios', 'AdminUserController');

    //encuestas
    Route::get('encuestas', 'AdminPollController@index');
    //Route::resource('/encuestas', 'Web\AdminPollController')->middleware('backend');

    Route::resource('/encuestas', 'AdminPollController');
    Route::get('/encuestas/{id}/status', 'AdminPollController@status')->name('encuestas.status');
    Route::get('/encuestas/datatables', 'AdminPollController@getDatatable');

    //collections
    Route::get('/collections/datatables', 'AdminCollectionController@getDatatable');
    Route::get('/collections/resources/{collection_id}', 'AdminCollectionController@resources');
    Route::resource('/collections', 'AdminCollectionController');

    //resource
    //Route::get('/resources/{id}', 'AdminResourceController@edit');
    Route::get('/resources/edit/{resource_id}', 'AdminResourceController@edit');
   // Route::get('resourceEdit','AdminResourceController@edit');
    //notifications
    Route::get('/notifications', 'AdminNotificationController@index')->name('notifications.index');
    Route::get('/notifications/{id}/read', 'AdminNotificationController@markAsRead')->name('notification.read');
    Route::get('/notifications/read', 'AdminNotificationController@markAllAsRead')->name('notification.all.read');
    Route::get('/notifications/unread', 'Web\NotificationController@getUnreadNotifications');

    //gallery
    Route::get('/gallery','GalleryController@index')->name('gallery.index');
    Route::get('/gallery/order','GalleryController@order')->name('gallery.order');
    Route::get('/gallery/datatables','GalleryController@getDatatable');
    Route::get('/gallery/create','GalleryController@create')->name('gallery.create');
    Route::post('/gallery/create','GalleryController@store')->name('gallery.store');
    Route::get('/gallery/edit/{id}','GalleryController@edit')->name('gallery.edit');
    Route::post('/gallery/edit/{id}','GalleryController@update')->name('gallery.update');
    Route::post('/gallery/change/{id}','GalleryController@change')->name('gallery.change');
    Route::post('/gallery/destroy{id}','CategoryController@destroy')->name('gallery.destroy');

    //categories
    Route::get('/categories','CategoryController@list')->name('categories.list');
    Route::get('/categories/datatables', 'CategoryController@getDatatable');
    Route::get('/categories/create','CategoryController@create');
    Route::put('/categories/update/{id}','CategoryController@update');
    Route::post('/categories/store','CategoryController@store');
    Route::get('/categories/{id}','CategoryController@edit');
    Route::post('/categories/edit/{id}','CategoryController@update');
    Route::get('/categories/show/{id}','CategoryController@show');
    Route::post('/categories/{id}/image/{mediaId}','CategoryController@imageDestroy');

});
